ï»¿"""This module contains skeletons which are a method of abstracting
the bone structures and layouts of various avatar representations. Examples
include disembodied head and hand models as well as Complete Characters
models and a more general auto parsed human model.
"""

import re

import viz
import vizmat

import inverse_kinematics


AVATAR_HEAD = 'head'
AVATAR_TORSO = 'torso'
AVATAR_PELVIS = 'pelvis'

AVATAR_L_EYE = 'l_eye'
AVATAR_R_EYE = 'r_eye'

AVATAR_L_CLAVICLE = 'l_clavicle'
AVATAR_R_CLAVICLE = 'r_clavicle'
AVATAR_L_UPPER_ARM = 'l_upper_arm'
AVATAR_R_UPPER_ARM = 'r_upper_arm'
AVATAR_L_FOREARM = 'l_forearm'
AVATAR_R_FOREARM = 'r_forearm'
AVATAR_L_HAND = 'l_hand'
AVATAR_R_HAND = 'r_hand'

AVATAR_L_THIGH = 'l_thigh'
AVATAR_R_THIGH = 'r_thigh'
AVATAR_L_CALF = 'l_calf'
AVATAR_R_CALF = 'r_calf'
AVATAR_L_FOOT = 'l_foot'
AVATAR_R_FOOT = 'r_foot'

AVATAR_NUB = '_nub'

JOINT_L_ELBOW = 'l_elbow'
JOINT_R_ELBOW = 'r_elbow'
JOINT_L_SHOULDER = 'l_shoulder'
JOINT_R_SHOULDER = 'r_shoulder'
JOINT_L_WRIST = 'l_wrist'
JOINT_R_WRIST = 'r_wrist'

JOINT_L_HIP = 'l_hip'
JOINT_R_HIP = 'r_hip'
JOINT_L_KNEE = 'l_knee'
JOINT_R_KNEE = 'r_knee'
JOINT_L_ANKLE = 'l_ankle'
JOINT_R_ANKLE = 'r_ankle'

AVATAR_ROOT = 'root'
AVATAR_BASE = 'base'

DEFAULT_EYE_HEIGHT = 1.82


class _Skeleton(object):
	"""A base skeleton class"""
	def __init__(self, avatar=None, **kwargs):
		super(_Skeleton, self).__init__(**kwargs)
		
		self._avatar = avatar
		self._boneDict = self._createBoneDict()
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		return {AVATAR_BASE:self._avatar}
	
	def getBoneDict(self):
		"""Returns a dictionary containing a set of bones indexed by bone name.
		
		@return {}
		"""
		return self._boneDict
	
	def getShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		return vizmat.Transform()
	
	def getShiftInv(self, bone):
		"""Returns the shift inverse matrix, a static value compute during
		initialization.
		
		@return vizmat.Transform()
		"""
		return vizmat.Transform()
	
	def setScale(self, scale):
		"""Sets the scale of the skeleton. This needs to be called when adjusting
		the scale of an avatar, so the animations match properly.
		"""
		pass
	
	def setToDefaultLayout(self):
		"""Returns the skeleton and by extension the avatar to the default layout."""
		pass


class _Human(_Skeleton):
	"""Base class for any human skeletons"""
	def __init__(self, **kwargs):
		super(_Human, self).__init__(**kwargs)


class Disembodied(_Human):
	"""Skeleton (possibly Base) class for disembodied skeletons."""
	def __init__(self, avatar, **kwargs):
		super(Disembodied, self).__init__(avatar=avatar, **kwargs)
		
		self._defaultPositionMappings = {
			AVATAR_HEAD:[0, DEFAULT_EYE_HEIGHT, 0],
			AVATAR_R_HAND:[DEFAULT_EYE_HEIGHT/2.0, 1.65, 0],
			AVATAR_L_HAND:[-DEFAULT_EYE_HEIGHT/2.0, 1.65, 0],
		}
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(Disembodied, self)._createBoneDict()
		boneDict.update(self._avatar._bodyPartDict)
		return boneDict
	
	def setToDefaultLayout(self):
		"""Returns the skeleton and by extension the avatar to the default layout."""
		for key, val in self._defaultPositionMappings:
			if key in self._boneDict:
				self._boneDict[key].setPosition(val)


class _Jointed(_Human):
	"""Generic class for Jointed human skeletons"""
	def __init__(self, **kwargs):
		super(_Jointed, self).__init__(**kwargs)
		self._rootPos = self._avatar.getRootBoneList()[0].getPosition()
		self._jointDict = self._createJointDict()
		self._shiftDict = {}
		self._shiftInvDict = {}
		self._originalBoneLength = {}
		
		self._defaultTransLocal = self._initDefaultTransforms()
		self._defaultTransGlobal = self._initDefaultTransforms(viz.ABS_GLOBAL)
		self._initShiftDict()
	
	def _computeShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		raise NotImplementedError
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(_Jointed, self)._createBoneDict()
		rootBoneList = self._avatar.getRootBoneList()
		boneDict.update({
			AVATAR_ROOT:rootBoneList[0]
		})
		return boneDict
	
	def _createJointDict(self):
		"""Creates the joint dictionary for the skeleton."""
		return {}
	
	def getDefaultGlobalTransforms(self):
		"""Returns the default global transformation dictionary.
		
		@return {}
		"""
		return self._defaultTransGlobal
	
	def getDefaultLocalTransforms(self):
		"""Returns the default local transformation dictionary.
		
		@return {}
		"""
		return self._defaultTransLocal
	
	def getBoneLength(self, bone):
		"""Returns the length (in meters) of the given bone
		
		@return float
		"""
		try:
			bonePos = vizmat.Vector(self._boneDict[bone].getPosition(viz.ABS_GLOBAL))
			children = self._boneDict[bone].getChildren()
			maxLength = 0
			for child in children:
				tempLength = (bonePos-vizmat.Vector(child.getPosition(viz.ABS_GLOBAL))).length()
				if tempLength > maxLength:
					maxLength = tempLength
			return maxLength
		except AttributeError:
			viz.logWarn('**Warning: invalid access for bone {}.'.format(bone))
			return 0
	
	def getJointDict(self):
		"""Returns the joint dictionary"""
		return self._jointDict
	
	def getShift(self, bone):
		"""Function that returns the shift/offset offset applied to
		a bone to place it into the global coordinate frame.
		
		@return vizmat.Transform()
		"""
		return vizmat.Transform(self._shiftDict[bone])
	
	def getShiftInv(self, bone):
		"""Function that returns the inverse of the shift/offset offset applied to
		a bone to place it into the global coordinate frame.
		
		@return vizmat.Transform()
		"""
		return vizmat.Transform(self._shiftInvDict[bone])
	
	def resetBoneLength(self, bone, fullRefresh=True):
		"""Resets the bone to it's original length"""
		try:
			self.setBoneLength(bone, self._originalBoneLength[bone], fullRefresh=fullRefresh)
		except KeyError:
			pass
	
	def refreshJointLengths(self):
		"""Refreshes the lengths of all joints. This function should be used 
		sparingly. Generally, it should only be used if changing a bone length
		when fullRefresh=False.
		"""
		for joint in self._jointDict.values():
			joint.refreshLength()
	
	def setBoneLength(self, bone, length, fullRefresh=True):
		"""Sets the length of the given bone in meters"""
		try:
			if not bone in self._originalBoneLength:
				self._originalBoneLength[bone] = self.getBoneLength(bone)
			
			currentBoneLength = self.getBoneLength(bone)
			
			parent = self._boneDict[bone]
			children = parent.getChildren()
			parentPos = vizmat.Vector(parent.getPosition(viz.ABS_GLOBAL))
			for child in children:
				childPos = vizmat.Vector(child.getPosition(viz.ABS_GLOBAL))
				diff = (childPos-parentPos)
				norm = diff/currentBoneLength
				child.setPosition(parentPos + norm*length, viz.ABS_GLOBAL)
			# normalize the vector then scale by length
			# refresh all joints for now, (TI_PERFORMANCE) a little inefficient
			if fullRefresh:
				self.refreshJointLengths()
		except AttributeError:
			viz.logWarn('**Warning: invalid accesss for bone {}.'.format(bone))
	
	def setScale(self, scale):
		"""Sets the scale of the skeleton. This needs to be called when adjusting
		the scale of an avatar, so the animations match properly.
		"""
		self._originalBoneLength = {}
		for boneName in self._boneDict.keys():
			shift = self._computeShift(boneName)
			self._shiftDict[boneName] = shift
			self._shiftInvDict[boneName] = shift.inverse()
		self.refreshJointLengths()
	
	def _initDefaultTransforms(self, mode=viz.ABS_PARENT):
		"""Internal method which returns the default transforms of the skeleton,
		in the given mode
		
		@return {}
		"""
		return {}
	
	def _initShiftDict(self):
		"""initializes all the shifts and inverse shifts. Calls computeShift
		for each bone
		"""
		for bone in self._boneDict.keys():
			self._shiftDict[bone] = self._computeShift(bone)
			self._shiftInvDict[bone] = self._shiftDict[bone].inverse()


class CompleteCharacters(_Jointed):
	"""Skeleton for complete characters models"""
	def __init__(self, avatar, **kwargs):
		super(CompleteCharacters, self).__init__(avatar=avatar, **kwargs)
	
	def _createJointDict(self):
		"""Internal method which creates the joint dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = self.getBoneDict()
		jointDict = super(CompleteCharacters, self)._createJointDict()
		
		jointDict[JOINT_R_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_R_FOREARM], boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_HAND], [0, 0, 1])
		jointDict[JOINT_R_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_CLAVICLE], boneDict[AVATAR_R_FOREARM], [0, 0, -1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_R_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_R_HAND], boneDict[AVATAR_R_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_L_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_L_FOREARM], boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_HAND], [0, 0, 1])
		jointDict[JOINT_L_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_CLAVICLE], boneDict[AVATAR_L_FOREARM], [0, 0, 1], [0, -1, 0], [1, 0, 0])
		jointDict[JOINT_L_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_L_HAND], boneDict[AVATAR_L_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		
		jointDict[JOINT_R_HIP] = inverse_kinematics._Ball(boneDict[AVATAR_R_THIGH], boneDict[AVATAR_PELVIS], boneDict[AVATAR_R_CALF], [1, 0, 0], [0, 0, 1], [0, -1, 0])
		jointDict[JOINT_R_KNEE] = inverse_kinematics._Hinge(boneDict[AVATAR_R_CALF], boneDict[AVATAR_R_THIGH], boneDict[AVATAR_R_FOOT], [0, 0, 1])
		jointDict[JOINT_R_ANKLE] = inverse_kinematics._Ball(boneDict[AVATAR_R_FOOT], boneDict[AVATAR_R_CALF], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_L_HIP] = inverse_kinematics._Ball(boneDict[AVATAR_L_THIGH], boneDict[AVATAR_PELVIS], boneDict[AVATAR_L_CALF], [1, 0, 0], [0, 0, 1], [0, -1, 0])
		jointDict[JOINT_L_KNEE] = inverse_kinematics._Hinge(boneDict[AVATAR_L_CALF], boneDict[AVATAR_L_THIGH], boneDict[AVATAR_L_FOOT], [0, 0, 1])
		jointDict[JOINT_L_ANKLE] = inverse_kinematics._Ball(boneDict[AVATAR_L_FOOT], boneDict[AVATAR_L_CALF], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		
		return jointDict
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(CompleteCharacters, self)._createBoneDict()
		
		boneDict[AVATAR_HEAD] = self._avatar.getBone('Bip01 Head')
#		boneDict[AVATAR_PELVIS] = self._avatar.getBone('Bip01')
		boneDict[AVATAR_PELVIS] = self._avatar.getBone('Bip01 Pelvis')
		boneDict[AVATAR_TORSO] = self._avatar.getBone('Bip01 Spine1')
		
		boneDict[AVATAR_L_CLAVICLE] = self._avatar.getBone('Bip01 L Clavicle')
		boneDict[AVATAR_L_UPPER_ARM] = self._avatar.getBone('Bip01 L UpperArm')
		boneDict[AVATAR_L_FOREARM] = self._avatar.getBone('Bip01 L Forearm')
		boneDict[AVATAR_L_HAND] = self._avatar.getBone('Bip01 L Hand')
		
		boneDict[AVATAR_R_CLAVICLE] = self._avatar.getBone('Bip01 R Clavicle')
		boneDict[AVATAR_R_UPPER_ARM] = self._avatar.getBone('Bip01 R UpperArm')
		boneDict[AVATAR_R_FOREARM] = self._avatar.getBone('Bip01 R Forearm')
		boneDict[AVATAR_R_HAND] = self._avatar.getBone('Bip01 R Hand')
		
		boneDict[AVATAR_L_THIGH] = self._avatar.getBone('Bip01 L Thigh')
		boneDict[AVATAR_L_CALF] = self._avatar.getBone('Bip01 L Calf')
		boneDict[AVATAR_L_FOOT] = self._avatar.getBone('Bip01 L Foot')
		
		boneDict[AVATAR_R_THIGH] = self._avatar.getBone('Bip01 R Thigh')
		boneDict[AVATAR_R_CALF] = self._avatar.getBone('Bip01 R Calf')
		boneDict[AVATAR_R_FOOT] = self._avatar.getBone('Bip01 R Foot')
		
		return boneDict
	
	def _computeShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		mat = vizmat.Transform()
		mat.setQuat(self._defaultTransGlobal[bone])
		mat = mat.inverse()
		if bone == AVATAR_HEAD:
			self._boneDict[AVATAR_HEAD].setQuat(self._defaultTransGlobal[AVATAR_HEAD], viz.ABS_GLOBAL)
			# doesn't have eyes so best guess
			mat.preTrans([0.0, 0.12, 0.107])#0.08+0.027
		elif bone == AVATAR_L_HAND:
			mat.postEuler(90, 0, 0)
		elif bone == AVATAR_R_HAND:
			mat.postEuler(-90, 0, 0)
		return mat
	
	def _initDefaultTransforms(self, mode=viz.ABS_PARENT):
		"""Internal method which returns the default transforms of the skeleton,
		in the given mode
		
		@return {}
		"""
		transformDict = {}
		
		# save the initial orientation of the avatar
		startQuat = self._avatar.getQuat(viz.ABS_GLOBAL)
		
		# set to the standard layout
		self._setStandard()
		
		# get the 
		for bone in self._boneDict.keys():
			transformDict[bone] = self._boneDict[bone].getQuat(mode=mode)
		
		# return the orientation of the avatar
		self._avatar.setQuat(startQuat, viz.ABS_GLOBAL)
		
		return transformDict
	
	def _setStandard(self):
		"""Apply the standard transformation"""
		self._boneDict[AVATAR_BASE].setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		#
		self._boneDict[AVATAR_PELVIS].setEuler(0, 0, 0, viz.AVATAR_WORLD)
		self._boneDict[AVATAR_TORSO].setEuler(0, 0, 0)
		self._boneDict[AVATAR_HEAD].setEuler(0, 0, 0)
		#
		self._boneDict[AVATAR_L_UPPER_ARM].setEuler(180, 90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_R_UPPER_ARM].setEuler(0, -90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_L_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_L_HAND].setEuler(0, 90, 0)
		self._boneDict[AVATAR_R_HAND].setEuler(0, -90, 0)
		#
		self._boneDict[AVATAR_L_THIGH].setEuler(-90, 0, -90, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_R_THIGH].setEuler(-90, 0, -90, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_L_CALF].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_CALF].setEuler(0, 0, 0)
		self._boneDict[AVATAR_L_FOOT].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_FOOT].setEuler(0, 0, 0)
	
	def setToDefaultLayout(self):
		"""Returns the skeleton and by extension the avatar to the default layout."""
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		for bone in [
			AVATAR_ROOT,
			AVATAR_PELVIS,
			AVATAR_TORSO,
			AVATAR_HEAD,
			AVATAR_L_UPPER_ARM,
			AVATAR_R_UPPER_ARM,
			AVATAR_L_FOREARM,
			AVATAR_R_FOREARM,
			AVATAR_L_HAND,
			AVATAR_R_HAND,
			AVATAR_L_THIGH,
			AVATAR_R_THIGH,
			AVATAR_L_CALF,
			AVATAR_R_CALF,
			AVATAR_L_FOOT,
			AVATAR_R_FOOT]:
			self._boneDict[bone].setQuat(self._defaultTransLocal[bone])
	
	def unlockAll(self):
		"""A generalized function to unlock all of the bones for a skeleton."""
		self._avatar.getBone('Bip01').unlock(recurse=1)


class CompleteCharactersHD(CompleteCharacters):
	"""Skeleton for complete characters HD models"""
	def __init__(self, avatar):
		super(CompleteCharactersHD, self).__init__(avatar)
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(CompleteCharactersHD, self)._createBoneDict()
		
		boneDict.update({
			AVATAR_R_EYE:self._avatar.getBone('Bip01 REyeNub'),
			AVATAR_L_EYE:self._avatar.getBone('Bip01 LEyeNub')
		})
		
		return boneDict
	
	def _computeShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		mat = vizmat.Transform()
		mat.setQuat(self._defaultTransGlobal[bone])
		mat = mat.inverse()
		if bone == AVATAR_HEAD:
			self._boneDict[AVATAR_HEAD].setQuat(self._defaultTransGlobal[AVATAR_HEAD], viz.ABS_GLOBAL)
			if AVATAR_L_EYE in self._boneDict and AVATAR_R_EYE in self._boneDict:
				# get the offset from the bone dict
				mid = vizmat.Vector(self._boneDict[AVATAR_R_EYE].getPosition(viz.ABS_GLOBAL))
				mid += vizmat.Vector(self._boneDict[AVATAR_L_EYE].getPosition(viz.ABS_GLOBAL))
				mid /= 2.0
				mid -= vizmat.Vector(self._boneDict[AVATAR_HEAD].getPosition(viz.ABS_GLOBAL))
				mat.preTrans(mid)
			else:# doesn't have eyes so best guess
				mat.preTrans([0.0, 0.07835, 0.126368])
		elif bone == AVATAR_L_HAND:
			mat.postEuler(90, 0, 0)
		elif bone == AVATAR_R_HAND:
			mat.postEuler(-90, 0, 0)
		return mat


class RobotTorso(CompleteCharactersHD):
	"""Skeleton for complete characters models"""
	def __init__(self, avatar, **kwargs):
		super(RobotTorso, self).__init__(avatar=avatar, **kwargs)
	
	def _createJointDict(self):
		"""Internal method which creates the joint dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = self.getBoneDict()
		jointDict = super(RobotTorso, self)._createJointDict()
		
		jointDict[JOINT_R_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_R_FOREARM], boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_HAND], [0, 0, 1])
		jointDict[JOINT_R_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_CLAVICLE], boneDict[AVATAR_R_FOREARM], [0, 0, -1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_R_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_R_HAND], boneDict[AVATAR_R_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_L_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_L_FOREARM], boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_HAND], [0, 0, 1])
		jointDict[JOINT_L_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_CLAVICLE], boneDict[AVATAR_L_FOREARM], [0, 0, 1], [0, -1, 0], [1, 0, 0])
		jointDict[JOINT_L_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_L_HAND], boneDict[AVATAR_L_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		
		return jointDict
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(RobotTorso, self)._createBoneDict()
		
		boneDict[AVATAR_HEAD] = self._avatar.getBone('Bip01 Head')
#		boneDict[AVATAR_PELVIS] = self._avatar.getBone('Bip01')
		boneDict[AVATAR_PELVIS] = self._avatar.getBone('Bip01 Pelvis')
		boneDict[AVATAR_TORSO] = self._avatar.getBone('Bip01 Spine1')
		
		boneDict[AVATAR_L_CLAVICLE] = self._avatar.getBone('Bip01 L Clavicle')
		boneDict[AVATAR_L_UPPER_ARM] = self._avatar.getBone('Bip01 L UpperArm')
		boneDict[AVATAR_L_FOREARM] = self._avatar.getBone('Bip01 L Forearm')
		boneDict[AVATAR_L_HAND] = self._avatar.getBone('Bip01 L Hand')
		
		boneDict[AVATAR_R_CLAVICLE] = self._avatar.getBone('Bip01 R Clavicle')
		boneDict[AVATAR_R_UPPER_ARM] = self._avatar.getBone('Bip01 R UpperArm')
		boneDict[AVATAR_R_FOREARM] = self._avatar.getBone('Bip01 R Forearm')
		boneDict[AVATAR_R_HAND] = self._avatar.getBone('Bip01 R Hand')
		
		return boneDict
	
	def _computeShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		mat = vizmat.Transform()
		mat.setQuat(self._defaultTransGlobal[bone])
		mat = mat.inverse()
		if bone == AVATAR_HEAD:
			self._boneDict[AVATAR_HEAD].setQuat(self._defaultTransGlobal[AVATAR_HEAD], viz.ABS_GLOBAL)
			# doesn't have eyes so best guess
			mat.preTrans([0.0, 0.12, 0.107])#0.08+0.027
		elif bone == AVATAR_L_HAND:
			mat.postEuler(90, 0, 0)
		elif bone == AVATAR_R_HAND:
			mat.postEuler(-90, 0, 0)
		return mat

	
	def _initDefaultTransforms(self, mode=viz.ABS_PARENT):
		"""Internal method which returns the default transforms of the skeleton,
		in the given mode
		
		@return {}
		"""
		transformDict = {}
		
		# save the initial orientation of the avatar
		startQuat = self._avatar.getQuat(viz.ABS_GLOBAL)
		
		# set to the standard layout
		self._setStandard()
		
		# get the 
		for bone in self._boneDict.keys():
			transformDict[bone] = self._boneDict[bone].getQuat(mode=mode)
		
		# return the orientation of the avatar
		self._avatar.setQuat(startQuat, viz.ABS_GLOBAL)
		
		return transformDict
	
	def _setStandard(self):
		"""Apply the standard transformation"""
		self._boneDict[AVATAR_BASE].setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		#
		self._boneDict[AVATAR_PELVIS].setEuler(0, 0, 0, viz.AVATAR_WORLD)
		self._boneDict[AVATAR_TORSO].setEuler(0, 0, 0)
		self._boneDict[AVATAR_HEAD].setEuler(0, 0, 0)
		#
		self._boneDict[AVATAR_L_UPPER_ARM].setEuler(180, 90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_R_UPPER_ARM].setEuler(0, -90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_L_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_L_HAND].setEuler(0, 90, 0)
		self._boneDict[AVATAR_R_HAND].setEuler(0, -90, 0)
	
	def setToDefaultLayout(self):
		"""Returns the skeleton and by extension the avatar to the default layout."""
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		for bone in [
			AVATAR_ROOT,
			AVATAR_PELVIS,
			AVATAR_TORSO,
			AVATAR_HEAD,
			AVATAR_L_UPPER_ARM,
			AVATAR_R_UPPER_ARM,
			AVATAR_L_FOREARM,
			AVATAR_R_FOREARM,
			AVATAR_L_HAND,
			AVATAR_R_HAND]:
			self._boneDict[bone].setQuat(self._defaultTransLocal[bone])
	
	def unlockAll(self):
		"""A generalized function to unlock all of the bones for a skeleton."""
		self._avatar.getBone('Bip01').unlock(recurse=1)



class HumanAutoParse(_Jointed):
	"""A human auto parse implementation. This is a best effort attempt
	at parsing a generic avatar model, with undefined bone/joint names.
	"""
	def __init__(self, avatar, **kwargs):
		super(HumanAutoParse, self).__init__(avatar=avatar, **kwargs)
	
	def _createBoneDict(self):
		"""Internal method which creates the bone dictionary for the skeleton.
		
		@return {}
		"""
		boneDict = super(HumanAutoParse, self)._createBoneDict()
		rootBoneList = self._avatar.getRootBoneList()
		
		boneNameTranslationDict = {}
		left = 'l(eft)*[_ ]*'
		right = 'r(ight)*[_ ]*'
		
		searchDict = {
			AVATAR_HEAD: 'head',
			AVATAR_TORSO: 'spine',
			AVATAR_PELVIS: 'pelvis',
			
			AVATAR_L_EYE: left+'eye',
			AVATAR_R_EYE: right+'eye',
			
			AVATAR_L_CLAVICLE: left+'clavicle',
			AVATAR_R_CLAVICLE: right+'clavicle',
			AVATAR_L_UPPER_ARM: left+'upperarm',
			AVATAR_R_UPPER_ARM: right+'upperarm',
			AVATAR_L_FOREARM: left+'forearm',
			AVATAR_R_FOREARM: right+'forearm',
			AVATAR_L_HAND: left+'hand',
			AVATAR_R_HAND: right+'hand',

			AVATAR_L_THIGH: left+'thigh',
			AVATAR_R_THIGH: right+'thigh',
			AVATAR_L_CALF: left+'(leg|calf)',
			AVATAR_R_CALF: right+'(leg|calf)',
			AVATAR_L_FOOT: left+'foot',
			AVATAR_R_FOOT: right+'foot',
		}
		for rootBone in rootBoneList:
			self._parseChildren(boneNameTranslationDict, rootBone, searchDict)
		for stdBoneName, localBoneName in boneNameTranslationDict.iteritems():
			if localBoneName:
				boneDict[stdBoneName] = self._avatar.getBone(localBoneName)
		
		boneDict[AVATAR_ROOT] = rootBoneList[0]
		
		return boneDict
	
	def _alignWithVec(self, bone, vec, terminator):
		"""Rotate the bone so that the target bone matches the vec."""
		if bone == terminator:
			return True
		
		for child in bone.getChildren():
			if self._alignWithVec(child, vec, terminator):
				basePos = vizmat.Vector(bone.getPosition(viz.ABS_GLOBAL))
				childPos = vizmat.Vector(child.getPosition(viz.ABS_GLOBAL))
				
				startVec = childPos-basePos
				startVec.normalize()
				# globally rotate so that vectors are inline
				if vizmat.Vector(vec)*startVec < 0.99:
					mat = vizmat.Transform()
					
					rotVec = startVec.cross(vec)
					rotAngle = inverse_kinematics._signedAngle(startVec, vec, rotVec)
					
					mat.setQuat(bone.getQuat(viz.ABS_GLOBAL))
					mat.postAxisAngle(rotVec[0], rotVec[1], rotVec[2], -rotAngle)
					bone.setQuat(mat.getQuat(), viz.ABS_GLOBAL)
				
				return True
		return False
	
	def _alignRoll(self, bone, targetVec, localVec, target):
		"""Rotate the bone so that the target bone matches the vec."""
		targetVec = vizmat.Vector(targetVec)
		targetVec.normalize()
		
		# put local vec into global frame
		currentVec = vizmat.Vector(target.getMatrix(viz.ABS_GLOBAL).preMultVec(localVec))
		currentVec.normalize()
		rotVec = targetVec.cross(currentVec)
		
		# get angle between vectors
		rotAngle = inverse_kinematics._signedAngle(targetVec, currentVec, rotVec)
		mat = vizmat.Transform()
		mat.setQuat(bone.getQuat(viz.ABS_GLOBAL))
		mat.postAxisAngle(rotVec[0], rotVec[1], rotVec[2], -rotAngle)
		
		# globally rotate so that vectors are inline
		bone.setQuat(mat.getQuat(), viz.ABS_GLOBAL)
	
	def _createJointDict(self):
		"""Creates the joint dictionary for the skeleton"""
		boneDict = self.getBoneDict()
		jointDict = super(HumanAutoParse, self)._createJointDict()
		
#		self._alignWithVec(boneDict[AVATAR_R_CLAVICLE], [1, 0, 0], boneDict[AVATAR_R_HAND])
#		self._alignRoll(boneDict[AVATAR_R_UPPER_ARM], [0, 0, -1], [0, 1, 0], boneDict[AVATAR_R_FOREARM])
		
		jointDict[JOINT_R_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_R_FOREARM], boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_HAND], [0, 0, 1])
		jointDict[JOINT_R_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_R_UPPER_ARM], boneDict[AVATAR_R_CLAVICLE], boneDict[AVATAR_R_FOREARM], [0, 0, -1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_R_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_R_HAND], boneDict[AVATAR_R_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_L_ELBOW] = inverse_kinematics._Hinge(boneDict[AVATAR_L_FOREARM], boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_HAND], [0, 0, 1])
		jointDict[JOINT_L_SHOULDER] = inverse_kinematics._Ball(boneDict[AVATAR_L_UPPER_ARM], boneDict[AVATAR_L_CLAVICLE], boneDict[AVATAR_L_FOREARM], [0, 0, 1], [0, -1, 0], [1, 0, 0])
		jointDict[JOINT_L_WRIST] = inverse_kinematics._Ball(boneDict[AVATAR_L_HAND], boneDict[AVATAR_L_FOREARM], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		
		jointDict[JOINT_R_HIP] = inverse_kinematics._Ball(boneDict[AVATAR_R_THIGH], boneDict[AVATAR_PELVIS], boneDict[AVATAR_R_CALF], [1, 0, 0], [0, 0, 1], [0, -1, 0])
		jointDict[JOINT_R_KNEE] = inverse_kinematics._Hinge(boneDict[AVATAR_R_CALF], boneDict[AVATAR_R_THIGH], boneDict[AVATAR_R_FOOT], [0, 0, 1])
		jointDict[JOINT_R_ANKLE] = inverse_kinematics._Ball(boneDict[AVATAR_R_FOOT], boneDict[AVATAR_R_CALF], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		jointDict[JOINT_L_HIP] = inverse_kinematics._Ball(boneDict[AVATAR_L_THIGH], boneDict[AVATAR_PELVIS], boneDict[AVATAR_L_CALF], [1, 0, 0], [0, 0, 1], [0, -1, 0])
		jointDict[JOINT_L_KNEE] = inverse_kinematics._Hinge(boneDict[AVATAR_L_CALF], boneDict[AVATAR_L_THIGH], boneDict[AVATAR_L_FOOT], [0, 0, 1])
		jointDict[JOINT_L_ANKLE] = inverse_kinematics._Ball(boneDict[AVATAR_L_FOOT], boneDict[AVATAR_L_CALF], None, [0, 0, 1], [0, 1, 0], [-1, 0, 0])
		
		return jointDict
	
	def _computeShift(self, bone):
		"""Function that returns any offset applied to a bone, generally to match 
		with an attachment point. Shifts are in global coordinates.
		
		@return vizmat.Transform()
		"""
		mat = vizmat.Transform()
		mat.setQuat(self._defaultTransGlobal[bone])
		mat = mat.inverse()
		if bone == AVATAR_HEAD:
			self._boneDict[AVATAR_HEAD].setQuat(self._defaultTransGlobal[AVATAR_HEAD], viz.ABS_GLOBAL)
			if AVATAR_L_EYE in self._boneDict and AVATAR_R_EYE in self._boneDict:
				# get the offset from the bone dict
				mid = vizmat.Vector(self._boneDict[AVATAR_R_EYE].getPosition(viz.ABS_GLOBAL))
				mid += vizmat.Vector(self._boneDict[AVATAR_L_EYE].getPosition(viz.ABS_GLOBAL))
				mid /= 2.0
				mid -= vizmat.Vector(self._boneDict[AVATAR_HEAD].getPosition(viz.ABS_GLOBAL))
				mat.preTrans(mid)
			else:# doesn't have eyes so best guess
				mat.preTrans([0.0, 0.07835, 0.126368])
		elif bone == AVATAR_L_HAND:
			mat.postEuler(90, 0, 0)
		elif bone == AVATAR_R_HAND:
			mat.postEuler(-90, 0, 0)
		return mat
	
	def _initDefaultTransforms(self, mode=viz.ABS_PARENT):
		"""Internal method which returns the default transforms of the skeleton,
		in the given mode
		
		@return {}
		"""
		transformDict = {}
		
		# save the initial orientation of the avatar
		startQuat = self._avatar.getQuat(viz.ABS_GLOBAL)
		
		# set to the standard layout
		self._setStandard()
		
		# get the 
		for bone in self._boneDict.keys():
			transformDict[bone] = self._boneDict[bone].getQuat(mode=mode)
		
		# return the orientation of the avatar
		self._avatar.setQuat(startQuat, viz.ABS_GLOBAL)
		
		return transformDict
	
	def _parseChildren(self, boneNameTranslationDict, bone, searchDict):
		"""Parses the children to determine the bone mappings"""
		name = bone.getName()
		lowerName = name.lower()
		
		# try to find some standard names for various body parts
		for stdBoneName, searchString in searchDict.iteritems():
			if searchString:
				match = re.search(searchString, lowerName)
				if match:
					# try to determine if it's a nub
					nubMatch = re.search(searchString+'[_ ]*nub', lowerName)
					isNub = False
					if nubMatch:
						stdBoneName += AVATAR_NUB
						isNub = True
					
					# check if match has been found
					if stdBoneName in boneNameTranslationDict:
						# if match has no children, use the previous
						# if it's not a nub, it's safer if it has children
						# keep shortest name
						if	(len(bone.getChildren()) == 0 and isNub
							or len(bone.getChildren()) > 0 and not isNub
#							or boneNameTranslationDict[stdBoneName] is None
							or len(name) < len(boneNameTranslationDict[stdBoneName])):
							boneNameTranslationDict[stdBoneName] = name
					else:#otherwise just add it
						boneNameTranslationDict[stdBoneName] = name
		
		# parse children
		for child in bone.getChildren():
			self._parseChildren(boneNameTranslationDict, child, searchDict)
	
	def _setStandard(self):
		"""Apply the standard transformation"""
		self._boneDict[AVATAR_BASE].setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		#
		self._boneDict[AVATAR_PELVIS].setEuler(0, 0, 0, viz.AVATAR_WORLD)
		self._boneDict[AVATAR_TORSO].setEuler(0, 0, 0)
		self._boneDict[AVATAR_HEAD].setEuler(0, 0, 0)
		#
		self._boneDict[AVATAR_L_UPPER_ARM].setEuler(180, 90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_R_UPPER_ARM].setEuler(0, -90, 0, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_L_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_FOREARM].setEuler(0, 0, 0)
		self._boneDict[AVATAR_L_HAND].setEuler(0, 90, 0)
		self._boneDict[AVATAR_R_HAND].setEuler(0, -90, 0)
		#
		self._boneDict[AVATAR_L_THIGH].setEuler(-90, 0, -90, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_R_THIGH].setEuler(-90, 0, -90, viz.ABS_GLOBAL)
		self._boneDict[AVATAR_L_CALF].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_CALF].setEuler(0, 0, 0)
		self._boneDict[AVATAR_L_FOOT].setEuler(0, 0, 0)
		self._boneDict[AVATAR_R_FOOT].setEuler(0, 0, 0)
	
	def setToDefaultLayout(self):
		"""Returns the skeleton and by extension the avatar to the default layout."""
		self._boneDict[AVATAR_ROOT].setPosition(self._rootPos)
		for bone in [
			AVATAR_ROOT,
			AVATAR_PELVIS,
			AVATAR_TORSO,
			AVATAR_HEAD,
			AVATAR_L_UPPER_ARM,
			AVATAR_R_UPPER_ARM,
			AVATAR_L_FOREARM,
			AVATAR_R_FOREARM,
			AVATAR_L_HAND,
			AVATAR_R_HAND,
			AVATAR_L_THIGH,
			AVATAR_R_THIGH,
			AVATAR_L_CALF,
			AVATAR_R_CALF,
			AVATAR_L_FOOT,
			AVATAR_R_FOOT]:
			self._boneDict[bone].setQuat(self._defaultTransLocal[bone])
	
	def unlockAll(self):
		"""A generalized function to unlock all of the bones for a skeleton."""
		rootBoneList = self._avatar.getRootBoneList()
		for rootBone in rootBoneList:
			rootBone.unlock(recurse=1)

