ï»¿"""Code that provides several types of animators for several types
of model files.
"""

import copy
import collections
import math

import viz
import vizmat
import vizinfo
import vizact

import skeleton
import inverse_kinematics


_TRACKER_COLOR_DICT = {
	skeleton.AVATAR_HEAD:viz.RED,
	skeleton.AVATAR_TORSO:viz.ORANGE,
	skeleton.AVATAR_PELVIS:viz.YELLOW,

	skeleton.AVATAR_L_CLAVICLE:viz.GREEN,
	skeleton.AVATAR_L_UPPER_ARM:viz.CYAN,
	skeleton.AVATAR_L_FOREARM:viz.BLUE,
	skeleton.AVATAR_L_HAND:viz.PURPLE,

	skeleton.AVATAR_R_CLAVICLE:viz.GREEN,
	skeleton.AVATAR_R_UPPER_ARM:viz.CYAN,
	skeleton.AVATAR_R_FOREARM:viz.BLUE,
	skeleton.AVATAR_R_HAND:viz.PURPLE,

	skeleton.AVATAR_L_THIGH:viz.CYAN,
	skeleton.AVATAR_L_CALF:viz.BLUE,
	skeleton.AVATAR_L_FOOT:viz.PURPLE,

	skeleton.AVATAR_R_THIGH:viz.CYAN,
	skeleton.AVATAR_R_CALF:viz.BLUE,
	skeleton.AVATAR_R_FOOT:viz.PURPLE,
}

DOF_POS = 1
DOF_ORI = 2
DOF_6DOF = 3

ASSIGNED_TRACKER = 0
ASSIGNED_PARENT = 1
ASSIGNED_DOF = 2

PRIORITY_ANIMATOR = 21

KINECT_V1 = 1
KINECT_V2 = 2

KINECT_MODE_FULL_BODY = 0
KINECT_MODE_UPPER_BODY = 1

_KINECT_TRACKER_INDEX_DICT = collections.OrderedDict()
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_PELVIS] = 3
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_TORSO] = 2
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_HEAD] = 1

_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_UPPER_ARM] = 4
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_FOREARM] = 5
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_HAND] = 6

_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_UPPER_ARM] = 8
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_FOREARM] = 9
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_HAND] = 10

_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_THIGH] = 12
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_CALF] = 13
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_L_FOOT] = 14

_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_THIGH] = 16
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_CALF] = 17
_KINECT_TRACKER_INDEX_DICT[skeleton.AVATAR_R_FOOT] = 18

DEGREES_TO_RADIANS = math.pi/180.0

KINECT_PPT = 1
KINECT_FAAST_V1 = 2
KINECT_FAAST_V2 = 3
KINECT_SM = 4

_KINECT_V2_PPT_MAPPING_DICT = collections.OrderedDict()
#
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_PELVIS] = 1
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_TORSO] = 2
#_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_NECK] = 3
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_HEAD] = 4
#
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_CLAVICLE] = 5
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_UPPER_ARM] = 6
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_FOREARM] = 7
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_HAND] = 8
#
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_CLAVICLE] = 9
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_UPPER_ARM] = 10
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_FOREARM] = 11
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_HAND] = 12
#
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_THIGH] = 14
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_CALF] = 15
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_L_FOOT] = 16
#
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_THIGH] = 18
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_CALF] = 19
_KINECT_V2_PPT_MAPPING_DICT[skeleton.AVATAR_R_FOOT] = 20

_KINECT_V2_PPT_TPOSE_TRANSFORM_DICT = {
	skeleton.AVATAR_PELVIS:[180.0, 0.0, 0.0],
	skeleton.AVATAR_TORSO:[180.0, 0.0, 0.0],
	#skeleton.AVATAR_NECK:[180.0, 0.0, 0.0],
	skeleton.AVATAR_HEAD:[0.0, 0.0, 0.0],

	skeleton.AVATAR_L_CLAVICLE:[180.0, 0.0, -90.0],
	skeleton.AVATAR_L_UPPER_ARM:[0.0, 90.0, 90.0],
	skeleton.AVATAR_L_FOREARM:[180.0, 0.0, -90.0],
	skeleton.AVATAR_L_HAND:[180.0, 0.0, -90.0],

	skeleton.AVATAR_R_CLAVICLE:[-180.0, 0.0, 90.0],
	skeleton.AVATAR_R_UPPER_ARM:[0.0, 90.0, -90.0],
	skeleton.AVATAR_R_FOREARM:[0.0, 180.0, -90.0],
	skeleton.AVATAR_R_HAND:[0.0, 180.0, -90.0],

	#hip:[135.0, 0.0, -90.0],
	skeleton.AVATAR_L_THIGH:[90.0, 0.0, 180.0],
	skeleton.AVATAR_L_CALF:[90.0, 0.0, 180.0],
	skeleton.AVATAR_L_FOOT:[0.0, 0.0, 0.0],

	#hip:[135.0, 0.0, 90.0],
	skeleton.AVATAR_R_THIGH:[-90.0, 0.0, 180.0],
	skeleton.AVATAR_R_CALF:[-90.0, 0.0, 180.0],
	skeleton.AVATAR_R_FOOT:[0.0, 0.0, 0.0],
}

# FAAST V1
_FAAST_V1_MAPPING_DICT = collections.OrderedDict()
#
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_PELVIS] = 4
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_TORSO] = 3
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_HEAD] = 1
#
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_UPPER_ARM] = 5
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_FOREARM] = 6
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_HAND] = 8#ori from wrists from faast v1 and kinect v1 is too random, using hand ori instead
#
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_UPPER_ARM] = 11
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_FOREARM] = 12
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_HAND] = 14#ori from wrists from faast v1 and kinect v1 is too random, using hand ori instead
#
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_THIGH] = 16
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_CALF] = 17
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_L_FOOT] = 18
#
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_THIGH] = 20
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_CALF] = 21
_FAAST_V1_MAPPING_DICT[skeleton.AVATAR_R_FOOT] = 22

# FAAST V2
_FAAST_V2_MAPPING_DICT = collections.OrderedDict()
#
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_PELVIS] = 3
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_TORSO] = 2
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_HEAD] = 1
#
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_UPPER_ARM] = 4
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_FOREARM] = 5
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_HAND] = 6
#
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_UPPER_ARM] = 8
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_FOREARM] = 9
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_HAND] = 10
#
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_THIGH] = 12
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_CALF] = 13
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_L_FOOT] = 14
#
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_THIGH] = 16
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_CALF] = 17
_FAAST_V2_MAPPING_DICT[skeleton.AVATAR_R_FOOT] = 18


def _isInFrustum(pos, left, right, bottom, top, near, far):
	"""Returns True if the given position is inside of the
	given frustum
	"""
	if pos[2] != 0:
		# z check
		if pos[2] < -far:
			return False
		if pos[2] > -near:
			return False
		# x check
		x = pos[0]/pos[2]
		rn = right/near
		ln = left/near
		if x > rn:
			return False
		if x < ln:
			return False
		# y check
		y = pos[1]/pos[2]
		tn = top/near
		bn = bottom/near
		if y > tn:
			return False
		if y < bn:
			return False
	else:
		return False
	return True


class _Animator(object):
	"""Base animator class. Meant to be extended."""
	def __init__(self, avatarObject):
		self._avatar = avatarObject
		self._updateEvent = vizact.onupdate(PRIORITY_ANIMATOR, self._update)

		self._movementScalePositionAdjustment = vizmat.Vector([0, 0, 0])
		self._movementScale = vizmat.Vector([1.0]*3)

	def requestUpdate(self):
		"""Call this method to request that that avatar animator
		performs an update. The update implementation is up to the particular
		animator implementation. There is no guarantee that the animator will
		update this frame.
		"""
		self._update()

	def _getScaledMovement(self):
		"""Internal method which returns the scaled position determined by
		applying the movement scale to the avatar's position.
		"""
		return self._avatar.getPosition()

	def getMovementScale(self):
		"""Returns the movement scale of the avatar as a list
		@return []
		"""
		return self._movementScale

	def remove(self):
		"""Removes the animator"""
		if self._updateEvent is not None:
			self._updateEvent.remove()
		self._updateEvent = None

	def setMovementScale(self, movementScale):
		"""Sets the movement scale used by the animator"""
		self._movementScalePositionAdjustment += self._getScaledMovement()
		self._movementScale = vizmat.Vector(movementScale)
		self._movementScalePositionAdjustment -= self._getScaledMovement()

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		pass


class _SkeletonBased(_Animator):
	"""A generic skeleton based animator class. Provides support for a skeleton
	object and a tracker assignment dictionary."""
	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict,
					**kwargs):
		super(_SkeletonBased, self).__init__(avatarObject=avatarObject, **kwargs)

		self._skeleton = skeletonObject
		self._trackerAssignmentDict = trackerAssignmentDict

		self._tposeDict = {}

		# remove all items that aren't valid
		popList = []
		for bp in self._trackerAssignmentDict:
			if not self._isValid(bp):
				popList.append(bp)
		for bp in popList:
			self._trackerAssignmentDict.pop(bp)

		self._usableDoFDict = self._createDefaultUsableDoFDict()

		self._boneDict = self._skeleton.getBoneDict()

	def _isValid(self, name):
		"""Internal method which returns true if the body part has a
		tracker assigned to it. It does not check that the tracker is
		functioning properly.
		"""
		if name in self._trackerAssignmentDict and self._trackerAssignmentDict[name]:
			if self._trackerAssignmentDict[name][ASSIGNED_TRACKER]:
				return True
		return False

	def _createDefaultUsableDoFDict(self):
		"""Internal method which returns a dictionary of the default usable
		degrees of freedom for each body part. This can be used to limit
		the information used by each joint.
		"""
		return {}

	def getRequiredBoneList(self):
		"""Returns a list of the bones are required by the animator.

		@return []
		"""
		return []

	def getOptionalBoneList(self):
		"""Returns a list of the bones which can be used for the animation, but
		are not required.

		@return []
		"""
		return []

	def _getScaledMovement(self):
		"""Internal method which returns the scaled position determined by
		applying the movement scale to the avatar's position.
		"""
		pos = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getPosition())
		pos[0] *= self._movementScale[0]
		pos[1] *= self._movementScale[1]
		pos[2] *= self._movementScale[2]
		return pos

	def getTrackerAssignmentDict(self):
		"""Returns the tracker assignment dictionary. The dictionary is indexed
		by the names of bones. The values are 3-tuples [ASSIGNED_TRACKER,
		ASSIGNED_PARENT, ASSIGNED_DOF]

		@return {}
		"""
		return self._trackerAssignmentDict


class _HeadBased(_SkeletonBased):
	"""Generic head-based animator. Provides scale adjustments, based
	on the head position of the avatar. By default also determines the
	heading rotation of the avatar based on the head's heading value.
	"""
	def _createDefaultUsableDoFDict(self):
		"""Internal method which returns a dictionary of the default usable
		degrees of freedom for each body part. This can be used to limit
		the information used by each joint.
		"""
		dofDict = super(_HeadBased, self)._createDefaultUsableDoFDict()
		dofDict.update({skeleton.AVATAR_HEAD: DOF_6DOF})
		return dofDict

	def _getAdjustedMatrix(self, bodyPart, parentMode=viz.ABS_GLOBAL, dof=None, useGlobal=False, useParent=True):
		"""Returns the tracker assignment dictionary. The dictionary is indexed
		by the names of bones. The values are 3-tuples [ASSIGNED_TRACKER,
		ASSIGNED_PARENT, ASSIGNED_DOF]

		@return {}
		"""
		assignmentDict = self._trackerAssignmentDict[bodyPart]
		# if using pos only tracker use ori from parent
		if dof is None:
			dof = assignmentDict[ASSIGNED_DOF]
		parentName = assignmentDict[ASSIGNED_PARENT]

		trackerMat = assignmentDict[ASSIGNED_TRACKER].getMatrix()
		if dof == DOF_POS:
			trackerMat.setQuat(0, 0, 0, 1)
		if dof == DOF_ORI:
			trackerMat.setPosition(0, 0, 0)

		mat = vizmat.Transform()
		if useParent:# world relative
			if parentName:
				mat = self._boneDict[parentName].getMatrix(viz.ABS_GLOBAL)
				mat.preMult(self._skeleton.getShift(parentName))
			else:# avatar relative
				parents = self._avatar.getParents()
				if parents:
					mat = parents[0].getMatrix(parentMode)
				else:
					mat = vizmat.Transform()
				mat.setScale([1.0]*3)
		mat.preMult(trackerMat)

		if not useGlobal:
			if bodyPart in self._tposeDict:
				mat.preMult(self._tposeDict[bodyPart])
#			else:
#				mat.preMult(self._skeleton.getShiftInv(bodyPart))

		return mat

	def _getBodyYaw(self, mat):
		"""Computes and returns the yaw/heading of the body, based on the
		tracker data.
		"""
		# project the z component of the head tracker onto the vertical axis, if the abs is close to one
		# then base the yaw on the x component instead.
		vec = vizmat.Vector(mat.preMultVec([0, 0, 1]))
		if abs(vec[1]) < 0.95:
			# it's safe to use z, so get the yaw from the z vector's projection.
			vec[1] = 0
			vec.normalize()
			# get the angle
			yaw = inverse_kinematics._signedAngle(vec, [0, 0, 1], [0, 1, 0])
		else:
			# z's too close to y so use the x component instead
			vec = vizmat.Vector(mat.preMultVec([1, 0, 0]))
			vec[1] = 0
			vec.normalize()
			# get the angle
			yaw = inverse_kinematics._signedAngle(vec, [0, 0, 1], [0, 1, 0])
			# add on 90
			yaw -= 90
		mat.postYaw()
		return yaw

	def _getEyeHeight(self):
		"""Returns the eye height for the head-based avatar"""
		return skeleton.DEFAULT_EYE_HEIGHT

	def getRequiredBoneList(self):
		"""Returns a list of the bones are required by the animator.

		@return []
		"""
		return [skeleton.AVATAR_HEAD]


class Disembodied(_HeadBased):
	"""An animator for a disembodied head"""
	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict,
					**kwargs):

		super(Disembodied, self).__init__(avatarObject=avatarObject,
											skeletonObject=skeletonObject,
											trackerAssignmentDict=trackerAssignmentDict,
											**kwargs)

		self._update()

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		if not self._isValid(skeleton.AVATAR_HEAD):
			return

		super(Disembodied, self)._update()

		headTracker = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER]
		headEuler = headTracker.getEuler()
		bodyPos = self._getScaledMovement()
		bodyPos += self._movementScalePositionAdjustment
		eyeHeight = bodyPos[1]
		bodyPos[1] = 0

		# assign position and yaw to base
		self._boneDict[skeleton.AVATAR_BASE].setEuler([headEuler[0], 0, 0])
		self._boneDict[skeleton.AVATAR_BASE].setPosition(bodyPos)

		# assign eyeHeight, pitch and roll to head
		self._boneDict[skeleton.AVATAR_HEAD].setPosition([0, eyeHeight, 0])
		self._boneDict[skeleton.AVATAR_HEAD].setEuler(0, headEuler[1], headEuler[2])

		# now update the body parts
		for bodyPart in [skeleton.AVATAR_R_HAND, skeleton.AVATAR_L_HAND]:
			if bodyPart in self._trackerAssignmentDict and bodyPart in self._boneDict and self._boneDict[bodyPart]:
				mat = self._getAdjustedMatrix(bodyPart, parentMode=viz.ABS_GLOBAL)
				self._boneDict[bodyPart].setMatrix(mat, viz.ABS_GLOBAL)

	def getOptionalBoneList(self):
		"""Returns a list of the bones which can be used for the animation, but
		are not required.

		@return []
		"""
		return [skeleton.AVATAR_L_HAND, skeleton.AVATAR_R_HAND]


class Direct(_HeadBased):
	"""An animator to directly map transformations for bones"""
	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict,
					**kwargs):

		super(Direct, self).__init__(avatarObject=avatarObject,
											skeletonObject=skeletonObject,
											trackerAssignmentDict=trackerAssignmentDict,
											**kwargs)

		self._update()

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		if not self._isValid(skeleton.AVATAR_HEAD):
			return

		super(Direct, self)._update()

		self._avatar.setMatrix(vizmat.Transform())
		parentMat = self._avatar.getMatrix(viz.ABS_GLOBAL)

		headTracker = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER]
		headEuler = headTracker.getEuler()
		bodyPos = self._getScaledMovement()
		bodyPos += self._movementScalePositionAdjustment
		eyeHeight = bodyPos[1]
		bodyPos[1] = 0

		# assign position and yaw to base
		self._boneDict[skeleton.AVATAR_BASE].setEuler([headEuler[0], 0, 0])
		self._boneDict[skeleton.AVATAR_BASE].setPosition(bodyPos)

		# assign eyeHeight, pitch and roll to head
		headMat = vizmat.Transform(headTracker.getMatrix())
#		headMat.setPosition([0, eyeHeight, 0])
#		headMat.setEuler(0, headEuler[1], headEuler[2])
		headMat.postMult(parentMat)
		headMat.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_HEAD))
		self._boneDict[skeleton.AVATAR_HEAD].setMatrix(headMat, viz.ABS_GLOBAL)

		# now update the body parts
		for bodyPart in [skeleton.AVATAR_R_HAND, skeleton.AVATAR_L_HAND]:
			if bodyPart in self._trackerAssignmentDict and bodyPart in self._boneDict and self._boneDict[bodyPart]:
				mat = self._getAdjustedMatrix(bodyPart, parentMode=viz.ABS_GLOBAL)
				mat.preMult(self._skeleton.getShiftInv(bodyPart))
				self._boneDict[bodyPart].setMatrix(mat, viz.ABS_GLOBAL)

	def getOptionalBoneList(self):
		"""Returns a list of the bones which can be used for the animation, but
		are not required.

		@return []
		"""
		return [skeleton.AVATAR_L_HAND, skeleton.AVATAR_R_HAND]


class InverseKinematics(_HeadBased):
	"""An animator which uses inverse kinematics"""

	DEBUG_TRACKER = 1
	DEBUG_CORRECTED = 2
	DEBUG_BONE = 4
	DEBUG_SHIFTED = 8

	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict,
					debug=False,
					defaultPostureEnabled=True,
					debugMask=4):

		super(InverseKinematics, self).__init__(avatarObject=avatarObject,
													skeletonObject=skeletonObject,
													trackerAssignmentDict=trackerAssignmentDict)

		self._boneDict = self._skeleton.getBoneDict()
		self._jointDict = self._skeleton.getJointDict()
		self._adjustedMatrixDict = {}
		self._tposeDict = {}
		self._backupTposeDict = {}
		self._scale = 1.0
		self._debug = debug
		self._defaultPostureEnabled = defaultPostureEnabled
		self._debugMask = debugMask
		self._trustDict = {}

		self._debugTracker = {}
		self._debugCorrected = {}
		self._debugBone = {}
		self._debugShifted = {}

		self._standInTracker = {}

		self._autoScale = {}
		self._ratchetLength = {}
		for bone in self._boneDict:
			self._autoScale[bone] = False
			self._ratchetLength[bone] = False

		self._leftArmIK = None
		if skeleton.AVATAR_L_HAND in self._trackerAssignmentDict:
			mat = vizmat.Transform()
			mat.setQuat(self._skeleton._defaultTransGlobal[skeleton.AVATAR_L_UPPER_ARM])
			forearmTracker = None
			if skeleton.AVATAR_L_FOREARM in self._trackerAssignmentDict:
				forearmTracker = self._trackerAssignmentDict[skeleton.AVATAR_L_FOREARM][ASSIGNED_TRACKER]
			upperArmTracker = None
			if skeleton.AVATAR_L_UPPER_ARM in self._trackerAssignmentDict:
				upperArmTracker = self._trackerAssignmentDict[skeleton.AVATAR_L_UPPER_ARM][ASSIGNED_TRACKER]
			self._leftArmIK = inverse_kinematics._Arm(self._avatar,
														handTracker=self._trackerAssignmentDict[skeleton.AVATAR_L_HAND][ASSIGNED_TRACKER],
														forearmTracker=forearmTracker,
														upperArmTracker=upperArmTracker,
														uparm=self._boneDict[skeleton.AVATAR_L_UPPER_ARM],
														lowarm=self._boneDict[skeleton.AVATAR_L_FOREARM],
														hand=self._boneDict[skeleton.AVATAR_L_HAND],
														elbow=self._jointDict[skeleton.JOINT_L_ELBOW],
														shoulder=self._jointDict[skeleton.JOINT_L_SHOULDER],
														wrist=self._jointDict[skeleton.JOINT_L_WRIST],
														rotOffsetMat=mat,
														sign=-1,
														debug=False)

		self._rightArmIK = None
		if skeleton.AVATAR_R_HAND in self._trackerAssignmentDict:
			mat = vizmat.Transform()
			mat.setQuat(self._skeleton._defaultTransGlobal[skeleton.AVATAR_R_UPPER_ARM])
			mat.postEuler([180, 0, 0])
			forearmTracker = None
			if skeleton.AVATAR_R_FOREARM in self._trackerAssignmentDict:
				forearmTracker = self._trackerAssignmentDict[skeleton.AVATAR_R_FOREARM][ASSIGNED_TRACKER]
			upperArmTracker = None
			if skeleton.AVATAR_R_UPPER_ARM in self._trackerAssignmentDict:
				upperArmTracker = self._trackerAssignmentDict[skeleton.AVATAR_R_UPPER_ARM][ASSIGNED_TRACKER]
			self._rightArmIK = inverse_kinematics._Arm(self._avatar,
														handTracker=self._trackerAssignmentDict[skeleton.AVATAR_R_HAND][ASSIGNED_TRACKER],
														forearmTracker=forearmTracker,
														upperArmTracker=upperArmTracker,
														uparm=self._boneDict[skeleton.AVATAR_R_UPPER_ARM],
														lowarm=self._boneDict[skeleton.AVATAR_R_FOREARM],
														hand=self._boneDict[skeleton.AVATAR_R_HAND],
														elbow=self._jointDict[skeleton.JOINT_R_ELBOW],
														shoulder=self._jointDict[skeleton.JOINT_R_SHOULDER],
														wrist=self._jointDict[skeleton.JOINT_R_WRIST],
														rotOffsetMat=mat,
														sign=1,
														debug=False)

		self._leftLegIK = None
		if skeleton.AVATAR_L_FOOT in self._trackerAssignmentDict:
			self._addLeftLegIK()

		self._rightLegIK = None
		if skeleton.AVATAR_R_FOOT in self._trackerAssignmentDict:
			self._addRightLegIK()

		if self._debug:
			self._addDebugBalls()

		self._update()

	def _addDebugBalls(self):
		for bodyPart in self._trackerAssignmentDict:
			if bodyPart not in self._debugBone:
				self._debugBone[bodyPart] = inverse_kinematics.makeDebugBall(0.05, viz.YELLOW)
			if bodyPart not in self._debugCorrected:
				self._debugCorrected[bodyPart] = inverse_kinematics.makeDebugBall(0.05, viz.GREEN)
			if bodyPart not in self._debugTracker:
				self._debugTracker[bodyPart] = inverse_kinematics.makeDebugBall(0.05, viz.RED)
			if bodyPart not in self._debugShifted:
				self._debugShifted[bodyPart] = inverse_kinematics.makeDebugBall(0.05, viz.PURPLE)
#		self._debugAvatar = inverse_kinematics.makeDebugCylinder(0.03, 0.15, viz.BLUE)
#		self._debugRoot = inverse_kinematics.makeDebugCylinder(0.03, 0.15, viz.PURPLE)
		self.setDebugMask(self._debugMask)

	def _addLeftLegIK(self):
		"""Internal function to add right leg ik"""
		mat = vizmat.Transform()
		mat.setQuat(self._skeleton._defaultTransLocal[skeleton.AVATAR_L_THIGH])
		calfTracker = None
		if skeleton.AVATAR_L_CALF in self._trackerAssignmentDict:
			calfTracker = self._trackerAssignmentDict[skeleton.AVATAR_L_CALF][ASSIGNED_TRACKER]
		self._leftLegIK = inverse_kinematics._Leg(self._avatar,
													footTracker=self._trackerAssignmentDict[skeleton.AVATAR_L_FOOT][ASSIGNED_TRACKER],
													calfTracker=calfTracker,
													thigh=self._boneDict[skeleton.AVATAR_L_THIGH],
													calf=self._boneDict[skeleton.AVATAR_L_CALF],
													foot=self._boneDict[skeleton.AVATAR_L_FOOT],
													hip=self._jointDict[skeleton.JOINT_L_HIP],
													knee=self._jointDict[skeleton.JOINT_L_KNEE],
													ankle=self._jointDict[skeleton.JOINT_L_ANKLE],
													rotOffsetMat=mat,
													debug=False)

	def _addRightLegIK(self):
		"""Internal function to add right leg ik"""
		mat = vizmat.Transform()
		mat.setQuat(self._skeleton._defaultTransLocal[skeleton.AVATAR_R_THIGH])
		calfTracker = None
		if skeleton.AVATAR_R_CALF in self._trackerAssignmentDict:
			calfTracker = self._trackerAssignmentDict[skeleton.AVATAR_R_CALF][ASSIGNED_TRACKER]
		self._rightLegIK = inverse_kinematics._Leg(self._avatar,
													footTracker=self._trackerAssignmentDict[skeleton.AVATAR_R_FOOT][ASSIGNED_TRACKER],
													calfTracker=calfTracker,
													thigh=self._boneDict[skeleton.AVATAR_R_THIGH],
													calf=self._boneDict[skeleton.AVATAR_R_CALF],
													foot=self._boneDict[skeleton.AVATAR_R_FOOT],
													hip=self._jointDict[skeleton.JOINT_R_HIP],
													knee=self._jointDict[skeleton.JOINT_R_KNEE],
													ankle=self._jointDict[skeleton.JOINT_R_ANKLE],
													rotOffsetMat=mat,
													debug=False)

	def addStandInTracker(self, bodyPart, dof):
		"""Adds a stand-in tracker, which is essentially a dummy tracker used so
		that full bodied avatars can have reasonable movement of their legs, etc
		"""
		if not bodyPart in self._trackerAssignmentDict and bodyPart in self._skeleton.getBoneDict():
			self._standInTracker[bodyPart] = viz.addGroup()#inverse_kinematics.makeDebugBall(0.15, viz.YELLOW)
			self._standInTracker[bodyPart].disable(viz.INTERSECTION)
			self._trackerAssignmentDict[bodyPart] = [None, None, 0]
			self._trackerAssignmentDict[bodyPart][ASSIGNED_TRACKER] = self._standInTracker[bodyPart]
			self._trackerAssignmentDict[bodyPart][ASSIGNED_PARENT] = None
			self._trackerAssignmentDict[bodyPart][ASSIGNED_DOF] = dof
			self.setTrusted(bodyPart, DOF_6DOF)

		# check for updated ik components
		if skeleton.AVATAR_L_FOOT in self._standInTracker:
			self._addLeftLegIK()
		if skeleton.AVATAR_R_FOOT in self._standInTracker:
			self._addRightLegIK()

		if self._debug:
			self._addDebugBalls()

	def autoAddStandInTrackers(self):
		"""Will attempt to automatically add the appropriate stand-in tackers."""
		for bodyPart, dof in [(skeleton.AVATAR_L_FOOT, DOF_6DOF),
							(skeleton.AVATAR_R_FOOT, DOF_6DOF)]:
			self.addStandInTracker(bodyPart, dof)

		if skeleton.AVATAR_L_FOOT in self._trackerAssignmentDict\
				and skeleton.AVATAR_R_FOOT in self._trackerAssignmentDict:
			self.addStandInTracker(skeleton.AVATAR_PELVIS, DOF_POS)

	def _createDefaultUsableDoFDict(self):
		"""Internal method which returns a dictionary of the default usable
		degrees of freedom for each body part. This can be used to limit
		the information used by each joint.
		"""
		dofDict = super(InverseKinematics, self)._createDefaultUsableDoFDict()
		dofDict.update({
			skeleton.AVATAR_L_HAND: DOF_6DOF,
			skeleton.AVATAR_R_HAND: DOF_6DOF,
			skeleton.AVATAR_L_FOREARM: DOF_POS,
			skeleton.AVATAR_R_FOREARM: DOF_POS,
			skeleton.AVATAR_L_FOOT: DOF_6DOF,
			skeleton.AVATAR_R_FOOT: DOF_6DOF,
			skeleton.AVATAR_L_THIGH: DOF_POS,
			skeleton.AVATAR_R_THIGH: DOF_POS,
			skeleton.AVATAR_L_CALF: DOF_POS,
			skeleton.AVATAR_R_CALF: DOF_POS,
			skeleton.AVATAR_PELVIS: DOF_ORI,
		})
		return dofDict

	def getCalibration(self):
		"""Returns the calibration data for the ik"""
		boneLengthDict = {}
		for bone in self._skeleton.getBoneDict():
			if bone != skeleton.AVATAR_ROOT and bone != skeleton.AVATAR_BASE:
				boneLengthDict[bone] = self._skeleton.getBoneLength(bone)
		dataDict = {
			'scale': self._scale,
			'tposeDict': copy.deepcopy(self._tposeDict),
			'boneLengthDict': boneLengthDict
		}
		return dataDict

	def getDebugMask(self):
		"""Returns the debug mask, indicating which combination of debug markers
		are currently being used
		"""
		return self._debugMask

	def _getPelvisRotationToPoint(self, parentMat):
		"""Gets and returns the rotation and rotation axis needed for the waist
		of the avatar to point at the waist tracker's pos.
		Returned data is tuple of (rotation, axisAngle)

		@return ()
		"""
		# if it's position based:
		mat = self._getAdjustedMatrix(skeleton.AVATAR_HEAD, useParent=False)
		mat.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_HEAD))
		#mat.postMult(parentMatInv)
		headTrackerPos = vizmat.Vector(mat.getPosition())
		mat = self._getAdjustedMatrix(skeleton.AVATAR_PELVIS, useParent=False)
		mat.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_PELVIS))
		pelvisTrackerPos = vizmat.Vector(mat.getPosition())

		parentMatInv = parentMat.inverse()

		mat = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		mat.postMult(parentMatInv)
		headCorrectedBonePos = mat.getPosition()

		mat = self._boneDict[skeleton.AVATAR_PELVIS].getMatrix(viz.ABS_GLOBAL)
		mat.preMult(self._skeleton.getShift(skeleton.AVATAR_PELVIS))
		mat.postMult(parentMatInv)
		pelvisCorrectedBonePos = mat.getPosition()

		bvec = vizmat.Vector(headCorrectedBonePos) - pelvisCorrectedBonePos
		tvec = vizmat.Vector(headTrackerPos) - pelvisTrackerPos

		# now get the pitch as the angle between the two vectors projected onto the yz plane
		v1 = bvec.normalize()
		v2 = tvec.normalize()
		rotAxis = v1.cross(v2).normalize()
		rot = (math.atan2(v1.cross(v2).length(), v1*v2) *180.0/math.pi) * math.copysign(1, v1.cross(v2)*rotAxis)

		return rot, rotAxis, tvec

	def _getEulerFromWaist(self, parentMat):
		"""Uses both position and orientation of waist tracker to get a final
		orientation for the waist.

		@headShift the shift that needs to be applied to the head
		"""
		dof = self._trackerAssignmentDict[skeleton.AVATAR_PELVIS][ASSIGNED_DOF]
		mat = vizmat.Transform()

		# if it has only position, use that and add in yaw augmentation from head
		# if it's 6DOF, use position first, then augment with ori
		if dof == DOF_POS or dof == DOF_6DOF:
			# use the position information from the waist since it's likely
			# the most reliable source of data
			rot, rotAxis, upAxis = self._getPelvisRotationToPoint(parentMat)
			rotMat = vizmat.Transform()
			rotMat.postAxisAngle(rotAxis[0], rotAxis[1], rotAxis[2], rot)

			if dof == DOF_POS:
				pelvisMat = self._getAdjustedMatrix(skeleton.AVATAR_HEAD, useParent=False)
				# vec sum of feet
				if skeleton.AVATAR_R_FOOT in self._trackerAssignmentDict\
						and skeleton.AVATAR_L_FOOT in self._trackerAssignmentDict:
					parentMatInv = parentMat.inverse()
					rfMat = vizmat.Transform(parentMatInv)
					rfMat.preMult(self._getAdjustedMatrix(skeleton.AVATAR_R_FOOT))
					lfMat = vizmat.Transform(parentMatInv)
					lfMat.preMult(self._getAdjustedMatrix(skeleton.AVATAR_L_FOOT))

					rfPos = vizmat.Vector(rfMat.getPosition())
					lfPos = vizmat.Vector(lfMat.getPosition())
#					rfPos = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_R_FOOT][ASSIGNED_TRACKER].getPosition())
#					lfPos = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_L_FOOT][ASSIGNED_TRACKER].getPosition())
					upVec = vizmat.Vector(pelvisMat.getPosition()) - (rfPos+lfPos)/2.0
#					rfForward = self._trackerAssignmentDict[skeleton.AVATAR_R_FOOT][ASSIGNED_TRACKER].getMatrix().getForward()
#					lfForward = self._trackerAssignmentDict[skeleton.AVATAR_L_FOOT][ASSIGNED_TRACKER].getMatrix().getForward()
					rfForward = rfMat.getForward()
					lfForward = lfMat.getForward()
					forward = (vizmat.Vector(rfForward) + vizmat.Vector(lfForward)) / 2.0
					forward += pelvisMat.getForward()
					forward /= 2
					oriMat = vizmat.Transform()
					oriMat.makeLookAt([0, 0, 0], forward, upVec)
					pelvisMat.setQuat(oriMat.getQuat())
			else:
				# alt use pelvis info if t-pose calibrated, otherwise use head
				pelvisMat = self._getAdjustedMatrix(skeleton.AVATAR_PELVIS, useParent=False)

			# we only need the orientation
			pmo = vizmat.Transform()
			pmo.setQuat(pelvisMat.getQuat())
			# get the projected Z vector for the physical tracker
			targetZ = vizmat.Vector(pmo.preMultVec([0, 0, 1]))
			# project pelvisZ onto rotation axis plane
			targetZ -= upAxis*(targetZ*upAxis)
			targetZ = targetZ.normalize()

			# find angle between current and target vectors
			currentZ = vizmat.Vector(rotMat.preMultVec([0, 0, 1]))
			headingAdjustment = inverse_kinematics._signedAngle(targetZ, currentZ, upAxis)
			# up axis as relative
			upAxis = rotMat.inverse().preMultVec(upAxis)
			rotMat.preAxisAngle(upAxis[0], upAxis[1], upAxis[2], headingAdjustment)

			mat.postMult(rotMat)

		# if it has only euler, use that
		elif dof == DOF_ORI:
			pelvisMat = self._getAdjustedMatrix(skeleton.AVATAR_PELVIS, useParent=False)
			mat.setQuat(pelvisMat.getQuat())

		# otherwise throw exception
		else:
			raise ValueError('Cannot determine dict for tracker')

		return mat

	def getScaleByEyeHeight(self, headTrackerHeight=-1):
		"""Return the desired scale for the avatar based on the head/eye tracker's
		height vs that of the avatar.

		@return float
		"""
		# set scale to default, and transform avatar to standard placement
		self.setScale(1.0)
		self._avatar.setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._skeleton.setToDefaultLayout()

		# get matrices for the tracker and bones for the head/eyes
		eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
		if headTrackerHeight <= 0:
			headTrackerMat = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()
			headTrackerMat.setQuat([0, 0, 0, 1])# remove rotation to prevent errors from slight head turn
			headTrackerHeight = headTrackerMat.getPosition()[1]
		newScale = headTrackerHeight / eyeBoneMatrix.getPosition()[1]

		# return to the previous scale
		self.setScale(self._scale)
		return newScale

	def _getYawAugmentation(self):
		"""Returns the yaw/heading augmentation determined by the position
		of the hands relative to the avatar's head/body.

		@return float
		"""
		if not skeleton.AVATAR_R_HAND in self._trackerAssignmentDict or not skeleton.AVATAR_L_HAND in self._trackerAssignmentDict:
			return 0

		# get the final transformation for the points
		rP = self._getAdjustedMatrix(skeleton.AVATAR_R_HAND).getPosition()
		lP = self._getAdjustedMatrix(skeleton.AVATAR_L_HAND).getPosition()
		avatarMat = self._avatar.getMatrix(viz.ABS_GLOBAL)
		ami = avatarMat.inverse()
		rP = ami.preMultVec(rP)
		lP = ami.preMultVec(lP)

		# transfer into the avatar's coordinate system
		dist = vizmat.Distance(rP, lP)
		if dist < 0.001:
			return 0

		vec = vizmat.Vector(rP) - lP
		avatarMat.setScale([1.0]*3)
#		vec = vizmat.Vector(avatarMat.inverse().preMultVec(vec))
		vec[1] = 0
		vec.normalize()
		yaw = vizmat.AngleBetweenVector([0, 0, 1], vec)-90.0
		yaw *= min(1.0, max(0, (dist-0.05)))
		yaw = min(60, yaw)
		return yaw

	def remove(self):
		"""Removes the animator and allocated resources such as vrpn trackers,
		links, etc.
		"""
		self._boneDict[skeleton.AVATAR_ROOT].unlock(recurse=True)
		super(InverseKinematics, self).remove()
		if self._debug:
			self._removeDebugBalls()

	def _removeDebugBalls(self):
		"""Removes all the debug balls"""
		if self._debug:
			for bodyPart in self._trackerAssignmentDict:
				try:
					self._debugBone[bodyPart].remove()
				except KeyError:
					pass
				try:
					self._debugCorrected[bodyPart].remove()
				except KeyError:
					pass
				try:
					self._debugTracker[bodyPart].remove()
				except KeyError:
					pass
				try:
					self._debugShifted[bodyPart].remove()
				except KeyError:
					pass
#			self._debugAvatar.remove()
#			self._debugRoot.remove()

	def setAutoscaleEnabled(self, boneName, state):
		"""Sets the enabled state for autoscaling for the given target
		(e.g. skeleton.AVATAR_R_HAND stretches the arm to the necessary length)
		"""
		if state is viz.TOGGLE:
			state = not self._autoScale[boneName]
		self._autoScale[boneName] = state

	def setRatchetBoneLengthEnabled(self, boneName, state):
		"""Sets the enabled state for autoscaling for the given target
		(e.g. skeleton.AVATAR_R_HAND stretches the arm to the necessary length)
		"""
		if state is viz.TOGGLE:
			state = not self._ratchetLength[boneName]
		self._ratchetLength[boneName] = state

	def setCalibration(self, dataDict):
		"""Applies a set of stored calibration data to the ik."""
		self.setScale(dataDict['scale'])
		self._tposeDict = copy.deepcopy(dataDict['tposeDict'])
		self._backupTposeDict = copy.deepcopy(self._tposeDict)
		for bone, length in dataDict['boneLengthDict'].iteritems():
			self._skeleton.setBoneLength(bone, length)

	def setDebugMask(self, mask):
		"""Sets which debug markers are visible"""
		self.setDebugTrackerBallsEnabled(mask & self.DEBUG_TRACKER == self.DEBUG_TRACKER)
		self.setDebugCorrectedBallsEnabled(mask & self.DEBUG_CORRECTED == self.DEBUG_CORRECTED)
		self.setDebugBoneBallsEnabled(mask & self.DEBUG_BONE == self.DEBUG_BONE)
		self.setDebugShiftedBallsEnabled(mask & self.DEBUG_SHIFTED == self.DEBUG_SHIFTED)

	def setDebugTrackerBallsEnabled(self, enabled):
		"""Shows/hides the balls matching the trackers."""
		for ball in self._debugTracker.values():
			ball.visible(enabled)
		if enabled:
			self._debugMask |= self.DEBUG_TRACKER
		else:
			self._debugMask &= ~self.DEBUG_TRACKER

	def setDebugCorrectedBallsEnabled(self, enabled):
		"""Shows/hides the balls matching the corrected tracker locations."""
		for ball in self._debugCorrected.values():
			ball.visible(enabled)
		if enabled:
			self._debugMask |= self.DEBUG_CORRECTED
		else:
			self._debugMask &= ~self.DEBUG_CORRECTED

	def setDebugBoneBallsEnabled(self, enabled):
		"""Shows/hides the balls matching the bones."""
		for ball in self._debugBone.values():
			ball.visible(enabled)
		if enabled:
			self._debugMask |= self.DEBUG_BONE
		else:
			self._debugMask &= ~self.DEBUG_BONE

	def setDebugShiftedBallsEnabled(self, enabled):
		"""Shows/hides the balls matching the bones shifted into a standard
		reference frame.
		"""
		for ball in self._debugShifted.values():
			ball.visible(enabled)
		if enabled:
			self._debugMask |= self.DEBUG_SHIFTED
		else:
			self._debugMask &= ~self.DEBUG_SHIFTED

	def setScale(self, scale):
		"""Sets the scale for the avatar"""
		self._scale = scale
		self._avatar.setScale([self._scale]*3)
		self._skeleton.setScale(self._scale)

	def setTrusted(self, bone, dof):
		"""Forces the tracker data to be trusted by clearing out the components
		of the tpose calibration given by dof.
		"""
		self._trustDict[bone] = dof
		try:
			self._tposeDict[bone] = vizmat.Transform(self._backupTposeDict[bone])
			if dof&DOF_ORI:
				self._tposeDict[bone].setQuat(0, 0, 0, 1)
			if dof&DOF_POS:
				self._tposeDict[bone].setPosition(0, 0, 0)
		except KeyError:
			pass
			#viz.logNotice("**Notice: Trusting the {} bone, but it was not found in the calibration. This is not a problem.".format(bone))

	def setUsableDoF(self, bone, dof):
		"""Allows a mask to be set for the degrees of freedom which can be used
		for a given body part. The resulting change in IK is dependent on the
		bone. For example setting the dof to DOF_POS for the hands, will lock
		the hands into line with the forearm.
		"""
		self._usableDoFDict[bone] = dof

	def tposeReset(self):
		"""Performs a calibration of the avatar based on the t-pose of the user"""
		# save autoscale settings
		oldAuto = copy.deepcopy(self._autoScale)

		# calibration is relative to eyes
		# set to default layout
		self.setScale(1.0)
		self._skeleton.setToDefaultLayout()

		# disable auto scaling
		for key in self._autoScale:
			self._autoScale[key] = False
			self._skeleton.resetBoneLength(key, fullRefresh=False)
		# reset the lengths for each of the bones
		self._skeleton.refreshJointLengths()

		# calibration is relative to eyes
		# set to default layout
		self.setScale(1.0)

		self._avatar.setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._skeleton.setToDefaultLayout()

		# get matrices for the tracker and bones for the head/eyes
		eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
		headTrackerMat = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()
		headTrackerMat.setQuat([0, 0, 0, 1])# remove rotation to prevent errors from slight head turn
		newScale = headTrackerMat.getPosition()[1] / eyeBoneMatrix.getPosition()[1]

		# scale avatar by the eye height difference so it touches the floor
		self.setScale(newScale)

		# get the difference in position between the eye bone and head tracker
		eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
		headDiff = vizmat.Transform(headTrackerMat)
		headDiff.setQuat([0, 0, 0, 1])# remove rotation to prevent errors from slight head turn
		headDiff.preTrans(-vizmat.Vector(eyeBoneMatrix.getPosition()))

		# shift the avatar by the difference in position so eyes are aligned
		self._avatar.setPosition(headDiff.getPosition(), viz.ABS_GLOBAL)

		# find transform from trackers to bones
		for bodyPart in self._trackerAssignmentDict:
			# get the matrix for the bone
			tPoseMatrix = self._boneDict[bodyPart].getMatrix(viz.ABS_GLOBAL)
			# mult by the shift matrix to put into global coordinates
			tPoseMatrix.preMult(self._skeleton.getShift(bodyPart))
			# mult by the inverse of the tracker matrix to get a final
			# difference matrix
			trackerMat = self._trackerAssignmentDict[bodyPart][ASSIGNED_TRACKER].getMatrix()
			dof = self._trackerAssignmentDict[bodyPart][ASSIGNED_DOF]
			if dof == DOF_POS:
				trackerMat.setQuat(0, 0, 0, 1)
			if dof == DOF_ORI:
				trackerMat.setPosition(0, 0, 0)
			tPoseMatrix.postMult(trackerMat.inverse())

			self._tposeDict[bodyPart] = tPoseMatrix
		self._tposeDict[skeleton.AVATAR_HEAD] = vizmat.Transform()

		self._backupTposeDict = copy.deepcopy(self._tposeDict)

		# re-apply any trusted settings
		for boneName, dof in self._trustDict.iteritems():
			self.setTrusted(boneName, dof)

		# refresh the default layout
		self._skeleton.setToDefaultLayout()

		# refresh the size of the bones given the new scale
		for key in self._autoScale:
			self._skeleton.resetBoneLength(key, fullRefresh=False)
		# reset the lengths for each of the bones
		self._skeleton.refreshJointLengths()

		# restore autoscaling settings
		self._autoScale = copy.deepcopy(oldAuto)

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		if not self._isValid(skeleton.AVATAR_HEAD):
			return

		try:
			if (self._autoScale[skeleton.AVATAR_R_FOREARM]
					and self._autoScale[skeleton.AVATAR_R_UPPER_ARM]):
				if (not self._ratchetLength[skeleton.AVATAR_R_FOREARM]
						and not self._ratchetLength[skeleton.AVATAR_R_UPPER_ARM]):
					self._skeleton.resetBoneLength(skeleton.AVATAR_R_FOREARM, fullRefresh=False)
					self._skeleton.resetBoneLength(skeleton.AVATAR_R_UPPER_ARM, fullRefresh=False)
					self._skeleton._jointDict[skeleton.JOINT_R_ELBOW].refreshLength()
					self._skeleton._jointDict[skeleton.JOINT_R_SHOULDER].refreshLength()
			if (self._autoScale[skeleton.AVATAR_L_FOREARM]
					and self._autoScale[skeleton.AVATAR_L_UPPER_ARM]):
				if (not self._ratchetLength[skeleton.AVATAR_L_FOREARM]
						and not self._ratchetLength[skeleton.AVATAR_L_UPPER_ARM]):
					self._skeleton.resetBoneLength(skeleton.AVATAR_L_FOREARM, fullRefresh=False)
					self._skeleton.resetBoneLength(skeleton.AVATAR_L_UPPER_ARM, fullRefresh=False)
					self._skeleton._jointDict[skeleton.JOINT_L_ELBOW].refreshLength()
					self._skeleton._jointDict[skeleton.JOINT_L_SHOULDER].refreshLength()
		except KeyError:
			pass

		# ensure avatar is at 0, 0, 0 locally
		self._avatar.setMatrix(vizmat.Transform())

		parentMat = self._avatar.getMatrix(viz.ABS_GLOBAL)
		parentInvMat = parentMat.inverse()

		eyeTrackerMatrix = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()

		self._avatar.setScale([self._scale]*3)

		self._skeleton.setToDefaultLayout()

		# pelvis stand-in tracker
		if skeleton.AVATAR_PELVIS in self._standInTracker:
			# determine the height of the avatar
			eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
			eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
			eyeBoneMatrix.preMult(parentInvMat)
			eyeBoneHeight = eyeBoneMatrix.getPosition()[1]

			# determine the eye tracker postion
			headTrackerMat = vizmat.Transform(eyeTrackerMatrix)
			headTrackerHeight = headTrackerMat.getPosition()[1]

			# base the waist orientation on the direction of the head
			shiftOriMat = vizmat.Transform()
			shiftOriMat.setEuler([headTrackerMat.getEuler()[0], 0, 0])

			# ensure the waist is above the ground

			# possibly base the waist orientation on the direction of the head
			# if we have feet trackers then waist should first be aligned between head and feet
			if skeleton.AVATAR_R_FOOT in self._trackerAssignmentDict\
				and skeleton.AVATAR_L_FOOT in self._trackerAssignmentDict:
				offset = vizmat.Vector([0]*3)
#				and not skeleton.AVATAR_R_FOOT in self._standInTracker\
#				and not skeleton.AVATAR_L_FOOT in self._standInTracker:
				# get the average of the feet
				rfPos = vizmat.Vector(self._getAdjustedMatrix(skeleton.AVATAR_R_FOOT, useParent=False).getPosition())
				lfPos = vizmat.Vector(self._getAdjustedMatrix(skeleton.AVATAR_L_FOOT, useParent=False).getPosition())
#				rfPos = vizmat.Vector(parentInvMat.preMultVec(rfPos))
#				lfPos = vizmat.Vector(parentInvMat.preMultVec(lfPos))
#				rfPos = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_R_FOOT][ASSIGNED_TRACKER].getPosition())
#				lfPos = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_L_FOOT][ASSIGNED_TRACKER].getPosition())
				center = (rfPos+lfPos)/2.0
				feetCenter = vizmat.Vector(center)
				# now get average between feet and head
#				headTrackerMat.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_HEAD))
				headCenter = vizmat.Vector(headTrackerMat.getPosition())# + (vizmat.Vector(headTrackerMat.getForward())*-0.5)
				# find the feet center to head vector in x, z
				center += headCenter
				center /= 2.0
				# find the offset
				xzVec = headCenter-feetCenter
				xzVec = xzVec.normalize()
				vertWeight = xzVec[1]
#				xzVec = vizmat.Vector(headTrackerMat.getForward())
				xzVec[1] = 0
				xzVec = xzVec.normalize()
#				if xzVec*vizmat.Vector(headTrackerMat.getForward()) > 0:
#					xzVec = -xzVec
				xzVec = (xzVec*(1.0-vertWeight)) + (vizmat.Vector(headTrackerMat.getForward())*vertWeight)
				bendDist = (eyeBoneHeight-headTrackerHeight)/4.0
				offset = -xzVec*bendDist
				# then slack in waist x, z should be opposite x, z from head to feet as counter-balance
				self._standInTracker[skeleton.AVATAR_PELVIS].setPosition(offset+center, viz.ABS_GLOBAL)

		self._updatePelvis(vizmat.Transform(parentMat))
		self._updateEyes(vizmat.Transform(parentMat), vizmat.Transform(eyeTrackerMatrix))
		self._updateRoot(vizmat.Transform(parentMat), vizmat.Transform(eyeTrackerMatrix))

		for bodyPart in self._trackerAssignmentDict:
			self._adjustedMatrixDict[bodyPart] = self._getAdjustedMatrix(bodyPart, viz.ABS_GLOBAL)

		# left foot stand-in tracker
		if skeleton.AVATAR_L_FOOT in self._standInTracker:
			localEyePos = parentInvMat.preMultVec(self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL).getPosition())
			localHipPos = parentInvMat.preMultVec(self._boneDict[skeleton.AVATAR_PELVIS].getMatrix(viz.ABS_GLOBAL).getPosition())
#			localEyePos = parentInvMat.preMultVec(eyeTrackerMatrix.getPosition())
#			localHipPos = parentInvMat.preMultVec(self._trackerAssignmentDict[skeleton.AVATAR_PELVIS][ASSIGNED_TRACKER].getMatrix().getPosition())
			localHipMat = vizmat.Transform(parentInvMat)
			localHipMat.preMult(self._boneDict[skeleton.AVATAR_PELVIS].getMatrix(viz.ABS_GLOBAL))
			localHipMat.preMult(self._skeleton.getShift(skeleton.AVATAR_PELVIS))
			mat = vizmat.Transform()
			euler = [localHipMat.getEuler()[0], 0, 0]
			mat.setEuler(euler)
			offset = vizmat.Vector(mat.preMultVec([-0.15, 0.16, 0]))
			center = vizmat.Vector([(localEyePos[0]+localHipPos[0])/2.0, 0, (localEyePos[2]+localHipPos[2])/2.0])
			self._standInTracker[skeleton.AVATAR_L_FOOT].setPosition(offset+center)
			self._standInTracker[skeleton.AVATAR_L_FOOT].setEuler(euler)
		# right foot stand-in tracker
		if skeleton.AVATAR_R_FOOT in self._standInTracker:
			localEyePos = parentInvMat.preMultVec(self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL).getPosition())
			localHipPos = parentInvMat.preMultVec(self._boneDict[skeleton.AVATAR_PELVIS].getMatrix(viz.ABS_GLOBAL).getPosition())
			localHipMat = vizmat.Transform(parentInvMat)
			localHipMat.preMult(self._boneDict[skeleton.AVATAR_PELVIS].getMatrix(viz.ABS_GLOBAL))
			localHipMat.preMult(self._skeleton.getShift(skeleton.AVATAR_PELVIS))
			mat = vizmat.Transform()
			euler = [localHipMat.getEuler()[0], 0, 0]
			mat.setEuler(euler)
			offset = vizmat.Vector(mat.preMultVec([0.15, 0.16, 0]))
			center = vizmat.Vector([(localEyePos[0]+localHipPos[0])/2.0, 0, (localEyePos[2]+localHipPos[2])/2.0])
			self._standInTracker[skeleton.AVATAR_R_FOOT].setPosition(offset+center)
			self._standInTracker[skeleton.AVATAR_R_FOOT].setEuler(euler)

		self._updateArms()
		self._updateLegs()
		self._updateHands()
		self._updateFeet()

		self._updateBody()

		if self._debug:
			# tracker
			for bone, ball in self._debugTracker.iteritems():
				mat = vizmat.Transform()
				# set the matrix for the ball
				mat.setMatrix(parentMat)
				mat.preMult(self._trackerAssignmentDict[bone][ASSIGNED_TRACKER].getMatrix())
				ball.setMatrix(mat)
			# bone
			for bone, ball in self._debugBone.iteritems():
				ball.setMatrix(self._boneDict[bone].getMatrix(viz.ABS_GLOBAL))
			# corrected
			for bone, ball in self._debugCorrected.iteritems():
				ball.setMatrix(self._getAdjustedMatrix(bone, viz.ABS_GLOBAL))
			# shifted
			for bone, ball in self._debugShifted.iteritems():
				mat = self._boneDict[bone].getMatrix(viz.ABS_GLOBAL)
				mat.preMult(self._skeleton.getShift(bone))
				ball.setMatrix(mat)
			# avatar
#			self._debugAvatar.setMatrix(self._avatar.getMatrix(viz.ABS_GLOBAL))
#			self._debugRoot.setMatrix(self._boneDict[bone].getMatrix(viz.AVATAR_WORLD))

	def _updatePelvis(self, parentMat):
		"""Updates the transformation of the pelvis."""
		if skeleton.AVATAR_PELVIS in self._trackerAssignmentDict:
			mat = self._getEulerFromWaist(parentMat)
		else:
			yawAugmentation = 0#self._getYawAugmentation()
			# base yaw of pelvis on head
			headYaw = self._getAdjustedMatrix(skeleton.AVATAR_HEAD, useParent=False).getEuler()[0]
			mat = vizmat.Transform()
			mat.postEuler([headYaw+yawAugmentation, 0, 0])

		mat.postMult(parentMat)
		mat.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_PELVIS))
		self._boneDict[skeleton.AVATAR_PELVIS].setQuat(mat.getQuat(), viz.ABS_GLOBAL)

	def _updateEyes(self, parentMat, eyeTrackerMatrix):
		"""Updates the orientation of the eyes (technically head bone with a
		shift to the eyes) to match the head tracker.
		"""
		# apply known orientation to head joint
		eyeTrackerMatrix.postMult(parentMat)
		eyeTrackerMatrix.preMult(self._skeleton.getShiftInv(skeleton.AVATAR_HEAD))
		self._boneDict[skeleton.AVATAR_HEAD].setQuat(eyeTrackerMatrix.getQuat(), viz.ABS_GLOBAL)

	def _updateRoot(self, parentMat, eyeTrackerMatrix):
		"""Updates the transformation of the root to match the eyes."""
		# compute the head bone target matrix based on tracker pos/ori and shift matrix

		# mat for eyes on avatar
		avatarEyeMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		shiftMat = self._skeleton.getShift(skeleton.AVATAR_HEAD)
		avatarEyeMatrix.preTrans(shiftMat.getPosition())
		avatarEyeMatrix.preQuat(shiftMat.getQuat())

		# get the difference between eyes given by tracker and current avatar eye pos
		eyeTrackerMatrix.postMult(parentMat)
		headDiff = vizmat.Transform(eyeTrackerMatrix)
		headDiff.preMult(avatarEyeMatrix.inverse())

		# get the matrix from the root
		rootMat = self._boneDict[skeleton.AVATAR_ROOT].getMatrix(viz.ABS_GLOBAL)
		rootMat.postMult(headDiff)

		# move the root to compensate for the difference between the target eyes and current eyes
		self._boneDict[skeleton.AVATAR_ROOT].setMatrix(rootMat, viz.ABS_GLOBAL)

	def _updateBody(self):
		"""Updates the transformation of the body."""
		# get scaled eye
		scaledPos = self._getScaledMovement()
		scaledPos += self._movementScalePositionAdjustment
		scaledPos -= self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getPosition()

		rootPos = vizmat.Vector(self._boneDict[skeleton.AVATAR_ROOT].getPosition(viz.AVATAR_LOCAL))
		self._boneDict[skeleton.AVATAR_ROOT].setPosition([0, rootPos[1], 0], viz.AVATAR_LOCAL)
		rootPos += scaledPos
		# existing root sample is without scale information since we're sampling
		# local to the avatar, need to apply scale since we want relative to external
		self._avatar.setPosition([self._scale*rootPos[0], 0, self._scale*rootPos[2]])

	def _updateArms(self):
		"""Updates the transformation of the arms."""
		if self._rightArmIK:
			upperArmMatrix = None
			if skeleton.AVATAR_R_UPPER_ARM in self._adjustedMatrixDict:
				upperArmMatrix = self._adjustedMatrixDict[skeleton.AVATAR_R_UPPER_ARM]

			forearmMatrix = None
			if skeleton.AVATAR_R_FOREARM in self._adjustedMatrixDict:
				forearmMatrix = self._adjustedMatrixDict[skeleton.AVATAR_R_FOREARM]

			if (viz.getFrameNumber() > 10
					and self._autoScale[skeleton.AVATAR_R_FOREARM]
					and self._autoScale[skeleton.AVATAR_R_UPPER_ARM]):
				# get the distance between the hand and arm
				dT = vizmat.Vector(self._adjustedMatrixDict[skeleton.AVATAR_R_HAND].getPosition()) - vizmat.Vector(self._boneDict[skeleton.AVATAR_R_UPPER_ARM].getPosition(viz.ABS_GLOBAL))
				dist = dT.length()
				# rotate the shoulder back so there's a straight line from shoulder to hand
				oLength = self._skeleton.getBoneLength(skeleton.AVATAR_R_FOREARM)
				iLength = self._skeleton.getBoneLength(skeleton.AVATAR_R_UPPER_ARM)
				maxDist = iLength + oLength# there's a max distance that the bone can reach
				if dist > maxDist:
					oPct = oLength/(oLength+iLength)
					iPct = iLength/(oLength+iLength)
					self._skeleton.setBoneLength(skeleton.AVATAR_R_FOREARM, dist*oPct)
					self._skeleton.setBoneLength(skeleton.AVATAR_R_UPPER_ARM, dist*iPct)

			self._rightArmIK.update(self._adjustedMatrixDict[skeleton.AVATAR_R_HAND], forearmMatrix, upperArmMatrix)
		elif self._defaultPostureEnabled:
			mat = vizmat.Transform()
			mat.setQuat(self._boneDict[skeleton.AVATAR_R_UPPER_ARM].getQuat(viz.ABS_GLOBAL))
			mat.preEuler([90, 0, 0])
			self._boneDict[skeleton.AVATAR_R_UPPER_ARM].setQuat(mat.getQuat(), viz.ABS_GLOBAL)

		if self._leftArmIK:
			forearmMatrix = None
			if skeleton.AVATAR_L_FOREARM in self._adjustedMatrixDict:
				forearmMatrix = self._adjustedMatrixDict[skeleton.AVATAR_L_FOREARM]
			upperArmMatrix = None
			if skeleton.AVATAR_L_UPPER_ARM in self._adjustedMatrixDict:
				upperArmMatrix = self._adjustedMatrixDict[skeleton.AVATAR_L_UPPER_ARM]

			if (viz.getFrameNumber() > 10
					and self._autoScale[skeleton.AVATAR_L_FOREARM]
					and self._autoScale[skeleton.AVATAR_L_UPPER_ARM]):
				# get the distance between the hand and arm
				dT = vizmat.Vector(self._adjustedMatrixDict[skeleton.AVATAR_L_HAND].getPosition()) - vizmat.Vector(self._boneDict[skeleton.AVATAR_L_UPPER_ARM].getPosition(viz.ABS_GLOBAL))
				dist = dT.length()
				# rotate the shoulder back so there's a straight line from shoulder to hand
				oLength = self._skeleton.getBoneLength(skeleton.AVATAR_L_FOREARM)
				iLength = self._skeleton.getBoneLength(skeleton.AVATAR_L_UPPER_ARM)
				maxDist = iLength + oLength# there's a max distance that the bone can reach
				if dist > maxDist:
					oPct = oLength/(oLength+iLength)
					iPct = iLength/(oLength+iLength)
					self._skeleton.setBoneLength(skeleton.AVATAR_L_FOREARM, dist*oPct)
					self._skeleton.setBoneLength(skeleton.AVATAR_L_UPPER_ARM, dist*iPct)

			self._leftArmIK.update(self._adjustedMatrixDict[skeleton.AVATAR_L_HAND], forearmMatrix, upperArmMatrix)
		elif self._defaultPostureEnabled:
			mat = vizmat.Transform()
			mat.setQuat(self._boneDict[skeleton.AVATAR_L_UPPER_ARM].getQuat(viz.ABS_GLOBAL))
			mat.preEuler([-90, 0, 0])
			self._boneDict[skeleton.AVATAR_L_UPPER_ARM].setQuat(mat.getQuat(), viz.ABS_GLOBAL)

	def _updateLegs(self):
		"""Updates the transformation of the legs."""
		if self._rightLegIK:
			calfMatrix = None
			if skeleton.AVATAR_R_CALF in self._adjustedMatrixDict:
				calfMatrix = self._adjustedMatrixDict[skeleton.AVATAR_R_CALF]
			self._rightLegIK.update(self._adjustedMatrixDict[skeleton.AVATAR_R_FOOT], calfMatrix)
		if self._leftLegIK:
			calfMatrix = None
			if skeleton.AVATAR_L_CALF in self._adjustedMatrixDict:
				calfMatrix = self._adjustedMatrixDict[skeleton.AVATAR_L_CALF]
			self._leftLegIK.update(self._adjustedMatrixDict[skeleton.AVATAR_L_FOOT], calfMatrix)

	def _updateHands(self):
		"""Updates the transformation of the feet."""
		for bodyPart in [skeleton.AVATAR_R_HAND, skeleton.AVATAR_L_HAND]:
			if bodyPart in self._trackerAssignmentDict and bodyPart in self._boneDict and self._boneDict[bodyPart]:
				if self._usableDoFDict[bodyPart]&DOF_ORI:# make sure we should use ori from tracker
					mat = vizmat.Transform(self._adjustedMatrixDict[bodyPart])
					# if we're using pos, use the head euler
					if self._trackerAssignmentDict[bodyPart][ASSIGNED_DOF] == DOF_POS and self._trackerAssignmentDict[bodyPart][ASSIGNED_PARENT] is None:
						mat.setQuat(self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getQuat())
					mat.preMult(self._skeleton.getShiftInv(bodyPart))
					self._boneDict[bodyPart].setQuat(mat.getQuat(), viz.ABS_GLOBAL)

	def _updateFeet(self):
		"""Updates the transformation of the hands."""
		for bodyPart in [skeleton.AVATAR_R_FOOT, skeleton.AVATAR_L_FOOT]:
			if bodyPart in self._trackerAssignmentDict and bodyPart in self._boneDict and self._boneDict[bodyPart]:
				if self._usableDoFDict[bodyPart]&DOF_ORI and self._trackerAssignmentDict[bodyPart][ASSIGNED_DOF]&DOF_ORI:# make sure we should use ori from tracker
					mat = vizmat.Transform(self._adjustedMatrixDict[bodyPart])
					mat.preMult(self._skeleton.getShiftInv(bodyPart))
					self._boneDict[bodyPart].setQuat(mat.getQuat(), viz.ABS_GLOBAL)


def _dataAverage(x):
	"""Internal function to compute the average from a list."""
	return sum(x) * 1.0 / len(x)


def _dataAverageList(dataList):
	"""Internal function to compute the average from a list of lists."""
	avg = [0]*len(dataList[0])
	for i in range(0, len(dataList)):
		x = dataList[i]
		for j in range(0, len(x)):
			avg[j] += x[j]
	for j in range(0, len(dataList[0])):
		avg[j] /= len(dataList)
	return avg


def _dataVariance(x):
	"""Internal function to compute the variance in a set of data."""
	avg = _dataAverage(x)
	return map(lambda y: (y - avg) ** 2, x)


def _isValidData(x, threshold):
	"""Internal check if a set of data's standard deviation is within a certain threshold."""
	if math.sqrt(_dataAverage(_dataVariance(x))) < threshold:
		return True
	else:
		return False


class _KinectPlayer(object):
	"""A internal class used by the kinect animator to represent a player."""
	def __init__(self,
					ip,
					playerIndex,
					source=KINECT_PPT):
		self._ip = ip
		self._playerIndex = playerIndex
		self._currentPlayerIndex = playerIndex
		self._source = source

		self._rawTrackerDict = {}

		if source == KINECT_PPT:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT
		elif source == KINECT_FAAST_V1:
			self._trackerIndexDict = _FAAST_V1_MAPPING_DICT
		elif source == KINECT_FAAST_V2:
			self._trackerIndexDict = _FAAST_V2_MAPPING_DICT
		elif source == KINECT_SM:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT

		# add trackers for all bones
		self.switchTo(self._currentPlayerIndex)

	def get(self, bodyPartName):
		"""Returns the matrix of the raw tracker at the given body part"""
		return self._rawTrackerDict[bodyPartName].getMatrix()

	def getCurrentIndex(self):
		"""Returns the current player index."""
		return self._currentPlayerIndex

	def removeAll(self):
		"""Removes all tracker data."""
		for bodyPartName in self._trackerIndexDict:
			if bodyPartName in self._rawTrackerDict:
				self._rawTrackerDict[bodyPartName].remove()
		self._rawTrackerDict = collections.OrderedDict()

	def switchTo(self, index):
		"""Switches the current player index to the given plater index. New vrpn
		trackers are added for each body part as needed.
		"""
		self.removeAll()
		self._currentPlayerIndex = index
		vrpn = viz.addExtension('vrpn7.dle')
		for bodyPartName in self._trackerIndexDict:
			trackerId = self._trackerIndexDict[bodyPartName]
			if self._source == KINECT_PPT:
				self._rawTrackerDict[bodyPartName] = vrpn.addTracker('PPT{}@{}'.format(self._playerIndex, self._ip), trackerId-1)
			elif self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
				self._rawTrackerDict[bodyPartName] = vrpn.addTracker('Tracker{}@{}'.format(self._currentPlayerIndex, self._ip), trackerId)
			elif self._source == KINECT_SM:
				self._rawTrackerDict[bodyPartName] = vrpn.addTracker('SMVRPN7@{}'.format(self._ip), self._playerIndex*25+trackerId-1)


class _KinectSensor(object):
	"""
	Kinect sensor measurements:
		http://en.wikipedia.org/wiki/Kinect
		The Kinect sensor has a practical ranging limit of 1.2â3.5 m (3.9â11 ft) distance when used with the Xbox software.
		The sensor has an angular field of view of 57Â° horizontally and 43Â° vertically, while the motorized pivot is capable of tilting the sensor up to 27Â° either up or down.
		http://msdn.microsoft.com/en-us/library/hh973074.aspx

	Kinect sensor placement:
		http://support.xbox.com/en-US/xbox-360/kinect/sensor-placement

	Kinect adventures indicator:
		http://lickskillet.files.wordpress.com/2010/11/dsc_0904.jpg
		http://www.nngroup.com/articles/kinect-gestural-ui-first-impressions/



	KinectV2
		http://www.ign.com/blogs/finalverdict/2013/11/02/xbox-one-vs-playstation-4-kinect-20-vs-playstation-4-camera
	"""

	def __init__(self,
					ip,
					trackerAssignmentDict,
					version=KINECT_V2,
					source=KINECT_PPT):

		self._ip = ip
		self._version = version
		self._source = source
		self._trackerAssignmentDict = trackerAssignmentDict

		self._HEAD_COUNT = 1
		self._CALIBRATION_END_STEP = 2
		self._playerDict = {}
		self._headTrackerList = []
		self._sensorTransform = vizmat.Transform()
		self._calibrationStep = 0
		self._sensorOrientation = [0.0]*3
		self._sensorOrientationSamples = []
		self._sensorPosition = [0.0]*3
		self._sensorPositionSamples = []

		if source == KINECT_PPT:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT
		elif source == KINECT_FAAST_V1:
			self._trackerIndexDict = _FAAST_V1_MAPPING_DICT
		elif source == KINECT_FAAST_V2:
			self._trackerIndexDict = _FAAST_V2_MAPPING_DICT
		elif source == KINECT_SM:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT

		vrpn = viz.addExtension('vrpn7.dle')
		for i in range(0, self._HEAD_COUNT):
			if self._source == KINECT_PPT:
				self._headTrackerList.append(vrpn.addTracker('PPT{}@{}'.format(i, self._ip), self._trackerIndexDict[skeleton.AVATAR_HEAD]-1))
			elif self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
				self._headTrackerList.append(vrpn.addTracker('Tracker{}@{}'.format(i, self._ip), self._trackerIndexDict[skeleton.AVATAR_HEAD]))
			elif source == KINECT_SM:
				self._headTrackerList.append(vrpn.addTracker('SMVRPN7@{}'.format(self._ip), i*25+self._trackerIndexDict[skeleton.AVATAR_HEAD]-1))

		self._clearCalibration()

	def add(self, playerIndex):
		"""Adds a player."""
		if playerIndex not in self._playerDict:
			self._playerDict[playerIndex] = _KinectPlayer(self._ip, playerIndex)
		return self._playerDict[playerIndex]

	def _clearCalibration(self):
		"""Internal method to clear the calibration, used by t-pose reset,
		and other methods.
		"""
		self._calibrationStep = 0

		self._sensorOrientation = [0.0]*3
		self._sensorOrientationSamples = []

		self._sensorPosition = [0.0]*3
		self._sensorPositionSamples = []

	def _computeSensorOrientation(self, player):
		"""Internal method to compute the sensor orientation. This method is called
		as part of the calibration process.
		"""
		finalEstimate = None
		sampleRequirement = 30
		sigmaThreshold = 1.0

		# the most reliable method for computing the sensor orientation seems to be getting the yaw for
		# the sensor from the outstretched arms of the avatar and assuming avatar is facing north
		lhp = vizmat.Vector(player.get(skeleton.AVATAR_L_HAND).getPosition())
		rhp = vizmat.Vector(player.get(skeleton.AVATAR_R_HAND).getPosition())
		# find the vector between the hands
		handVec = rhp - lhp

		if handVec.length() < 1.2:
			return None

		# use only the x and z components and normalize the vector
		handVec[1] = 0
		handVec.normalize()

		# determine the angle between the normalized hand vec and the x vector
		rotAxis = [0, 1, 0]
		dirVec = [1, 0, 0]
		yaw = vizmat.AngleBetweenVector(handVec, dirVec) * math.copysign(1, handVec.cross(dirVec)*vizmat.Vector(rotAxis))

		# set the estimate from the sample average if low enough variance
		self._sensorOrientationSamples.append(yaw)
		if len(self._sensorOrientationSamples) > sampleRequirement:
			self._sensorOrientationSamples.pop(0)
			if _isValidData(self._sensorOrientationSamples, sigmaThreshold):
				yaw = _dataAverage(self._sensorOrientationSamples)
				yaw += self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getEuler()[0]
				finalEstimate = [yaw, 0.0, 0.0]

		return finalEstimate

	def _computeSensorPosition(self, player, globalHeadMat):
		"""Internal method to compute the sensor position. This method is called
		as part of the calibration process.
		"""
		finalEstimate = None
		sample = None
		sampleRequirement = 30

		# get the x, y, z of the head from the sensor
		kinectHeadPos = player.get(skeleton.AVATAR_HEAD).getPosition()
		mat = vizmat.Transform()
		mat.setEuler(self._sensorOrientation)
		kinectHeadPos = mat.preMultVec(kinectHeadPos)

		# if we have an assigned head tracker with position get the pos from that and diff with
		# reported head pos from kinect
		sample = vizmat.Vector(globalHeadMat.getPosition()) - kinectHeadPos

		# set the estimate from the sample average if low enough variance
		if sample is not None:
			self._sensorPositionSamples.append(sample)
			if len(self._sensorPositionSamples) > sampleRequirement:
				finalEstimate = _dataAverageList(self._sensorPositionSamples)

		return finalEstimate

	def get(self, playerIndex):
		"""Returns the player object matching the given index. Internally,
		the player may use a different index than assigned by a given kinect
		sensor.

		@return _KinectPlayer()
		"""
		return self._playerDict[playerIndex]

	def getCalibration(self):
		"""Returns the calibration data for the sensor as a tuple (pos, euler)"""
		return self._sensorPosition, self._sensorOrientation

	def _getClosest(self, headPosition):
		"""Returns the closest kinect head sensor to the provided head location.
		This method is used to match between multiple kinect sensor's predicted
		locations.
		"""
		threshold = 0.1
		minIndex = -1
		minDistance = 100
		# find the closest value within a threshold, if not found return inconclusive (-1)
		for i in range(0, 4):
			tracker = self._headTrackerList[i]
			distance = vizmat.Distance(tracker.getPosition(), headPosition)
			if distance < threshold and distance < minDistance:
				minIndex = i
				minDistance = distance
		return minIndex

	def _getTPosePlayerIndex(self):
		"""Get the index for the first found player to be in the t-pose.

		@return int
		"""
		for playerIndex in self._playerDict:
			if self.isInTPose(playerIndex):
				return playerIndex
		return -1

	def isCalibrated(self):
		"""Returns True if the calibration process is complete.

		@return bool
		"""
		if self._calibrationStep < self._CALIBRATION_END_STEP:
			return False
		else:
			return True

	def isInFOV(self, pos):
		"""Returns True if the given matrix has a value inside the
		FOV of the sensor.

		@return bool
		"""
		# set the fov depending on which version of kinect is being used
		if self._version == KINECT_V1:
			n = 1.2
			f = 3.5
			r = math.tan(57.0*DEGREES_TO_RADIANS/2.0)*n
			l = -r
			t = math.tan(43.0*DEGREES_TO_RADIANS/2.0)*n
			b = -t
			return _isInFrustum(pos, l, r, b, t, n, f)
		if self._version == KINECT_V2:
			n = 0.2
			f = 10.5
			r = math.tan(70.0*DEGREES_TO_RADIANS/2.0)*n
			l = -r
			t = math.tan(60.0*DEGREES_TO_RADIANS/2.0)*n
			b = -t
			return _isInFrustum(pos, l, r, b, t, n, f)
		return True

	def isInTPose(self, playerIndex):
		"""Checks if the player is in the t-pose."""
		player = self._playerDict[playerIndex]

		# get the hand position for the avatar
		rhandPos = vizmat.Vector(player.get(skeleton.AVATAR_R_HAND).getPosition())
		lhandPos = vizmat.Vector(player.get(skeleton.AVATAR_L_HAND).getPosition())

		rfootPos = vizmat.Vector(player.get(skeleton.AVATAR_R_FOOT).getPosition())
		lfootPos = vizmat.Vector(player.get(skeleton.AVATAR_L_FOOT).getPosition())

		# get vector between left shoulder and right hand and right shoulder
		# and left hand and make sure they're inline
		lsrh = rhandPos - player.get(skeleton.AVATAR_L_FOREARM).getPosition()
		rslh = lhandPos - player.get(skeleton.AVATAR_R_FOREARM).getPosition()
		lsrh.normalize()
		rslh.normalize()
		align = lsrh*rslh

		# get the distance between the tracker information
		dist = (rhandPos - lhandPos).length()
		armYDist = abs(rhandPos[1] - lhandPos[1])
		armRFootYDist = abs(rhandPos[1] - rfootPos[1])
		armLFootYDist = abs(lhandPos[1] - lfootPos[1])
		if abs(align) > 0.996 and dist > 1.2 and armYDist < 1.0 and armRFootYDist > 0.5 and armLFootYDist > 0.5:
			return True
		return False

	def _updateCalibration(self, globalHeadMat):
		"""Internal method to update the calibration of the sensor"""
		playerIndex = self._getTPosePlayerIndex()

		if playerIndex == -1:
			return
		player = self.get(playerIndex)

		if self._calibrationStep == 0:
			# determining sensor orientation
			# if we have a valid height compute the yaw orientation of the sensor
			orientationEstimate = self._computeSensorOrientation(player)
			if orientationEstimate is not None:
				self._sensorOrientation = orientationEstimate
				# apply a post rotation to the trackers (compensation for sensor)

				# update the calibration step
				self._calibrationStep += 1

		elif self._calibrationStep == 1:
			# determining sensor position
			positionEstimate = self._computeSensorPosition(player, globalHeadMat)
			if positionEstimate is not None:
				self._sensorPosition = positionEstimate
				# apply a post trans to the trackers (compensation for sensor)

				# update the calibration step
				self._calibrationStep += 1

	def verifyPlayerIndex(self, playerIndex, headTrackerMatrix):
		"""Checks to be sure that the assigned player index is correct by using
		an external "ground truth" head matrix, which should match to the
		rectified (in calibrated sensor's transformation) head transformation. If
		a match is incorrect, this method swaps the player object's current index
		to the matching index.
		"""
		if self.isCalibrated():
			# transform the head tracker matrix into sensor coordinates
			sensorRelativePos = self._sensorTransform.preMultVec(headTrackerMatrix.getPosition())
			# check if the pos is valid first, if not find the
			match = self._getClosest(sensorRelativePos)
			if match != -1 and match != self._playerDict[playerIndex].getCurrentIndex():
				self._playerDict[playerIndex].switchTo(match)


class _KinectSensorManager(object):
	"""Class that manages kinect sensors"""
	def __init__(self):
		self._kinectDict = {}

	def add(self, ip, trackerAssignmentDict, version=KINECT_V2):
		"""Adds a new tracker assignment dictionary with a new kinect version.
		The kinect is assigned based on the ip address."""
		if ip not in self._kinectDict:
			self._kinectDict[ip] = _KinectSensor(ip, version=version, trackerAssignmentDict=trackerAssignmentDict)
		return self._kinectDict[ip]

	def get(self, ip):
		"""Returns the kinect given by the associated ip"""
		if ip in self._kinectDict:
			return self._kinectDict[ip]
		else:
			return None
_KINECT_SENSOR_MANAGER = _KinectSensorManager()


class MultiKinect(_HeadBased):
	"""A class for enabling multiple Kinects.

	NOTE:
	This class is currently undergoing changes, and may have functional and API
	differences in the near future.
	"""
	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict=None,
					ipList=None,
					mode=KINECT_MODE_FULL_BODY,
					playerIndex=0,
					avgSampleCount=1,
					medianSampleCount=1,
					showCalibrationPanel=True,
					source=KINECT_PPT,
					debug=False):

		self._CALIBRATION_END_STEP = 1

		if trackerAssignmentDict is None:
			trackerAssignmentDict = {}
		super(MultiKinect, self).__init__(avatarObject=avatarObject,
										skeletonObject=skeletonObject,
										trackerAssignmentDict=trackerAssignmentDict)

		self._ipList = ['localhost'] if ipList is None else ipList.split(', ')
		self._mode = mode
		self._playerIndex = playerIndex
		self._avgSampleCount = avgSampleCount
		self._medianSampleCount = medianSampleCount
		self._showCalibrationPanel = showCalibrationPanel
		self._debug = debug
		self._source = source

		self._sensorDict = {}

		self._calibrationStep = 0
		self._scale = [1.0]*3
		self._scaleSamples = []

		self._clearCalibration()

		# add kinect sensors to list if not present
		for ip in self._ipList:
			self._sensorDict[ip] = _KINECT_SENSOR_MANAGER.add(ip=ip, trackerAssignmentDict=trackerAssignmentDict)
			# for each sensor, ensure that this player index exists
			self._sensorDict[ip].add(self._playerIndex)

		if source == KINECT_PPT:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT
		elif source == KINECT_FAAST_V1:
			self._trackerIndexDict = _FAAST_V1_MAPPING_DICT
		elif source == KINECT_FAAST_V2:
			self._trackerIndexDict = _FAAST_V2_MAPPING_DICT
		elif source == KINECT_SM:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT

		self._debugBallDict = collections.OrderedDict()
		for bodyPartName in self._trackerIndexDict:
			if self._debug:
				self._debugBallDict[bodyPartName] = inverse_kinematics.makeDebugBall(0.05, _TRACKER_COLOR_DICT[bodyPartName])

		# add an info panel to provide calibration feedback
		if self._showCalibrationPanel:
			self._panel = vizinfo.InfoPanel('', align=viz.ALIGN_CENTER, fontSize=22, icon=False, key=None)
			self._text = viz.addText('')
			self._panel.addItem(self._text)
			self._panel.visible(False)

	def _clearCalibration(self):
		"""Internal method to clear the calibration, used by t-pose reset,
		and other methods.
		"""
		self._calibrationStep = 0

		self._scale = [1.0]*3
		self._scaleSamples = []

	def _computeCompositeRawMatrix(self, bodyPartName):
		"""Computes a composite raw matrix for the given body part over the
		set of sensors.
		"""
		mat = None
		for ip in self._sensorDict:
			sensor = self._sensorDict[ip]
			if sensor.isCalibrated():
				# get the player, note that the player index may have changed
				player = sensor.get(self._playerIndex)
				# get the the tracker data from the player
				mat = player.get(bodyPartName)
				if mat is not None:
					# get the calibration from the sensor test against head tracker
					pos, ori = sensor.getCalibration()
					headMat = vizmat.Transform()
					headMat.postEuler(ori)
					headMat.postTrans(pos)
					headMat = headMat.inverse()
					headMat.preMult(self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix())
					if True:#sensor.isInFOV(headMat.getPosition()):
						kinectMat = player.get(skeleton.AVATAR_HEAD)
						inv = vizmat.Transform()
						inv.setEuler(_KINECT_V2_PPT_TPOSE_TRANSFORM_DICT[skeleton.AVATAR_HEAD])
						inv = inv.inverse()
						kinectMat.preEuler(inv.getEuler())
						pos, ori = sensor.getCalibration()
						kinectMat.postEuler(ori)
						kinectMat.postTrans(pos)

						pptV = vizmat.Vector(self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getPosition())
						kinectV = vizmat.Vector(kinectMat.getPosition())

						vec = pptV - kinectV

						if vec.length() < 0.2:# and sensor.isInFOV(tracker.getPosition()):
							# update the player
							if self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
								# switch from right to left handed coordinates
								pos = mat.getPosition()
								quat = mat.getQuat()
								pos = [pos[0], pos[1], -pos[2]]
								quat = [-quat[0], -quat[1], quat[2], quat[3]]
								mat.setPosition(pos)
								mat.setQuat(quat)
							elif self._source == KINECT_PPT or self._source == KINECT_SM:
								inv = vizmat.Transform()
								inv.setEuler(_KINECT_V2_PPT_TPOSE_TRANSFORM_DICT[bodyPartName])
								inv = inv.inverse()
								mat.preEuler(inv.getEuler())

							# apply the offset from the sensor
							pos, ori = sensor.getCalibration()
							mat.postEuler(ori)
							mat.postTrans(pos)
							return mat
		return None

	def _computeMatrix(self, bodyPartName):
		"""Internal method which computes the matrix for the given body part."""
		mat = self._computeCompositeRawMatrix(bodyPartName)

		# if we found the mat in any of the sensors we can continue
		if mat is not None:
			mat = self._computeMergedMatrix(bodyPartName, mat)
		return mat

	def _computeMergedMatrix(self, bodyPartName, mat):
		"""Internal method which computes the merged matrix (over multiple sensors)
		for the given body part.
		"""
		if bodyPartName in self._trackerAssignmentDict:
			assignment = self._trackerAssignmentDict[bodyPartName]
			assignmentMat = assignment[ASSIGNED_TRACKER].getMatrix()

			if assignment[ASSIGNED_DOF] == DOF_POS:
				mat.setPosition(assignmentMat.getPosition())
			elif assignment[ASSIGNED_DOF] == DOF_ORI:
				mat.setQuat(assignmentMat.getQuat())
			elif assignment[ASSIGNED_DOF] == DOF_6DOF:
				mat = assignmentMat
		return mat

	def _computeScale(self):
		"""Internal method which computes the scale of the avatar using the
		height of the head tracker.
		"""
		finalEstimate = None
		sample = -1
		sampleRequirement = 30
		sigmaThreshold = 0.9

		sampleRequirement = 30
		# if the head is given, and it has position, use the position to set the scale
		eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
		headTrackerMat = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()

		# scale avatar by the eye height difference so it touches the floor
		sample = headTrackerMat.getPosition()[1] / eyeBoneMatrix.getPosition()[1]

		# set the estimate from the sample average if low enough variance
		if sample != -1:
			self._scaleSamples.append(sample)
			if len(self._scaleSamples) > sampleRequirement:
				self._scaleSamples.pop(0)
				if _isValidData(self._scaleSamples, sigmaThreshold):
					finalEstimate = _dataAverage(self._scaleSamples)

		return finalEstimate

	def _getTPoseSensor(self):
		"""Returns a sensor containing an aver in the t-pose, if there is one.
		If not, then returns None.
		"""
		for ip in self._sensorDict:
			sensor = self._sensorDict[ip]
			if sensor.isCalibrated():
				if sensor.isInTPose(self._playerIndex):
					return sensor
		return None

	def tposeReset(self):
		"""Clears the calibration and and also clears the calibration for
		each sensor.
		"""
		self._clearCalibration()

		for sensor in self._sensorDict.values():
			sensor._clearCalibration()

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		# update avatar
		self._avatar.setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._skeleton.setToDefaultLayout()
		self._avatar.setScale(self._scale)

		# update calibration if needed
		if self._calibrationStep < self._CALIBRATION_END_STEP:
			tposeSensor = self._getTPoseSensor()
			if tposeSensor is not None:
				self._updateCalibration()
			else:
				self._clearCalibration()
				if self._showCalibrationPanel:
					self._panel.visible(False)

		# verify that the user registered as player X is really player X as reported by kinect and matched to
		# known head trackers, if not, swap id's
#		self._verifyPlayerIndex(self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER])

		self._updateSensorCalibrations()

		# update the skeleton of the avatar
		headTrackerMat = None
		for bodyPartName in self._trackerIndexDict:
			# get the composite matrix by combining data from each sensor in which the tracker is visible
			trackerMat = self._computeMatrix(bodyPartName)
			if trackerMat:
				if bodyPartName == skeleton.AVATAR_HEAD:
					headTrackerMat = trackerMat

				boneMat = vizmat.Transform()
				boneMat.setQuat(self._skeleton._defaultTransGlobal[bodyPartName])
				boneMat.postQuat(trackerMat.getQuat())
				self._boneDict[bodyPartName].setQuat(boneMat.getQuat(), viz.ABS_GLOBAL)

				if self._debug:
					self._debugBallDict[bodyPartName].setMatrix(trackerMat)

		# shift the body so that the head matches
		if headTrackerMat:
			headTrackerMat.setQuat([0, 0, 0, 1])
			headBoneMat = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
			headBoneMat.setQuat([0, 0, 0, 1])
			headTrackerMat.preMult(headBoneMat.inverse())
			self._avatar.setPosition(headTrackerMat.getPosition())

	def _updateSensorCalibrations(self):
		"""Updates the sensor calibrations."""
		globalHeadMat = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()
		for ip in self._sensorDict:
			sensor = self._sensorDict[ip]
			if not sensor.isCalibrated():
				sensor._updateCalibration(globalHeadMat)

	def _updateCalibration(self):
		"""Internal method to update the calibration. For multi kinect calibration,
		this means the height of the avatar. Sensor calibration is done separately.
		"""
		text = ''
		if self._calibrationStep == 0:
			text = 'determining scale'

			scaleEstimate = self._computeScale()
			if scaleEstimate is not None:
				self._scale = [scaleEstimate]*3
				# update the calibration step
				self._calibrationStep += 1

		if self._showCalibrationPanel:
			if self._calibrationStep < self._CALIBRATION_END_STEP:
				self._text.message(text)
				self._panel.getPanel().dirtyLayout()
				self._panel.visible(True)
			else:
				self._panel.visible(False)

	def _verifyPlayerIndex(self, headMat):
		"""Checks to be sure that the assigned player index is correct by using
		an external "ground truth" head matrix, which should match to the
		rectified (in calibrated sensor's transformation) head transformation. If
		a match is incorrect, this method swaps the player object's current index
		to the matching index.
		"""
		# verify the player index for each sensor
		for ip in self._sensorDict:
			self._sensorDict[ip].verifyPlayerIndex(self._playerIndex, headMat)


class Kinect(_HeadBased):
	"""An animator which uses the kinect to animate the body of an avatar.
	The choice of bones is determined by ."""
	def __init__(self,
					avatarObject,
					skeletonObject,
					trackerAssignmentDict=None,
					kinectVersion=KINECT_V2,
					source=KINECT_PPT,
					ip='localhost',
					mode=KINECT_MODE_FULL_BODY,
					playerIndex=0,
					avgSampleCount=1,
					medianSampleCount=1,
					showCalibrationPanel=True,
					debug=False):

		if trackerAssignmentDict is None:
			trackerAssignmentDict = {}
		super(Kinect, self).__init__(avatarObject=avatarObject,
										skeletonObject=skeletonObject,
										trackerAssignmentDict=trackerAssignmentDict)

		self._kinectVersion = kinectVersion
		self._source = source
		self._ip = ip
		self._mode = mode
		self._playerIndex = playerIndex
		self._avgSampleCount = avgSampleCount
		self._medianSampleCount = medianSampleCount
		self._showCalibrationPanel = showCalibrationPanel
		self._debug = debug

		self._scale = [0, 0, 0]
		self._boneDict = self._skeleton.getBoneDict()
		self._calibrationStep = 0
		self._scale = [1.0]*3
		self._scaleSamples = []
		self._sensorPosition = [0.0]*3
		self._sensorPositionSamples = []
		self._sensorOrientation = [0.0]*3
		self._sensorOrientationSamples = []

		if source == KINECT_PPT:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT
		elif source == KINECT_FAAST_V1:
			self._trackerIndexDict = _FAAST_V1_MAPPING_DICT
		elif source == KINECT_FAAST_V2:
			self._trackerIndexDict = _FAAST_V2_MAPPING_DICT
		elif source == KINECT_SM:
			self._trackerIndexDict = _KINECT_V2_PPT_MAPPING_DICT

		self._rawTrackerDict = collections.OrderedDict()
		self._filteredTrackerDict = collections.OrderedDict()
		self._assignedTrackerLinkDict = collections.OrderedDict()# links from any assigned tracker to final sample/coordinate conversion used
		self._mergedTrackerDict = collections.OrderedDict()

		self._CALIBRATION_END_STEP = 3

		self._debugBallDict = {}

		self._clearCalibration()
#		self._calibrationStep = self._CALIBRATION_END_STEP
		for bodyPartName in self._trackerIndexDict:
			self._addKinectTracker(bodyPartName, self._trackerIndexDict[bodyPartName])
			self._addFilter(bodyPartName)
			if bodyPartName in self._trackerAssignmentDict:
				self._addMergedTracker(bodyPartName)
			if self._debug:
				self._debugBallDict[bodyPartName] = inverse_kinematics.makeDebugBall(0.05, _TRACKER_COLOR_DICT[bodyPartName])

		# add an info panel to provide calibration feedback
		if self._showCalibrationPanel:
			self._panel = vizinfo.InfoPanel('', align=viz.ALIGN_CENTER, fontSize=22, icon=False, key=None)
			self._text = viz.addText('')
			self._panel.addItem(self._text)
			self._panel.visible(False)

	def _addKinectTracker(self, bodyPartName, trackerId):
		"""Internal method to add a kinect tracker."""
		# Add trackers for each of the body parts we will need
		vrpn = viz.addExtension('vrpn7.dle')
		if self._source == KINECT_PPT:
			self._rawTrackerDict[bodyPartName] = vrpn.addTracker('PPT{}@{}'.format(self._playerIndex, self._ip), trackerId-1)
		elif self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
			self._rawTrackerDict[bodyPartName] = vrpn.addTracker('Tracker{}@{}'.format(self._playerIndex, self._ip), trackerId)
		elif self._source == KINECT_SM:
			self._rawTrackerDict[bodyPartName] = vrpn.addTracker('SMVRPN7@{}'.format(self._ip), self._playerIndex*25+trackerId-1)

	def _addFilter(self, bodyPartName):
		"""Adds a filter to the given body part's tracker."""
		tracker = self._rawTrackerDict[bodyPartName]
		if self._avgSampleCount > 1 or self._medianSampleCount > 1:
			filterObj = viz.add('filter.dle')
			if self._medianSampleCount > 1:
				tracker = filterObj.median(tracker, samples=self._medianSampleCount, mask=filterObj.FILTER_ORI|filterObj.FILTER_POS)
			if self._avgSampleCount > 1:
				tracker = filterObj.average(tracker, samples=self._avgSampleCount, mask=filterObj.FILTER_ORI|filterObj.FILTER_POS)

		link = viz.link(tracker, viz.NullLinkable)
		if self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
			link.swapPos([1, 2, -3])
			link.swapQuat([-1, -2, 3, 4])
		self._filteredTrackerDict[bodyPartName] = link

	def _addMergedTracker(self, bodyPartName):
		"""Internal method to add a merged tracker."""
		group = viz.addGroup()
		group.disable(viz.INTERSECTION)

		assignment = self._trackerAssignmentDict[bodyPartName]
		self._assignedTrackerLinkDict[bodyPartName] = viz.link(assignment[ASSIGNED_TRACKER], group)

		link = self._filteredTrackerDict[bodyPartName]
		link.setDst(group)

		if bodyPartName == skeleton.AVATAR_R_HAND:
			self._assignedTrackerLinkDict[bodyPartName].preEuler([-90, 0, 0])
		if bodyPartName == skeleton.AVATAR_L_HAND:
			self._assignedTrackerLinkDict[bodyPartName].preEuler([90, 0, 0])

		if assignment[ASSIGNED_DOF] == DOF_POS:
			link.setMask(viz.LINK_ORI)
			self._assignedTrackerLinkDict[bodyPartName].setMask(viz.LINK_POS)
		elif assignment[ASSIGNED_DOF] == DOF_ORI:
			link.setMask(viz.LINK_POS)
			self._assignedTrackerLinkDict[bodyPartName].setMask(viz.LINK_ORI)
		elif assignment[ASSIGNED_DOF] == DOF_6DOF:
			link.setEnabled(False)

		self._mergedTrackerDict[bodyPartName] = group

	def _clearCalibration(self):
		"""Internal method to clear the calibration, used by t-pose reset,
		and other methods.
		"""
		self._calibrationStep = 0

		# clear offsets applied to links based on offsets from previous calibrations
		for bodyPart in self._filteredTrackerDict:
			link = self._filteredTrackerDict[bodyPart]
			link.reset(viz.RESET_OPERATORS)
			if self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
				link.swapPos([1, 2, -3])
				link.swapQuat([-1, -2, 3, 4])

		self._scale = [1.0]*3
		self._scaleSamples = []

		self._sensorPosition = [0.0]*3
		self._sensorPositionSamples = []

		self._sensorOrientation = [0.0]*3
		self._sensorOrientationSamples = []

	def _computeScale(self):
		"""Computes the scale of the avatar/user."""
		finalEstimate = None
		sample = -1
		sampleRequirement = 30
		sigmaThreshold = 0.9

		# if we have an assigned head tracker with position information use that
		if skeleton.AVATAR_HEAD in self._trackerAssignmentDict and self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_DOF]&DOF_POS:
			sampleRequirement = 30
			# if the head is given, and it has position, use the position to set the scale
			eyeBoneMatrix = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
			eyeBoneMatrix.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
			headTrackerMat = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getMatrix()

			# scale avatar by the eye height difference so it touches the floor
			sample = headTrackerMat.getPosition()[1] / eyeBoneMatrix.getPosition()[1]
		else:# otherwise get a scale from kinect measurements
			sampleRequirement = 60
			lhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_L_HAND].getPosition())
			rhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_R_HAND].getPosition())
			physicalWingspan = (lhp - rhp).length()*1.1# FUDGE for avatar scale

			lha = vizmat.Vector(self._boneDict[skeleton.AVATAR_L_HAND].getPosition(viz.ABS_GLOBAL))
			rha = vizmat.Vector(self._boneDict[skeleton.AVATAR_R_HAND].getPosition(viz.ABS_GLOBAL))
			virtualWingspan = (lha-rha).length()
			sample = physicalWingspan/virtualWingspan

		# set the estimate from the sample average if low enough variance
		if sample != -1:
			self._scaleSamples.append(sample)
			if len(self._scaleSamples) > sampleRequirement:
				self._scaleSamples.pop(0)
				if _isValidData(self._scaleSamples, sigmaThreshold):
					finalEstimate = _dataAverage(self._scaleSamples)

		return finalEstimate

	def _computeSensorPosition(self):
		"""Internal method to compute the sensor position. This method is called
		as part of the calibration process.
		"""
		finalEstimate = None
		sample = None
		sampleRequirement = 30

		# get the x, y, z of the head from the sensor
		kinectHeadPos = self._filteredTrackerDict[skeleton.AVATAR_HEAD].getPosition()
		mat = vizmat.Transform()
		mat.setEuler(self._sensorOrientation)
		kinectHeadPos = mat.preMultVec(kinectHeadPos)

		# if we have an assigned head tracker with position get the pos from that and diff with
		# reported head pos from kinect
		if skeleton.AVATAR_HEAD in self._trackerAssignmentDict and self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_DOF]&DOF_POS:
			headTrackerPos = self._trackerAssignmentDict[skeleton.AVATAR_HEAD][ASSIGNED_TRACKER].getPosition()
			sample = vizmat.Vector(headTrackerPos) - kinectHeadPos
		else:
			# compute height from the physical wingspan
			lhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_L_HAND].getPosition())
			rhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_R_HAND].getPosition())
			physicalWingspan = (lhp - rhp).length()*1.1# FUDGE for avatar scale
			heightEstimate = physicalWingspan*1.1# FUDGE

			# negate kinect head pos to return to origin, shifting up by height estimate
			sample = [-kinectHeadPos[0], (heightEstimate - kinectHeadPos[1]), -kinectHeadPos[2]]

		# set the estimate from the sample average if low enough variance
		if sample is not None:
			self._sensorPositionSamples.append(sample)
			if len(self._sensorPositionSamples) > sampleRequirement:
				finalEstimate = _dataAverageList(self._sensorPositionSamples)

		return finalEstimate

	def _computeSensorOrientation(self):
		"""Internal method to compute the sensor orientation. This method is called
		as part of the calibration process.
		"""
		finalEstimate = None
		sampleRequirement = 30
		sigmaThreshold = 1.0

		# the most reliable method for computing the sensor orientation seems to be getting the yaw for
		# the sensor from the outstretched arms of the avatar and assuming avatar is facing north
		lhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_L_HAND].getPosition())
		rhp = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_R_HAND].getPosition())
		# find the vector between the hands
		handVec = rhp - lhp

		# use only the x and z components and normalize the vector
		handVec[1] = 0
		handVec.normalize()

		# determine the angle between the normalized hand vec and the x vector
		rotAxis = [0, 1, 0]
		dirVec = [1, 0, 0]
		yaw = vizmat.AngleBetweenVector(handVec, dirVec) * math.copysign(1, handVec.cross(dirVec)*vizmat.Vector(rotAxis))

		# set the estimate from the sample average if low enough variance
		self._sensorOrientationSamples.append(yaw)
		if len(self._sensorOrientationSamples) > sampleRequirement:
			self._sensorOrientationSamples.pop(0)
			if _isValidData(self._sensorOrientationSamples, sigmaThreshold):
				yaw = _dataAverage(self._sensorOrientationSamples)
				finalEstimate = [yaw, 0.0, 0.0]

		return finalEstimate

	def _createDefaultUsableDoFDict(self):
		"""Internal method which returns a dictionary of the default usable
		degrees of freedom for each body part. This can be used to limit
		the information used by each joint.
		"""
		dofDict = super(Kinect, self)._createDefaultUsableDoFDict()
		dofDict.update({
			skeleton.AVATAR_L_HAND: DOF_ORI,
			skeleton.AVATAR_R_HAND: DOF_ORI,
		})
		return dofDict

	def isInTPose(self):
		"""Checks if the player is in the t-pose."""
		# get the hand position for the avatar
		rhandPos = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_R_HAND].getPosition())
		lhandPos = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_L_HAND].getPosition())

		rfootPos = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_R_FOOT].getPosition())
		lfootPos = vizmat.Vector(self._filteredTrackerDict[skeleton.AVATAR_L_FOOT].getPosition())

		# get vector between left shoulder and right hand and right shoulder
		# and left hand and make sure they're inline
		lsrh = rhandPos - self._filteredTrackerDict[skeleton.AVATAR_L_FOREARM].getPosition()
		rslh = lhandPos - self._filteredTrackerDict[skeleton.AVATAR_R_FOREARM].getPosition()
		lsrh.normalize()
		rslh.normalize()
		align = lsrh*rslh

		# get the distance between the tracker information
		dist = (rhandPos - lhandPos).length()
		armYDist = abs(rhandPos[1] - lhandPos[1])
		armRFootYDist = abs(rhandPos[1] - rfootPos[1])
		armLFootYDist = abs(lhandPos[1] - lfootPos[1])
		if abs(align) > 0.996 and dist > 1.2 and armYDist < 0.3 and armRFootYDist > 0.5 and armLFootYDist > 0.5:
			return True
		return False

	def remove(self):
		"""Removes the animator and allocated resources such as vrpn trackers,
		links, etc.
		"""
		# remove raw trackers
		for bodyPart in self._trackerIndexDict:
			self._rawTrackerDict[bodyPart].remove()
		# remove filtered links
		for bodyPart in self._filteredTrackerDict:
			self._filteredTrackerDict[bodyPart].remove()
		# remove links created for separately assigned trackers
		for bodyPart in self._assignedTrackerLinkDict:
			self._assignedTrackerLinkDict[bodyPart].remove()
		# remove any groups and links from the merged trackers
		for bodyPart in self._mergedTrackerDict:
			self._mergedTrackerDict[bodyPart].remove()
		# removing items for debugging if enabled
		if self._debug:
			for bodyPart in self._debugBallDict:
				self._debugBallDict[bodyPart].remove()

	def tposeReset(self):
		"""Resets the t-pose."""
		self._clearCalibration()

	def _update(self):
		"""Internal method to update the animator, called every frame by the
		updateEvent.
		"""
		if self._avatar.getParents():
			parentMat = self._avatar.getParents()[0].getMatrix(viz.ABS_GLOBAL)
		else:
			parentMat = vizmat.Transform()

		# update avatar
		self._avatar.setMatrix(vizmat.Transform(), viz.ABS_GLOBAL)
		self._skeleton.setToDefaultLayout()
		self._avatar.setScale(self._scale)

		# update calibration if needed
		if self._calibrationStep < self._CALIBRATION_END_STEP:
			if self.isInTPose():
				self._updateCalibration()
			else:
				self._clearCalibration()
				if self._showCalibrationPanel:
					self._panel.visible(False)

		# update the skeleton of the avatar
		for bodyPartName in self._filteredTrackerDict:
			if self._source == KINECT_FAAST_V1 or self._source == KINECT_FAAST_V2:
				if bodyPartName in self._mergedTrackerDict:
					trackerMat = self._mergedTrackerDict[bodyPartName].getMatrix()
				else:
					trackerMat = self._filteredTrackerDict[bodyPartName].getMatrix()
			elif self._source == KINECT_PPT or self._source == KINECT_SM:
				trackerMat = self._filteredTrackerDict[bodyPartName].getMatrix()
				inv = vizmat.Transform()
				inv.setEuler(_KINECT_V2_PPT_TPOSE_TRANSFORM_DICT[bodyPartName])
				inv = inv.inverse()
				trackerMat.preEuler(inv.getEuler())

			boneMat = vizmat.Transform()
			boneMat.setQuat(self._skeleton._defaultTransGlobal[bodyPartName])
			boneMat.postQuat(trackerMat.getQuat())
			self._boneDict[bodyPartName].setQuat(boneMat.getQuat(), viz.ABS_GLOBAL)

			if self._debug:
				self._debugBallDict[bodyPartName].setMatrix(trackerMat)
				self._debugBallDict[bodyPartName].setQuat(trackerMat.getQuat())

		# shift the body so that the head matches
		if skeleton.AVATAR_HEAD in self._mergedTrackerDict:
			headTrackerMat = self._mergedTrackerDict[skeleton.AVATAR_HEAD].getMatrix()
		else:
			headTrackerMat = self._filteredTrackerDict[skeleton.AVATAR_HEAD].getMatrix()
		headBoneMat = self._boneDict[skeleton.AVATAR_HEAD].getMatrix(viz.ABS_GLOBAL)
		headBoneMat.preMult(self._skeleton.getShift(skeleton.AVATAR_HEAD))
		headTrackerMat.preMult(headBoneMat.inverse())

		parentMat.preTrans(headTrackerMat.getTrans())
		self._avatar.setPosition(parentMat.getTrans(), viz.ABS_GLOBAL)
		self._avatar.setQuat(parentMat.getQuat(), viz.ABS_GLOBAL)

	def _updateCalibration(self):
		"""Internal method to update the calibration of the sensor"""
		text = ''

		if self._calibrationStep == 0:
			# determining scale
			scaleEstimate = self._computeScale()
			if scaleEstimate is not None:
				self._scale = [scaleEstimate]*3
				# update the calibration step
				self._calibrationStep += 1
		elif self._calibrationStep == 1:
			text = 'determining sensor orientation'

			# if we have a valid height compute the yaw orientation of the sensor
			orientationEstimate = self._computeSensorOrientation()
			if orientationEstimate is not None:
				self._sensorOrientation = orientationEstimate
				# apply a post rotation to the trackers (compensation for sensor)
				for bodyPart in self._filteredTrackerDict:
					self._filteredTrackerDict[bodyPart].postEuler(self._sensorOrientation)
				# update the calibration step
				self._calibrationStep += 1
		elif self._calibrationStep == 2:
			text = 'determining sensor position'

			positionEstimate = self._computeSensorPosition()
			if positionEstimate is not None:
				self._sensorPosition = positionEstimate
				# apply a post trans to the trackers (compensation for sensor)
				for bodyPart in self._filteredTrackerDict:
					self._filteredTrackerDict[bodyPart].postTrans(self._sensorPosition)
				# update the calibration step
				self._calibrationStep += 1

		if self._showCalibrationPanel:
			if self._calibrationStep < self._CALIBRATION_END_STEP:
				self._text.message(text)
				self._panel.getPanel().dirtyLayout()
				self._panel.visible(True)
			else:
				self._panel.visible(False)


if __name__ == "__main__":
	import virtual_trackers

	viz.go()

	# load a model
	piazza = viz.add('gallery.osgb')

	# setup the window and view
	viz.window.setSize(1600, 1200)
	viz.MainView.setPosition(0, 1.72, 4)
	viz.MainView.setEuler([180, 0, 0])

	def initAnimator():
		"""Initializes the animator for our test"""
		# add the avatar
		#myAvatar = viz.add('vcc_male2.cfg')
		#avatarSkeleton = skeleton.CompleteCharactersHD(myAvatar)
		# alternatively you could use a regular complete characters and matching
		# skeleton data...
		myAvatar = viz.add('vcc_male.cfg')
		#myAvatar.state(2)



		avatarSkeleton = skeleton.CompleteCharacters(myAvatar)
		print(skeleton.CompleteCharacters.__mro__)
		print(viz.VizAvatar.__mro__)

		#avatarSkeleton._bodyPartDict = {}
		#avatarSkeleton._handModelDict = {}

		bodyParts = {
			skeleton.AVATAR_HEAD, # MUST HAVE A HEAD
			skeleton.AVATAR_L_HAND,
			skeleton.AVATAR_R_HAND,
			skeleton.AVATAR_L_FOOT,
			skeleton.AVATAR_R_FOOT,
			skeleton.AVATAR_PELVIS,
		#	skeleton.AVATAR_L_FOREARM,
		#	skeleton.AVATAR_R_FOREARM,
			skeleton.AVATAR_L_UPPER_ARM,
			skeleton.AVATAR_R_UPPER_ARM,
			skeleton.AVATAR_L_CALF,
			skeleton.AVATAR_R_CALF,
		}

		# add a parent group
		#group = viz.addGroup()
		#group.setEuler([180, 0, 0])
		#myAvatar.setParent(group)

		# init the trackers
		trackerDict = {}
		for bodyPart in bodyParts:
			trackerDict[bodyPart] = virtual_trackers.Keyboard(debug=True)
		# offset the head
		trackerDict[skeleton.AVATAR_HEAD].setPosition(0, 2.82, 0)

		# we can adjust the length of individual joints if need be
		#jointDict = avatarSkeleton.getJointDict()
		#jointDict[skeleton.JOINT_R_ELBOW].setLength(1.0)

		# setup a tracker assignment dictionary using each of the trackers
		# created earlier
		trackerAssignmentDict = {}
		for bodyPart in bodyParts:
			trackerAssignmentDict[bodyPart] = (trackerDict[bodyPart], None, DOF_6DOF)
		# it's possible to set the dof for a tracker individually
		#trackerAssignmentDict[vizconnect.AVATAR_L_FOREARM] = (trackerDict[vizconnect.AVATAR_L_FOREARM], None, animator.DOF_POS)

		# initialize the inverse kinematics animator for avatar
		myAnimator = InverseKinematics(myAvatar, avatarSkeleton, trackerAssignmentDict, debug=False)

		vizact.onkeydown('p', myAnimator.tposeReset)

	initAnimator()
