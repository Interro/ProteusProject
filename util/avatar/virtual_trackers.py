ï»¿"""This module contains implementations of various virtual trackers."""

import math

import viz
import vizact
import vizmat
import vizshape


# global var for keeping track of keyboard trackers
_TRACKER_LIST = [None]*10

VIRTUAL_TRACKER_PRIORITY = -25

DOF_POS = 1
DOF_ORI = 2
DOF_6DOF = 3


def addLineAxes(length=1.0, lineWidth=1):
	""" Add an axes model as set of lines"""
	viz.lineWidth(lineWidth)
	viz.startLayer(viz.LINES)
	viz.vertexColor(viz.RED);	viz.vertex(0, 0, 0); viz.vertex(length, 0, 0)
	viz.vertexColor(viz.GREEN);	viz.vertex(0, 0, 0); viz.vertex(0, length, 0)
	viz.vertexColor(viz.BLUE);	viz.vertex(0, 0, 0); viz.vertex(0, 0, length)
	return viz.endLayer()


class VirtualTracker(viz.VizNode):
	"""
	@param node: the vizard node3d object used by the virtual tracker
	@param debug: if True, tracker has a model representation, if false and no node is specified, a group node is added.
	"""

	def __init__(self, node=None, debug=False):
		self._defaultNode = None
		self._debug = debug
		if node is None:
			if debug:
				node = addLineAxes(length=0.1, lineWidth=5)#vizshape.addAxes(0.1)#vizshape.addSphere(0.1)
				self._defaultNode = node
			else:
				node = viz.addGroup()
				self._defaultNode = node
		self._node = node
		viz.VizNode.__init__(self, self._node.id)
		self._updateEvent = vizact.onupdate(VIRTUAL_TRACKER_PRIORITY, self.onUpdate)
		self._enabled = True

	def onUpdate(self):
		"""Update method called by update function"""
		pass

	def setEnabled(self, *args, **kwargs):
		"""Sets the enabled state of the tracker and update event."""
		self._updateEvent.setEnabled(*args, **kwargs)
		self._enabled = self._updateEvent.getEnabled()

	def getEnabled(self):
		"""Gets the enabled state of the tracker via the update event."""
		return self._updateEvent.getEnabled()

	def remove(self):
		"""Removes the tracker"""
		# remove the node if we created one
		if self._defaultNode:
			self._defaultNode.remove()
		if self._updateEvent:
			self._updateEvent.setEnabled(False)
			self._updateEvent.remove()
		self._node = None

	def setVelocity(self, vel):
		"""Sets the velocity of the tracker"""
		pass


class Keyboard(VirtualTracker):
	"""
	This is a virtual tracker that's currently mapped to keyboard input. Later
	we will allow for more general mappings.

	Multiple copies of the same tracker can be instantiated and you can switch
	between them using the number keys

	This is a walking style tracker, where forward, back, left, and right
	depend on the orientation of the tracker

	@param trackerIndex: the 0-9 index of the tracker
	@param debug: if True, tracker is represented by a sphere.
	"""

	def __init__(self, trackerIndex=-1, speed=0.5, modifier=None, mode=0, **kwargs):
		super(Keyboard, self).__init__(**kwargs)

		self._mode = mode# 0 for wasd 1 for arrows

		# member vars
		self.speed = speed
		self._modifier = modifier

		# Either use the supplied tracker index or get the current+1 then setup
		# so we can use the keyboard to switch to this tracker.
		if trackerIndex == -1:
			# add the tracker and determine the tracker index
			index = 0
			while _TRACKER_LIST[index] is not None:
				index += 1
			self.trackerIndex = index
			_TRACKER_LIST[index] = self
			# alternately to just increase...
			#self.trackerIndex = int(viz.getOption('FAKE_KEYBOARD_TRACKER_INDEX', 0))+1
		else:
			self.trackerIndex = trackerIndex
			_TRACKER_LIST[trackerIndex] = self
		viz.setOption('FAKE_KEYBOARD_TRACKER_INDEX', self.trackerIndex)
		self.switchEvent = vizact.onkeydown(str(self.trackerIndex+1), lambda index: viz.setOption('FAKE_KEYBOARD_TRACKER_INDEX', index), self.trackerIndex)

	def setTrackerIndex(self, index):
		"""Allows tracker index to be reassigned, mostly so we can reassign indices
		in a live mode after trackers have been removed.
		"""
		self.trackerIndex = index
		self.switchEvent = vizact.onkeydown(str(self.trackerIndex+1), lambda index: viz.setOption('FAKE_KEYBOARD_TRACKER_INDEX', index), self.trackerIndex)

	def onUpdate(self):
		"""Function that updates the position and orientation of the tracker. Speed
		is based on the time between update calls.
		"""
		# Manually sampling time, since there's no guarantee that we'll be using
		# a timer function, we don't use viz.elasped.
		dt = viz.getFrameElapsed()

		if self._modifier:
			if not viz.key.isDown(self._modifier, immediate=1):
				return

		if self._mode == 0:
			# run a check for each key
			if viz.key.isDown('x', immediate=1):
				self.adjPosition([0, self.speed*dt, 0])
			if viz.key.isDown('z', immediate=1):
				self.adjPosition([0, -self.speed*dt, 0])
			if viz.key.isDown('d', immediate=1):
				self.adjPosition([self.speed*dt, 0, 0])
			if viz.key.isDown('a', immediate=1):
				self.adjPosition([-self.speed*dt, 0, 0])
			if viz.key.isDown('w', immediate=1):
				self.adjPosition([0, 0, self.speed*dt])
			if viz.key.isDown('s', immediate=1):
				self.adjPosition([0, 0, -self.speed*dt])

			if viz.key.isDown('t', immediate=1):
				self.adjEuler([0, self.speed*dt, 0])
			if viz.key.isDown('g', immediate=1):
				self.adjEuler([0, -self.speed*dt, 0])
			if viz.key.isDown('h', immediate=1):
				self.adjEuler([self.speed*dt, 0, 0])
			if viz.key.isDown('f', immediate=1):
				self.adjEuler([-self.speed*dt, 0, 0])
			if viz.key.isDown('v', immediate=1):
				self.adjEuler([0, 0, self.speed*dt])
			if viz.key.isDown('b', immediate=1):
				self.adjEuler([0, 0, -self.speed*dt])
		else:
			# run a check for each key
			if viz.key.isDown(viz.KEY_PAGE_UP, immediate=1):
				self.adjPosition([0, self.speed*dt, 0])
			if viz.key.isDown(viz.KEY_PAGE_DOWN, immediate=1):
				self.adjPosition([0, -self.speed*dt, 0])
			if viz.key.isDown(viz.KEY_RIGHT, immediate=1):
				self.adjPosition([self.speed*dt, 0, 0])
			if viz.key.isDown(viz.KEY_LEFT, immediate=1):
				self.adjPosition([-self.speed*dt, 0, 0])
			if viz.key.isDown(viz.KEY_UP, immediate=1):
				self.adjPosition([0, 0, self.speed*dt])
			if viz.key.isDown(viz.KEY_DOWN, immediate=1):
				self.adjPosition([0, 0, -self.speed*dt])

	def adjPosition(self, adj):
		"""Function to move forward, back, left, right, etc.. basically any
		directional vector, but relative to the euler of the tracker.
		"""
		if self._node is not None and viz.getOption('FAKE_KEYBOARD_TRACKER_INDEX', 0) == str(self.trackerIndex):
			ori = self._node.getEuler()[0]*math.pi/180.0
			pos = self._node.getPosition(viz.ABS_GLOBAL)
			pos[2] += ((adj[2]*math.cos(ori))-(adj[0]*math.sin(ori)))
			pos[0] += ((adj[2]*math.sin(ori))+(adj[0]*math.cos(ori)))
			pos[1] += adj[1]
			self._node.setPosition(pos, viz.ABS_GLOBAL)

	def adjEuler(self, adj):
		"""Function to change the euler of the node, binds the view to a set range
		of values.
		"""
		if self._node is not None and viz.getOption('FAKE_KEYBOARD_TRACKER_INDEX', 0) == str(self.trackerIndex):
			euler = self._node.getEuler()
			euler[0] += adj[0]*45
			euler[1] += adj[1]*45
			euler[1] = min(89.999, max(-89.999, euler[1]))
			euler[2] += adj[2]*45
			self._node.setEuler(euler)

	def remove(self):
		"""Removes the keyboard tracker"""
		super(Keyboard, self).remove()
		# sort the tracker list, so things are like what you'd expect
		removeIndex = 0
		for i in range(0, 10):
			if _TRACKER_LIST[i] == self:
				removeIndex = i
		for i in range(removeIndex, 9):
			_TRACKER_LIST[i] = _TRACKER_LIST[i+1]
		_TRACKER_LIST[9] = None
		# remove the tracker
		self.switchEvent.remove()
		self.trackerIndex = -1


class LookAt(VirtualTracker):
	"""A tracker which takes a target node which it follows"""
	def __init__(self, eye, target, srcMode=-1, dstMode=-1, up=None, **kwargs):
		super(LookAt, self).__init__(**kwargs)

		self._eye = eye
		self._target = target
		self._srcMode = srcMode
		self._dstMode = dstMode
		self._up = up
		if self._up is None:
			self._up = [0, 1, 0]
		vizact.onupdate(VIRTUAL_TRACKER_PRIORITY, self.onUpdate)

	def getEye(self):
		"""Returns the eye node.

		@return viz.VizNode()
		"""
		return self._eye

	def setEye(self, eye):
		"""Sets the eye node"""
		self._eye = eye

	def getTarget(self):
		"""Gets the target node

		@return viz.VizNode()
		"""
		return self._target

	def setTarget(self, target):
		"""Sets the target node"""
		self._target = target

	def onUpdate(self):
		"""Update method called by update function"""
		mat = vizmat.Transform()
		eyePos = self._eye.getPosition(mode=self._srcMode) if self._srcMode != -1 else self._eye.getPosition()
		targetPos = self._target.getPosition(mode=self._dstMode) if self._dstMode != -1 else self._target.getPosition()
		mat.makeLookAt(eyePos, targetPos, self._up)
		self.setMatrix(mat)


class MouseOrientation(VirtualTracker):
	"""Temporary tracker implementation for using a mouse and a keyboard

	@param rotationSensitivity: the speed of the orientation change
	@param trap: traps the mouse
	@param visible: sets the visibility of the mouse
	@param usingDrag: if true, orientation changes only on mouse drag
	"""

	def __init__(self, rotationSensitivity=0.15, trap=False, visible=True, usingDrag=False, yawOnly=False, **kwargs):
		super(MouseOrientation, self).__init__(**kwargs)

		# member vars
		self.maxVel = 10
		self.rotationSensitivity = rotationSensitivity
		self._usingDrag = usingDrag
		self._yawOnly = yawOnly

#		# add the direct input mouse
#		if not self._usingDrag:
#			d = viz.add('directinput.dle')
#			device = d.getMouseDevices()[0]
#			self.mouse = d.addMouse(device)
#			self.DELTA_VALS = d.MOUSE_RAW_VALUES
		viz.callback(viz.MOUSE_MOVE_EVENT, self.onMouseMove)

		# set the status of the mouse
		if trap:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')
		if not visible:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')

		self._vizWindow = viz.window.getHandle()

	def onMouseMove(self, e):
		"""Callback for when the mouse moves"""
		if self._enabled:
			euler = self.getEuler()
			euler[0] += e.dx*self.rotationSensitivity
			if not self._yawOnly:
				euler[1] += -e.dy*self.rotationSensitivity
				euler[1] = max(-89.9, min(89.9, euler[1]))
			else:
				euler[1] = 0
			self.setEuler(euler, viz.ABS_GLOBAL)

#	def onUpdate(self):
#		if not self._usingDrag:
#			if win32gui.GetForegroundWindow() == self._vizWindow:
#				euler = self.getEuler()
#				mouseDelta = self.mouse.getPosition(self.DELTA_VALS)
#				euler[0] += mouseDelta[0]*self.rotationSensitivity
#				euler[1] += mouseDelta[1]*self.rotationSensitivity
#				euler[1] = max(-89.9, min(89.9, euler[1]))
#				self.setEuler(euler, viz.ABS_GLOBAL)
#				mouseDelta[0] = 0
#				mouseDelta[1] = 0
#		else:
#			if viz.mouse.getState() & viz.MOUSEBUTTON_RIGHT:
#				euler = self.getEuler()
#				pos = viz.mouse.getPosition(viz.WINDOW_NORMALIZED)
#				pos[0] = max(-1, min(1, (pos[0]-0.5)*2.0))*10.0*self.rotationSensitivity
#				pos[1] = max(-1, min(1, -(pos[1]-0.5)*2.0))*10.0*self.rotationSensitivity
#				if math.fabs(pos[0]) > 0.05:
#					euler[0] += pos[0]*math.fabs(pos[0])
#				if math.fabs(pos[1]) > 0.05:
#					euler[1] = max(-89.9, min(89.9, euler[1]+pos[1]*math.fabs(pos[1])))
#				self.setEuler(euler, viz.ABS_GLOBAL)


class MouseAndKeyboardFlying(MouseOrientation):
	"""Tracker implementation for using a mouse and a keyboard to navigate by
	flying.

	@param positionSensitivity: the speed of the position change
	@param trap: traps the mouse
	@param visible: sets the visibility of the mouse
	@param debug: if True, tracker is represented by a sphere.
	"""

	def __init__(self, positionSensitivity=0.5, maxVel=10.44, **kwargs):
		super(MouseAndKeyboardFlying, self).__init__(**kwargs)

		# member vars
		self.maxVel = maxVel
		self.positionSensitivity = positionSensitivity

		# callback to ensure that tracker gets updated each frame
		self.setPosition(0, 1.82, 0)

	def onUpdate(self):
		"""Function that updates the position and orientation of the tracker. Speed
		is based on the time between update calls.
		"""
		dt = viz.getFrameElapsed()

		# check the keys that are being pressed
		vec = [0, 0, 0]
		if viz.key.isDown(viz.KEY_RIGHT, immediate=1):#'d'
			vec[0] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_LEFT, immediate=1):#'a'
			vec[0] -= dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_UP, immediate=1):#'w'
			vec[2] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_DOWN, immediate=1):#'s'
			vec[2] -= dt*self.positionSensitivity

		super(MouseAndKeyboardFlying, self).onUpdate()

		mat = vizmat.Transform()
		mat.makeEuler(self.getEuler())
		posOff = vizmat.Vector(mat.preMultVec(vec))
		self.setPosition(vizmat.Vector(self.getPosition()) + posOff)


class MouseAndKeyboardWalking(MouseOrientation):
	"""Tracker for using a mouse and keyboard to navigate by walking.

	@param positionSensitivity: the speed of the position change
	@param rotationSensitivity: the speed of the orientation change
	@param trap: traps the mouse
	@param visible: sets the visibility of the mouse
	@param debug: if True, tracker is represented by a sphere.
	"""

	def __init__(self, positionSensitivity=0.5, maxVel=10.44, **kwargs):
		super(MouseAndKeyboardWalking, self).__init__(**kwargs)

		# member vars
		self.maxVel = maxVel
		self.positionSensitivity = positionSensitivity

		# shift the tracker up to head height
		self.setPosition(0, 1.82, 0)

	def onUpdate(self):
		"""Update method called by update function"""
		dt = viz.getFrameElapsed()

		# check the keys that are being pressed
		vec = [0, 0, 0]
		if viz.key.isDown(viz.KEY_RIGHT, immediate=1):#'d'
			vec[0] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_LEFT, immediate=1):#'a'
			vec[0] -= dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_UP, immediate=1):#'w'
			vec[2] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_DOWN, immediate=1):#'s'
			vec[2] -= dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_PAGE_UP, immediate=1):
			vec[1] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_PAGE_DOWN, immediate=1):
			vec[1] -= dt*self.positionSensitivity

		super(MouseAndKeyboardWalking, self).onUpdate()

		mat = vizmat.Transform()
		euler = self.getEuler()
		mat.makeEuler([euler[0], 0, 0])
		posOff = vizmat.Vector(mat.preMultVec(vec))
		self.setPosition(vizmat.Vector(self.getPosition()) + posOff)


class TrackerAndKeyboardWalking(VirtualTracker):
	"""Combination tracker and keyboard walking, takes some 6DoF (typically
	desktop mounted tracker, e.g. oculus DK2) and mounts it on a virtual
	tracker, augmenting the yaw with the mouse.
	"""
	def __init__(self, tracker, addMouseOrientation=True, positionSensitivity=0.5, maxVel=10.44, heightOffset=1.82, dof=DOF_6DOF, **kwargs):
		super(TrackerAndKeyboardWalking, self).__init__(**kwargs)

		# member vars
		self.maxVel = maxVel
		self.positionSensitivity = positionSensitivity
		self._tracker = tracker
		self._dof = dof

		self._lastQuat = None
		self._virtualTracker = None
		if addMouseOrientation:
			self._mouseOrientation = MouseOrientation(yawOnly=True)
			self._virtualTracker = self._mouseOrientation
		if dof == DOF_6DOF and not self._virtualTracker:
			self._virtualTracker = viz.addGroup()
			self._virtualTracker.disable(viz.INTERSECTION)

		# shift the tracker up to head height
		self._virtualTracker.setPosition(0, heightOffset, 0)

	def onUpdate(self):
		"""Update method called by update function"""
		dt = viz.getFrameElapsed()

		if self._virtualTracker:
			mat = vizmat.Transform()
			mat.setQuat(self._virtualTracker.getQuat())
			if self._tracker:
				mat.preQuat(self._tracker.getQuat())
			self.setQuat(mat.getQuat())
		# check the keys that are being pressed
		vec = [0, 0, 0]
		if viz.key.isDown(viz.KEY_RIGHT, immediate=1):
			vec[0] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_LEFT, immediate=1):
			vec[0] -= dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_UP, immediate=1):
			vec[2] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_DOWN, immediate=1):
			vec[2] -= dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_PAGE_UP, immediate=1):
			vec[1] += dt*self.positionSensitivity
		if viz.key.isDown(viz.KEY_PAGE_DOWN, immediate=1):
			vec[1] -= dt*self.positionSensitivity

		super(TrackerAndKeyboardWalking, self).onUpdate()

		yawMat = vizmat.Transform()
		euler = self.getEuler()
		yawMat.makeEuler([euler[0], 0, 0])
		posOff = vizmat.Vector(yawMat.preMultVec(vec))
		virPos = vizmat.Vector(self._virtualTracker.getPosition()) + posOff
		if not self._tracker or self._dof == DOF_ORI:
			self.setPosition(virPos)
		else:
			virYawMat = vizmat.Transform()
			virYawMat.setQuat(self._virtualTracker.getQuat())
			physPos = vizmat.Vector(self._tracker.getPosition())
			physPos = virYawMat.preMultVec(physPos)

			if self._tracker and self._lastQuat:
				mat = vizmat.Transform()
				mat.setQuat(self._virtualTracker.getQuat())
				virPos -= mat.preMultVec(self._tracker.getPosition())
				mat.setQuat(self._lastQuat)
				virPos += mat.preMultVec(self._tracker.getPosition())
				self.setPosition(virPos + physPos)
			else:
				self.setPosition(virPos + physPos)
		self._virtualTracker.setPosition(virPos)

		if self._tracker:
			self._lastQuat = self._virtualTracker.getQuat()


class OrientationAndKeyboardWalking(TrackerAndKeyboardWalking):
	"""Subclass for TrackerAndKeyboardWalking that uses orientation
	specifically. Maintained for backwards compatability."""
	def __init__(self, orientationTracker, addMouseOrientation=True, positionSensitivity=0.5, maxVel=10.44, **kwargs):
		super(OrientationAndKeyboardWalking, self).__init__(tracker=orientationTracker,
															addMouseOrientation=addMouseOrientation,
															positionSensitivity=positionSensitivity,
															maxVel=maxVel,
															dof=DOF_ORI,
															**kwargs)



class ScrollWheel(VirtualTracker):
	"""Tracker implementation for using a mouse and a keyboard.
	This class will be replaced with a generic extender tracker once trackers can
	have their inputs remapped in a similar way to transports/tools.

	@param trap: traps the mouse
	@param visible: sets the visibility of the mouse
	@param followMouse: if True, tracker orientation will follow the mouse.
	"""

	def __init__(self, trap=False, visible=True, followMouse=False, scaleVelocityWithDistance=True, extensionAccel=100.0, **kwargs):
		super(ScrollWheel, self).__init__(**kwargs)

		self.distance = 0.0
		self.extensionAccel = extensionAccel#m/s^2
		self.followMouse = followMouse
		self.scaleVelocityWithDistance = scaleVelocityWithDistance

		self._accel = 0
		self._vel = 0

		# set the status of the mouse
		if trap:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')
		if not visible:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')

		# event functions for triggering extension and retraction
		vizact.onwheelup(self.extend, 1)
		vizact.onwheeldown(self.retract, 1)

	def extend(self, mag):
		"""Extends the tracker"""
		self._accel = mag*mag*self.extensionAccel

	def retract(self, mag):
		"""Retracts the tracker"""
		self._accel = -mag*mag*self.extensionAccel

	def onUpdate(self):
		"""Callback that updates the tracker"""
		if self._enabled:
			# get the line along the mouse position
			dt = 0.03#viz.getFrameElapsed()
			self._vel += self._accel*dt
			if self.scaleVelocityWithDistance:
				self._vel *= max(1.0, self.distance)
			self.distance += self._vel*dt
			if self.followMouse:
				line = viz.MainWindow.screenToWorld(viz.mouse.getPosition())
				mat = vizmat.Transform()
				mat.makeEuler(viz.MainView.getEuler(viz.ABS_GLOBAL))
				mat = mat.inverse()
				dir = mat.preMultVec(line.dir)
				dir = vizmat.Vector(dir)
				dir.normalize()
				vector = dir*self.distance
			else:
				vector = [0, 0, self.distance]
			self._vel = 0
			self._accel = 0
			self.setPosition(vector)

	def setVelocity(self, vel):
		"""Sets the velocity of the tracker"""
		self._vel = vel[:]


class MousePick(VirtualTracker):
	"""Tracker implementation for using a mouse and a keyboard.
	This class will be replaced with a generic extender tracker once trackers can
	have their inputs remapped in a similar way to transports/tools.

	@param trap: traps the mouse
	@param visible: sets the visibility of the mouse
	@param followMouse: if True, tracker orientation will follow the mouse.
	"""

	def __init__(self,
					window=None,
					mode=viz.WORLD,
					eye=viz.BOTH_EYE,
					ignoreBackFace=True,
					followMouse=True,
					**kwargs):

		super(MousePick, self).__init__(**kwargs)

		if window is None:
			window = viz.MainWindow

		self._window = window
		self._info = True
		self._eye = eye
		self._mode = mode
		self._ignoreBackFace = ignoreBackFace
		self._intersectionList = []
		self._followMouse = followMouse

		self._distance = 1.0

		vizact.onmousedown(viz.MOUSEBUTTON_LEFT, self.jumpTo)
#		self._updateEvent.remove()
#		self._updateEvent = vizact.onupdate(VIRTUAL_TRACKER_PRIORITY, self.onUpdate)

	def setIntersectionList(self, list):
		"""Sets the intersection list"""
		self._intersectionList = list[:]

	def jumpTo(self):
		"""Callback that updates the tracker"""
		# get the line along the mouse position
		intersection = self._window.pick(eye=self._eye,
											mode=self._mode,
											info=self._info,
											ignoreBackFace=self._ignoreBackFace)

		self.setPosition(intersection.point, viz.ABS_GLOBAL)
		self._distance = (vizmat.Vector(intersection.point) - self._window.getView().getPosition(viz.ABS_GLOBAL)).length()

	def onUpdate(self):
		"""Callback that updates the tracker"""
		# get the line along the mouse position
		if self._intersectionList:
			intersection = self._window.pick(eye=self._eye,
												mode=self._mode,
												info=self._info,
												ignoreBackFace=self._ignoreBackFace)

			if intersection.object in self._intersectionList:
				self.setPosition(intersection.point, viz.ABS_GLOBAL)

		if self._followMouse:
			line = self._window.screenToWorld(viz.mouse.getPosition())
			ray = [line[3]-line[0], line[4]-line[1], line[5]-line[2]]
			mat = self._window.getView().getMatrix(viz.ABS_GLOBAL)
			mat.setEuler([0, 0, 0])
			vec = vizmat.Vector(ray)
			vec.normalize()
			vector = vec*self._distance
			vector = mat.preMultVec(vector)
		else:
			vector = [0, 0, self._distance]

		self.setPosition(vector)


class WindowPickOrientation(VirtualTracker):
	"""Orientation tracker which uses a picked window point to determine
	the orientation needed to match the corresponding projected line.
	"""
	def __init__(self,
					window=None,
					eye=viz.BOTH_EYE,
					mode=viz.WINDOW_NORMALIZED,
					local=True,
					**kwargs):

		super(WindowPickOrientation, self).__init__(**kwargs)

		if window is None:
			window = viz.MainWindow

		self._window = window
		self._eye = eye
		self._mode = mode
		self._local = local
		self._view = None
		if hasattr(self._window, 'getView'):
			self._view = self._window.getView()
		else:
			self._local = False

	def setWindowPoint(self, point):
		"""Sets the window point"""
		if self._local:
			tempMat = self._view.getMatrix()
			self._view.setMatrix(vizmat.Transform())
		line = self._window.screenToWorld(point, self._eye, self._mode)
		mat = vizmat.Transform()
		mat.makeLookAt([0, 0, 0], line.dir, [0, 1, 0])
		self.setMatrix(mat)
#		self.setMatrix(self._window.screenToWorld(point, self._eye, self._mode).matrix)
		if self._local:
			self._view.setMatrix(tempMat)


class MousePickOrientation(WindowPickOrientation):
	"""Convenience subclass of WindowPickOrientation which uses the mouse
	explicitly to provide the pick location.
	"""
	def __init__(self,
					eye=viz.BOTH_EYE,
					trap=False,
					visible=True,
					**kwargs):

		super(MousePickOrientation, self).__init__(**kwargs)

		# set the status of the mouse
		if trap:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')
		if not visible:
			viz.logNotice('**Notice: mouse settings via virtual trackers are deprecated')

	def onUpdate(self):
		"""Update method called by update function"""
		# scale and bound the position to the window
		pos = viz.mouse.getPosition(immediate=True)
		self.setWindowPoint(pos)


class MovingAverageFilter(object):
	"""Generic moving average filter class"""
	def __init__(self, max=60):
		self._max = max

		self._list = []
		self._total = None
		self._avg = None
		self._currentIndex = 0

	def add(self, val):
		"""Add an item to the filter, when full, fifo"""
		itemCount = float(len(self._list))
		if self._avg is None:
			self._total = val
		else:
			self._total = (self._avg*itemCount)+val

		if itemCount < self._max:
			self._list.append(val)
			itemCount += 1.0
		else:
			self._total -= self._list[self._currentIndex]
			self._list[self._currentIndex] = val

		self._avg = self._total / itemCount
		self._currentIndex = int((self._currentIndex+1)%self._max)

	def isFull(self):
		"""Returns True if the list is full."""
		return len(self._list) == self._max

	def getAvg(self):
		"""Returns the average sample size"""
		return self._avg

L = 0
R = 1
C = 2

class OpticalHeading(viz.VizNode):
	"""Optical heading implementation. Uses two position trackers and one
	orientation tracker and fuses the data.
	"""

	def __init__(self,
					leftPosTracker,
					rightPosTracker,
					oriTracker,
					distance=-1,
					jumpThreshold=1.0, # amount in meters a tracker can jump in a single frame before it's counted as an error
					distanceTolerance=10.0, # as a percent with 0 min 100 is twice distance
					reliableFrameRequirement=60, # number of reliable frames required before optical heading correction is updated
					interpolationWeight=0.5,
					stableResetTime=2.0,
					stableAcceptableDeviation=0.05,
					resetAngleThreshold=5.0, # min angle variation which would allow a reset to occur when stationary
					debug=False,
					*args, **kwargs):
		if debug:
			super(OpticalHeading, self).__init__(id=vizshape.addAxes(0.1).id, *args, **kwargs)
		else:
			super(OpticalHeading, self).__init__(id=viz.addGroup().id, *args, **kwargs)

		self._distance = distance

		self._leftPosTracker = leftPosTracker
		self._rightPosTracker = rightPosTracker
		self._oriTracker = oriTracker
		self._jumpThreshold = jumpThreshold
		self._distanceTolerance = max(0.0, distanceTolerance/100.0)
		self._reliableFrameRequirement = reliableFrameRequirement
		self._interpolationWeight = interpolationWeight
		self._stableResetTime = stableResetTime
		self._stableAcceptableDeviation = stableAcceptableDeviation
		self._resetAngleThreshold = resetAngleThreshold
		self._debug = debug

#		self._stableCount = 0
#		self._stableAvg = vizmat.Vector(0, 0, 0)
		self._stableStartPoint = [vizmat.Vector(0, 0, 0), vizmat.Vector(0, 0, 0), vizmat.Vector(0, 0, 0)]
		self._stableLightTime = 0
		self._defaultInterpolationWeight = interpolationWeight

		self._minDistThreshold = self._distance * (1.0-self._distanceTolerance)
		self._maxDistThreshold = self._distance * (1.0+self._distanceTolerance)

		self._distanceFilter = MovingAverageFilter(max=360)

		self._predictiveMax = 3
		self._prevState = None

		if self._debug:
			self._leftArrow = vizshape.addArrow(0.1, axis=vizshape.AXIS_Y)
			self._leftArrow.color(viz.RED)
			self._rightArrow = vizshape.addArrow(0.1, axis=vizshape.AXIS_Y)
			self._rightArrow.color(viz.BLUE)

			self._leftCyl = vizshape.addCylinder(0.15, radius=0.01, axis=vizshape.AXIS_Y)
			self._leftCyl.color(viz.GREEN)
			self._rightCyl = vizshape.addCylinder(0.15, radius=0.01, axis=vizshape.AXIS_Y)
			self._rightCyl.color(viz.GREEN)

		self._up = vizmat.Vector([0, 1, 0])
		self._updateEvent = vizact.onupdate(VIRTUAL_TRACKER_PRIORITY, self.update)

	def _swap(self, state):
		"""Swaps the lights and associated values (pNorm, pVec) for the given
		state. Increments swapCount.
		"""
		state.swapCount = self._prevState.swapCount + 1
		state.pNorm = -state.pNorm
		state.pVec = -state.pVec
		# swap positions
		temp = state.p[R]
		state.p[R] = state.p[L]
		state.p[L] = temp

	def _verifyChanged(self, state, prev):
		"""Simple check for static or inactive trackers. If both trackers are
		the same position as last time, or just swapped, then return False.
		"""
		if state.p[L] == prev.p[L] and state.p[R] == prev.p[R]:
			return False
		if state.p[R] == prev.p[L] and state.p[L] == prev.p[R]:
			return False
		return True

	def _verifyDistance(self, state):
		"""Simple check for that the distance between the lights in the given
		state is within the set thresholds. If not, returns False.
		"""
		if self._distanceFilter.isFull() or self._distance > 0:
			if state.pDist < self._minDistThreshold or state.pDist > self._maxDistThreshold:
				return False
		return True

	def _verifySet(self, state, prev):
		"""Check if the points are within the distance threshold, iff we have
		# a valid distance threshold
		"""
		if not self._verifyDistance(state):
			if self._debug:
				viz.logDebug("frame:\t{0}\t invalid distance {1}".format(viz.getFrameNumber(), state.pDist))
			return False

		# find the last reliable relative pVec
		# get the relative vec from ori tracker to pVec, since mounted, orientation diff should be relatively
		# stable from frame to frame. This purposefully doesn't include correction.
		if prev is not None and state.pVecRelativeLastReliable is not None:
			inv = state.oMat.inverse()
			rel = inv.preMultVec(state.pVec)
			angle = vizmat.AngleBetweenVector(state.pVecRelativeLastReliable, rel)
			# if it's greater than 130 then the trackers have likely swapped
			if angle > 30.0 and angle < 130.0:#NOTE sets a max frame to frame drift of 30 deg which should never happen... but still
				if self._debug:
					viz.logDebug("distance between markers correct, but angle too steep", angle, viz.getFrameNumber())
				return False

		# iff they're within the distance threshold, check if swapped
		if prev is not None:
			# check if the x vector flipped instantaneously, indicates trackers
			# have swapped, so unswap them (only if both are valid)
			if state.pNorm*state.nMat[0:3] < 0:
				self._swap(state)
				if self._debug:
					viz.logDebug("frame:\t{0}\t swapping flipped x".format(viz.getFrameNumber()))
		return True

#	def _verifyReasonable(self, state, prev, jumpThreshold):
#		"""Checks for a valid tracker, if the tracker:
#		1.) has been returned to the origin
#		2.) has made a large jump
#		then return False. Otherwise return True
#		"""
#		# get the static confidence, static confidence is not cumulative
#		state.lost[L] = not _getReasonable(state.p[L],
#											prev.p[L],
#											jumpThreshold)
#		state.lost[R] = not _getReasonable(state.p[R],
#											prev.p[R],
#											jumpThreshold)

	def _verifyID(self, state, prev):
		"""Tries to determine if the lights are swapped. Checks the prediction
		accuracy of the normal values, swapped left/right lights, and determining
		the left and right lights using the center prediction and the opposing light.

		It weighs the combined confidence of the predicted normal left/right and
		center left/right against the swapped left/right.
		"""
		distance = self._getDistance(state)
		# measure the confidence using the scaled versions of positions, and vectors
		normalLeft = self._getStaticConfidence(state.p[L]*1000.0,
											prev.p[L]*1000.0,
											prev.v[L]*1000.0)
		normalRight = self._getStaticConfidence(state.p[R]*1000.0,
											prev.p[R]*1000.0,
											prev.v[R]*1000.0)

		normalCenterLeft = self._getStaticConfidence((state.p[L]+state.nMat.preMultVec([distance/2.0, 0, 0]))*1000.0,
											prev.p[C]*1000.0,
											prev.v[C]*1000.0)
		normalCenterRight = self._getStaticConfidence((state.p[R]-state.nMat.preMultVec([distance/2.0, 0, 0]))*1000.0,
											prev.p[C]*1000.0,
											prev.v[C]*1000.0)

		swappedLeft = self._getStaticConfidence(state.p[L]*1000.0,
												prev.p[R]*1000.0,
												prev.v[R]*1000.0)
		swappedLeft += self._getStaticConfidence((state.p[L]-state.nMat.preMultVec([distance/2.0, 0, 0]))*1000.0,
												prev.p[C]*1000.0,
												prev.v[C]*1000.0)

		swappedRight = self._getStaticConfidence(state.p[R]*1000.0,
												prev.p[L]*1000.0,
												prev.v[L]*1000.0)
		swappedRight += self._getStaticConfidence((state.p[R]+state.nMat.preMultVec([distance/2.0, 0, 0]))*1000.0,
												prev.p[C]*1000.0,
												prev.v[C]*1000.0)

		swapped = False
		if normalLeft > normalRight:
			if abs(swappedLeft) > abs(normalLeft+normalCenterLeft)*1.5:
				if self._debug:
					viz.logDebug("swapping id, left {0}".format(viz.getFrameNumber()))
				swapped = True
		else:
			if abs(swappedRight) > abs(normalRight+normalCenterRight)*1.5:
				if self._debug:
					viz.logDebug("swapping id, right {0}".format(viz.getFrameNumber()))
				swapped = True
		if swapped:
			self._swap(state)

	def _verifyTrackers(self, state, prev):
		""" we know that the two trackers are off by some amount of distance.
		We need to know which tracker is incorrect. Using dead reckoning determine
		which tracker is likely to have jumped.

		We need to check for both jumps and slow movement apart,

		1.) If a tracker doesn't move, don't increase the confidence
		2.) If a tracker moves in the direction of dead reckoning increase confidence
		in scale with distance moved, if good distance
		3.) If a tracker moves in the opposite direction of dead reckoning decrease
		the confidence by distance moved, or if not good distance...
		"""
		# if the static confidence is much higher when one of the trackers is swapped then they're likely swapped

		# get the static confidence, static confidence is not cumulative
		self._updateStaticConfidence(state, prev)
		self._updateDynamicConfidence(state, prev)
		self._updateValidityConfidence(state, prev)

		# use whichever tracker has the highest validity confidence
		# use multipliers to prevent swapping for close confidence values
		lm = 200.0 if prev.valid[L] else 100.0
		rm = 200.0 if prev.valid[R] else 100.0
		if state.cV[L] > state.cV[R]*rm:
			state.valid[L] = True
			state.valid[R] = False
		elif state.cV[R] > state.cV[L]*lm:
			state.valid[R] = True
			state.valid[L] = False

	def _getState(self, prev):
		"""Returns the current state"""
		p = [None]*3
		p[R] = vizmat.Vector(self._rightPosTracker.getPosition())
		p[L] = vizmat.Vector(self._leftPosTracker.getPosition())
		p[C] = (p[R]+p[L])/2.0

		# can't compute vector until the end
		v = [vizmat.Vector(0, 0, 0)]*3

		# get the x vector, and normalize the vector
		pVec = p[R]-p[L]
		pDist = pVec.length()
		pNorm = pVec.normalize()
		pVecRelativeLastReliable = None
		if prev is not None:
			pVecRelativeLastReliable = prev.pVecRelativeLastReliable

		# get some predictive average vectors so we can filter out the high frequency noise
		predict = [None]*3
		if prev is None:
			predict[R] = MovingAverageFilter(max=self._predictiveMax)
			predict[L] = MovingAverageFilter(max=self._predictiveMax)
			predict[C] = MovingAverageFilter(max=self._predictiveMax)
		else:
			predict = prev.predict

		# get new orientation data
		oMat = viz.Matrix.quat(self._oriTracker.getQuat())
		if prev is not None:
			sMat = vizmat.Transform(prev.sMat)
		else:
			sMat = vizmat.Transform()
		# get the current estimated orientation using the last heading
		# correction (if available) and the current orientation matrix
		nMat = vizmat.Transform(oMat)
		nMat.postMult(sMat)

		# setup confidence vars
		cD = [0]*2
		cS = [0]*2
		cV = [0]*2
		valid = [True]*2

		# add a state variable
		state = viz.Data(# position data
							p=p,
							v=v,
							# orientation data
							oMat=oMat,
							sMat=sMat,
							nMat=nMat,
							# two tracker data
							pVec=pVec,
							pNorm=pNorm,
							pDist=pDist,
							swapCount=0,
							pVecRelativeLastReliable=pVecRelativeLastReliable,
							# validity
							cD=cD,
							cS=cS,
							cV=cV,
							valid=valid,
							# predictive vars
							predict=predict,
							)

		return state

	def _getStaticConfidence(self,
								pos,
								last,
								vec,
								nearRange=2500.0):
		"""Returns the static confidence. Measures previous prediction against
		current pos. Any errors are uniformly reported, see dynamic confidence
		as a contrast.
		"""
		# very high confidence within a certain range
		#(2500mm) => 50mm distance
		estimated = last+vec
		estimationError = (pos-estimated).length2()
		confidence = max(0, nearRange-estimationError)
		return confidence

	def _getDynamicConfidence(self,
								pos,
								last,
								vec):
		"""Returns the dynamic confidence. The dynamic confidence is weighted
		by how much the directional movement agrees. If the tracker and prediction
		are both moving in the same direction the resulting confidence will be
		high. It will be higher if they're both close to each other as well, but
		if they are moving in opposite directions, the confidence will be low,
		and lower for vastly different values.
		"""
		# get the dot product of the predicted vector and the current vector
		# no max, no min
		currentVec = pos-last
		pL = vec.length2()
		cL = currentVec.length2()
		# how much the distances agree
		distCon = 1.0/max(1.0, abs(pL - cL))
		# how large the distances are
		avgDist = (pL+cL)/2.0
		vn = vec.normalize()
		cn = currentVec.normalize()
		confidence = (vn*cn)*avgDist*distCon
		return confidence*confidence

	def _getDistance(self, state):
		"""Gets the distance between the lights for the given state"""
		if self._distance <= 0:
			avg = self._distanceFilter.getAvg()
			if avg is None:
				distance = state.pDist
			else:
				distance = avg
			return distance
		return self._distance

	def _getStableLights(self, state):
		"""Returns true if the lights are stable in the given state.
		Check if the lights are stable in that they haven't moved.
		Compares current state against running average.
		"""
		for i in [L, R]:
			# verification using average
#			stableTotal = self._stableAvg[i]*self._stableCount + state.p[i]
#			self._stableCount += 1
#			avg = stableTotal / self._stableCount
#			dist = (avg-state.p[i]).length()
			# verification using single point
			dist = (self._stableStartPoint[i]-state.p[i]).length()
			# if one light is not close enough throw the whole thing out
			if dist > self._stableAcceptableDeviation:
#				self._stableCount = 0
				self._stableStartPoint[i] = state.p[i]
				return False
		# verify that the distance is correct
		if not self._verifyDistance(state):
			return False

		# if we made it through then return that the lights are stable
		return True

	def _getStableTrackers(self, state):
		"""Return True if the lights are stable, haven't moved for the required amount of time
		"""
		elapsed = viz.getFrameElapsed()
		if self._getStableLights(state):
			self._stableLightTime += elapsed
		else:
			self._stableLightTime = 0

		# check if the trackers have been in roughly the same position for x amount of time
		if self._stableLightTime > self._stableResetTime:
			return True
		return False

#	def _getReasonable(self, current, prev, jumpThreshold):
#		valid = True
#		# if the tracker has jumped between states, assume it's lost
#		distance = vizmat.Distance(current, prev)
#		if distance > jumpThreshold:
#			valid = False
#		# if the tracker is at 0, 0, 0 assume it's lost
#		if current == [0, 0, 0]:
#			valid = False
#		return valid

	def reset(self):
		state = self._prevState
		up = self._up
		# get some variables from the state
		nMat = state.nMat
		oMat = state.oMat
		sMat = state.sMat
		pVec = state.pVec

		# get a new up vector
		nY = nMat.preMultVec([0, 1, 0])

		# get the position matrix
		pMat = vizmat.Transform()
		pZ = pVec.cross(nY).normalize()
		pY = pZ.cross(pVec).normalize()
		pX = pY.cross(pZ).normalize()
		pMat.set([pX[0], pX[1], pX[2], 0.0,
					pY[0], pY[1], pY[2], 0.0,
					pZ[0], pZ[1], pZ[2], 0.0,
					0.0, 0.0, 0.0, 1.0])

		# get the error matrix
		eMat = oMat.inverse()
		eMat.postMult(pMat)

		# get the heading correction from the error matrix
		# flatten the error along the vertical axis
		# error is already relative to current orientation, so this makes
		# the error effectively a yaw correction
		hZ = vizmat.Vector(eMat.get()[0:3]).cross(up)
		hY = up
		hX = up.cross(hZ)

		hMat = vizmat.Transform()
		hMat.set([hX[0], hX[1], hX[2], 0.0,
					hY[0], hY[1], hY[2], 0.0,
					hZ[0], hZ[1], hZ[2], 0.0,
					0.0, 0.0, 0.0, 1.0])

		sMat.setQuat(hMat.getQuat())

		# use the yaw to reset the heading of the tracking device
		nMat = vizmat.Transform(oMat)
		nMat.postMult(state.sMat)

	def update(self):
		"""Check if trackers are correct distance, if not check for which point isn't updating
		if point isn't updating or distance from last point is too large use more valid point
		plus orientation
		Added code for testing validity of tracking objects, etc
		"""
		# ensure that the trackers are valid
		if self._oriTracker is None or self._leftPosTracker is None or self._rightPosTracker is None:
			return

		# get the current state
		prev = self._prevState
		state = self._getState(prev)

		self._updateThresholds(state)

		# check if the lights are stable
		# if the trackers are stable trust them
		trustLights = self._getStableTrackers(state)
#		if trustLights:
#			self._interpolationWeight = 0.9
#		else:
#			self._interpolationWeight = self._defaultInterpolationWeight

		self._interpolationWeight = self._defaultInterpolationWeight
		# if we trust the lights, then reset immediately if the distance is over threshold
		if trustLights and vizmat.AngleBetweenVector(state.pNorm, vizmat.Vector(state.nMat.getSide())) > self._resetAngleThreshold:
			self.reset()

		if trustLights and state.pNorm*vizmat.Vector(state.nMat.getSide()) < 0.95:
			# if it's valid, try to get the fused data
			for _ in range(0, 10):# weight more for trusted
				self._updateDistance(state)
			self._updateFusion(state, prev)
			# if reliable save the pVec
			inv = state.oMat.inverse()
			state.pVecRelativeLastReliable = inv.preMultVec(state.pVec)
		elif prev is not None:
			# check if the tracker has changed, if not return
			changed = self._verifyChanged(state, prev)
			if not changed:
				# update orientation information before returning
				nMat = viz.Matrix.quat(self._oriTracker.getQuat())
				nMat.postMult(prev.sMat)
				self.setQuat(nMat.getQuat())
				return

			# verify the set data, check if the points are the right distance,
			# this will fix a number of problems
			validSet = self._verifySet(state, prev)

			# if the set's not valid we need to apply checks to the trackers
			# to see if there's a tracker that's been lost, or that's jumped
			if not validSet:
				# check if trackers have swapped using the static confidence
				self._verifyID(state, prev)
				# make a basic check to see if the tracker is unreasonable
				#self._verifyReasonable(state, prev)
				# check to see which tracker is valid using the dynamic confidence
				self._verifyTrackers(state, prev)

			# if it's valid, try to get the fused data
			if validSet:
				self._updateDistance(state)
				self._updateFusion(state, prev)
				# if reliable save the pVec
				inv = state.oMat.inverse()
				state.pVecRelativeLastReliable = inv.preMultVec(state.pVec)

			# finalize the state and get the extrapolated position for any lost
			# tracker and vectors for next frame's estimates
			self._updateExtrapolated(state, prev)

			self._updateVectors(state, prev)

		if self._debug:
			self._updateDebug(state)

		# set the position and orientation of this tracker
		self.setPosition(state.p[C])
		self.setQuat(state.nMat.getQuat())

		# save the state
		self._prevState = state

	def _updateDebug(self, state):
		"""Update the debug arrows, etc"""
		self._leftArrow.setPosition(state.p[L])
		self._rightArrow.setPosition(state.p[R])
		self._leftCyl.setPosition(state.p[L])
		self._rightCyl.setPosition(state.p[R])

		if state.valid[L]:
			self._leftCyl.color(vizmat.Vector(viz.GREEN)*min(1.0, float(state.cV[L])))
		else:
			self._leftCyl.color(viz.RED)
		if state.valid[R]:
			self._rightCyl.color(vizmat.Vector(viz.GREEN)*min(1.0, float(state.cV[R])))
		else:
			self._rightCyl.color(viz.RED)

	def _updateDistance(self, state):
		"""If we've got valid left and right and we need more data, then update
		the filter that we use for determining the distance between the lights
		in the unspecified format.
		"""
		if state.valid[L] and state.valid[R]:
			if self._distance <= 0:
				self._distanceFilter.add(state.pDist)

	def _updateFusion(self, state, prev):
		"""Update the fusion of the sensors. Orientations are slerp'd together
		using the current interpolation weight.
		"""
		up = self._up
		# get some variables from the state
		nMat = state.nMat
		oMat = state.oMat
		sMat = state.sMat
		pVec = state.pVec
		pNorm = state.pNorm

		# get a new up vector
		nY = nMat.preMultVec([0, 1, 0])

		# get the position matrix
		pMat = vizmat.Transform()
		pZ = pVec.cross(nY).normalize()
		pY = pZ.cross(pVec).normalize()
		pX = pY.cross(pZ).normalize()
		pMat.set([pX[0], pX[1], pX[2], 0.0,
					pY[0], pY[1], pY[2], 0.0,
					pZ[0], pZ[1], pZ[2], 0.0,
					0.0, 0.0, 0.0, 1.0])

		# get the error matrix
		eMat = oMat.inverse()
		eMat.postMult(pMat)

		# get the heading correction from the error matrix
		# flatten the error along the vertical axis
		# error is already relative to current orientation, so this makes
		# the error effectively a yaw correction
		hZ = vizmat.Vector(eMat.get()[0:3]).cross(up)
		hY = up
		hX = up.cross(hZ)

		# create a matrix heading vectors
		hMat = vizmat.Transform()
		hMat.set([hX[0], hX[1], hX[2], 0.0,
					hY[0], hY[1], hY[2], 0.0,
					hZ[0], hZ[1], hZ[2], 0.0,
					0.0, 0.0, 0.0, 1.0])

		# compute the new hMat by slerping the quat data, fix
		# the tracker information
		if prev is not None:
			# verify that the pNorm (essentially an x vector) is not close to the
			# vertical axis
			vertAlignDot = abs(pNorm*[0, 1, 0])
			if vertAlignDot < 0.6:
				weight = abs(1.0-vertAlignDot)*self._interpolationWeight
			else:
				weight = 0
			# scale the interpolation so that it's relative to the current movement,
			# if not moving, no interpolation
			weight *= vizmat.AngleBetweenVector(prev.oMat.getSide(), oMat.getSide()) / 180.0
			sMat.setQuat(vizmat.slerp(prev.sMat.getQuat(), hMat.getQuat(), weight))
		else:
			sMat.setQuat(hMat.getQuat())

		# use the yaw to reset the heading of the tracking device
		nMat = vizmat.Transform(oMat)
		nMat.postMult(state.sMat)

	def _updateExtrapolated(self, state, prev):
		"""Update the extrapolated content."""
		distance = self._getDistance(state)

		# if we lost both trackers use the last position, determine left/right
		# using orientation
		if not state.valid[L] and not state.valid[R]:
			state.p[C] = vizmat.Vector(prev.p[C])
			state.p[R] = state.p[C]+state.nMat.preMultVec([distance/2.0, 0, 0])
			state.p[L] = state.p[C]+state.nMat.preMultVec([-distance/2.0, 0, 0])

		# if we lost a single tracker compute new point and center
#		if state.valid[L] and not state.valid[R]:
#			state.p[R] = state.p[L]+state.nMat.preMultVec([distance, 0, 0])
#			state.p[C] = (state.p[R]+state.p[L])/2.0
#		if not state.valid[L] and state.valid[R]:
#			state.p[L] = state.p[R]+state.nMat.preMultVec([-distance, 0, 0])
#			state.p[C] = (state.p[R]+state.p[L])/2.0

	def _updateVectors(self, state, prev):
		"""Update the current velocity vector and prediction for left, right,
		and center.
		"""
		if prev is not None:
			for i in [L, R, C]:
				state.v[i] = state.p[i]-prev.p[i]
				state.predict[i].add(state.v[i])

	def _updateThresholds(self, state):
		"""Update the thresholds based on the newly computed distance from
		the distance filter, only applicable if we are using auto compute
		for the distance
		"""
		# get the mid point
		if self._distance <= 0:
			distance = self._getDistance(state)
			if self._distanceFilter.isFull():
				self._minDistThreshold = distance * (1.0-self._distanceTolerance)
				self._maxDistThreshold = distance * (1.0+self._distanceTolerance)
			else:
				self._minDistThreshold = 0.0
				self._maxDistThreshold = 10.0

	def _updateStaticConfidence(self, state, prev):
		"""Updates the static confidence (state.cS) for each light in the
		given state.
		"""
		for i in [L, R]:
			state.cS[i] = self._getStaticConfidence(state.p[i]*1000.0, prev.p[i]*1000.0, prev.v[i]*1000.0)

	def _updateDynamicConfidence(self, state, prev):
		"""Updates the dynamic confidence (state.cD) for each light in the
		given state.
		"""
		for i in [L, R]:
			if prev.predict[i].getAvg() is not None:
				state.cD[i] = self._getDynamicConfidence(state.p[i]*1000.0,
														(prev.p[i]-(prev.predict[i].getAvg()*(self._predictiveMax-1)))*1000.0,
														prev.predict[i].getAvg()*self._predictiveMax*1000.0)
			else:
				state.cD[i] += self._getDynamicConfidence(state.p[i]*1000.0, prev.p[i]*1000.0, prev.v[i]*1000.0)

	def _updateValidityConfidence(self, state, prev):
		"""Updates the validity confidence (state.cV). Tries to determine if left
		and right lights are valid.
		"""
		# add in dynamic confidence, which is best indicator, use only a little
		# of static confidence
		staticWeight = 0.0001
		for i in [L, R]:
			if state.cV[i] == 0:
				state.cV[i] = 2.0*(prev.cV[i] + state.cD[i]) + (state.cS[i]*staticWeight)
			else:
				state.cV[i] = prev.cV[i] + state.cD[i] + (state.cS[i]*staticWeight)
			state.cV[i] = min(100000, abs(state.cV[i]))
			# if the moving average filter is 0 or close to it, then decrease the confidence
			if prev.predict[i].getAvg() is not None:
				if prev.predict[i].getAvg().length() == 0.0:
					state.cV[i] /= 2.0

