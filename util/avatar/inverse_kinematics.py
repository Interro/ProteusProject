ï»¿"""Module that contains inverse kinematics code for avatars"""

import math

import viz
import vizshape
import vizmat


DOF_POS = 1
DOF_ORI = 2
DOF_6DOF = 3


def addLineAxes(length=1.0, lineWidth=1):
	""" Add an axes model as set of lines"""
	viz.lineWidth(lineWidth)
	viz.startLayer(viz.LINES)
	viz.lineWidth(lineWidth)
	viz.vertexColor(viz.RED);	viz.vertex(0, 0, 0);	viz.vertex(length, 0, 0)
	viz.vertexColor(viz.GREEN);	viz.vertex(0, 0, 0);	viz.vertex(0, length, 0)
	viz.vertexColor(viz.BLUE);	viz.vertex(0, 0, 0);	viz.vertex(0, 0, length)
	return viz.endLayer()


def makeDebugBall(size, color, lineWidth=7, hiddenAlpha=0.3):
	"""Creates and returns a debug ball, useful for following particular
	transformations since it provides an occluded object as well.
	"""
	debugBall = vizshape.addSphere(size)
	debugBall.color(color)
	debugBall.disable(viz.INTERSECTION)
	#debugBall.setParent(self._avatar)
	debugBallHidden = vizshape.addSphere(size)
	debugBallHidden.alpha(hiddenAlpha)
	debugBallHidden.disable(viz.DEPTH_TEST)
	debugBallHidden.drawOrder(10000)
	debugBallHidden.setParent(debugBall)
	debugBallHidden.color(color)
	addLineAxes(3.0*size, lineWidth).setParent(debugBall)
	hiddenAxes = addLineAxes(3.0*size, lineWidth)
	hiddenAxes.alpha(hiddenAlpha)
	hiddenAxes.disable(viz.DEPTH_TEST)
	hiddenAxes.drawOrder(10000)
	hiddenAxes.setParent(debugBall)
	return debugBall


def makeDebugCylinder(height, radius, color, lineWidth=7, hiddenAlpha=0.3):
	"""Creates and returns a debug Cclinder, useful for following particular
	transformations since it provides an occluded object as well.
	"""
	debugBall = vizshape.addCylinder(height, radius)
	debugBall.color(color)
	#debugBall.setParent(self._avatar)
	debugBallHidden = vizshape.addCylinder(height, radius)
	debugBallHidden.alpha(hiddenAlpha)
	debugBallHidden.disable(viz.DEPTH_TEST)
	debugBallHidden.drawOrder(10000)
	debugBallHidden.setParent(debugBall)
	debugBallHidden.color(color)
	axes = addLineAxes(3.0*radius, lineWidth)
	axes.setParent(debugBall)
	axes.setScale([1.0, height/radius, 1.0])
	hiddenAxes = addLineAxes(2.5*radius, lineWidth)
	hiddenAxes.setScale([1.0, height/radius, 1.0])
	hiddenAxes.alpha(hiddenAlpha)
	hiddenAxes.disable(viz.DEPTH_TEST)
	hiddenAxes.drawOrder(10000)
	hiddenAxes.setParent(debugBall)
	return debugBall


class _Joint(object):
	"""Base class for joints"""
	def __init__(self):
		pass
	
	def refreshLength(self, scale):
		"""Refreshes the length of the bones used by the joint."""
		pass


class _BoneJoint(_Joint):
	"""Base class for joints"""
	def __init__(self, bone, parentBone, childBone):
		_Joint.__init__(self)
		self.bone = bone
		self.parentBone = parentBone
		self.childBone = childBone
		
		self.childBoneLength = 0
		self.parentBoneLength = 0
		
		self._maxDist = 0# there's a max distance that the bone can reach
		self._minDist = 0# there's a min distance that the bone can reach
		
		self.refreshLength()
	
	def refreshLength(self):
		"""Refreshes the length of the bones used by the joint."""
		bonePos = vizmat.Vector(self.bone.getPosition(viz.ABS_GLOBAL))
		if self.childBone is not None:
			childPos = vizmat.Vector(self.childBone.getPosition(viz.ABS_GLOBAL))
			self.childBoneLength = (childPos-bonePos).length()
		else:
			self.childBoneLength = 0
		
		if self.parentBone is not None:
			parentPos = vizmat.Vector(self.parentBone.getPosition(viz.ABS_GLOBAL))
			self.parentBoneLength = (bonePos-parentPos).length()
		else:
			self.parentBoneLength = 0
		
		# find the min and max dist
		oLength = self.childBoneLength
		iLength = self.parentBoneLength
		self._maxDist = iLength+oLength# there's a max distance that the bone can reach
		self._minDist = abs(iLength-oLength)# there's a min distance that the bone can reach


class _Hinge(_BoneJoint):
	"""Implementation of a hinge joint.
	
	@param bone: the bone to extend, if elbow then forearm
	@param bone: the base bone, if elbow then upperArm
	@param bone: the child bone, if elbow then hand
	@param axis: the axis of rotation (a 3 vector)
	
	@args object, object, object, []
	"""
	def __init__(self, bone, parentBone, childBone, axis):
		_BoneJoint.__init__(self, bone, parentBone, childBone)
		
		self.axis = axis
	
	def extendToDist(self, dist):
		"""Extends the joint to the given distance"""
		dist = max(self._minDist, min(self._maxDist, dist))
		C = _angleCfromABC(self.childBoneLength, self.parentBoneLength, dist)
		self.bone.setAxisAngle([self.axis[0], self.axis[1], self.axis[2], C+180])


class _Ball(_BoneJoint):
	"""Generic ball joint"""
	def __init__(self, bone, parentBone, childBone, yawAxis, pitchAxis, rollAxis):
		_BoneJoint.__init__(self, bone, parentBone, childBone)
		
		self.pitchAxis = pitchAxis
		self.yawAxis = yawAxis
		self.rollAxis = rollAxis
	
	def rotateHipToPoint(self, dt, rotOffsetMat=None):
		"""Rotates the inner joint so that the line between the inner joint
		and the end of the outer joint points toward the specified point
		"""
		#
		# remove the existing local transform on the bone
		self.bone.setQuat([0, 0, 0, 1])
		
		# set the matrix to the orientation of the bone in global coordinates
		gmat = vizmat.Transform()
		gmat.setQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		# apply the rotation offset to transform to a standard reference frame
		if rotOffsetMat:
			gmat.preMult(rotOffsetMat)
		# get the inverse matrix and mult to get the shoulder relative dt
		inv = gmat.inverse()
		srdt = inv.preMultVec(dt)
		
		h = 180+_getHeadingFromVectorXZ([srdt[0], -srdt[2], srdt[1]])*180.0/math.pi
		p = _getPitchFromVector([srdt[0], -srdt[2], srdt[1]])*180.0/math.pi
		
		# get the quat from the bone, same as what we used for transformation
		mat = vizmat.Transform()
		mat.preQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		if rotOffsetMat:
			mat.preMult(rotOffsetMat)
		
		mat.preAxisAngle([0, 0, 1, h])
		mat.preAxisAngle([0, 1, 0, p])
		self.bone.setQuat(mat.getQuat(), viz.ABS_GLOBAL)
	
	def rotateToPointElbow(self, dt, rotOffsetMat=None):
		"""Rotates the inner joint so that the line between the inner joint
		and the end of the outer joint points toward the specified point
		"""
		# We need to transform a global coordinate into the reference frame
		# of the avatar's joint, so we need to get the global transformation
		# of the joint and multiply the dt by the inverse of that
		#
		# remove the existing local transform on the bone
		self.bone.setQuat([0, 0, 0, 1])
		
		# set the matrix to the orientation of the bone in global coordinates
		gmat = vizmat.Transform()
		gmat.setQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		# apply the rotation offset to transform to a standard reference frame
		if rotOffsetMat:
			gmat.preMult(rotOffsetMat)
		# get the inverse matrix and mult to get the shoulder relative dt
		inv = gmat.inverse()
		srdt = inv.preMultVec(dt)
		
		# get the heading and pitch
		h = 180.0+_getHeadingFromVectorXZ(srdt)*180.0/math.pi
		p = _getPitchFromVector(srdt)*180.0/math.pi
		
		# apply the rotations
		mat = vizmat.Transform()
		mat.preQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		mat.preAxisAngle([self.yawAxis[0], self.yawAxis[1], self.yawAxis[2], h])
		mat.preAxisAngle([self.pitchAxis[0], self.pitchAxis[1], self.pitchAxis[2], p])
		mat.preMult(rotOffsetMat)
		
		self.bone.setQuat(mat.getQuat(), viz.ABS_GLOBAL)
	
	def rotateToPoint(self, dt, rotOffsetMat=None, sign=1):
		"""Rotates the inner joint so that the line between the inner joint
		and the end of the outer joint points toward the specified point
		"""
		# We need to transform a global coordinate into the reference frame
		# of the avatar's joint, so we need to get the global transformation
		# of the joint and multiply the dt by the inverse of that
		# remove the existing local transform on the bone
		self.bone.setQuat([0, 0, 0, 1])
		
		# set the matrix to the orientation of the bone in global coordinates
		gmat = vizmat.Transform()
		gmat.setQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		# apply the rotation offset to transform to a standard reference frame
		if rotOffsetMat:
			gmat.preMult(rotOffsetMat)
		# get the inverse matrix and mult to get the shoulder relative dt
		srdt = gmat.inverse().preMultVec(dt)
		
		defaultRoll = 240
		h = 180.0+_getHeadingFromVectorXZ(srdt)*180.0/math.pi
		p = _getPitchFromVector(srdt)*180.0/math.pi
		r = defaultRoll
		
		# check dt to make sure that it's not too close to the yaw axis
		dtn = vizmat.Vector(srdt)
		dtn.normalize()
		dist = 0.3
		dot = dtn * [0, 1, 0]# projection onto vertical axis, relative to joint orientation
		nDot = (abs(dot)-dist)/(1.0-dist)# normalized [0-1] dot
		if dot > dist:
			mat = vizmat.Transform()
			mat.preAxisAngle([0, sign*1, 0, h])
			mat.preAxisAngle([1, 0, 0, p])
			zv1 = vizmat.Vector(mat.preMultVec([1, 0, 0]))
			
			zv2 = vizmat.Vector([1, 0, 0])
			
			zv3 = vizmat.Vector([0, 1, 0])
			
			elbowYaw = 30-(math.atan2(zv1.cross(zv2).length(), zv1*zv2) *180.0/math.pi) * math.copysign(1, zv1.cross(zv2)*vizmat.Vector(zv3))
			
			# now we need to average default angle and target angle, for now,
			# projecting vectors, adding those, then getting the angle of the result
			zv2 = vizmat.Vector([0, 0, 1])
			
			mat = vizmat.Transform()
			mat.setEuler([elbowYaw, 0, 0])
			ev = mat.preMultVec([0, 0, nDot])
			
			mat = vizmat.Transform()
			mat.setEuler([defaultRoll, 0, 0])
			rv = vizmat.Vector(mat.preMultVec([0, 0, (1.0-nDot)]))
			
			rv = rv+ev
			rv.normalize()
			
			r = -(math.atan2(rv.cross(zv2).length(), rv*zv2) *180.0/math.pi) * math.copysign(1, rv.cross(zv2)*vizmat.Vector(zv3))
			
		elif dot < -dist:
			mat = vizmat.Transform()
			mat.preAxisAngle([0, sign*1, 0, h])
			mat.preAxisAngle([1, 0, 0, p])
			zv1 = vizmat.Vector(mat.preMultVec([1, 0, 0]))
			
			zv2 = vizmat.Vector([1, 0, 0])
			
			zv3 = vizmat.Vector([0, 1, 0])
			
			elbowYaw = 120+(math.atan2(zv1.cross(zv2).length(), zv1*zv2) *180.0/math.pi) * math.copysign(1, zv1.cross(zv2)*vizmat.Vector(zv3))
			
			# now we need to average default angle and target angle, for now,
			# projecting vectors, adding those, then getting the angle of the result
			zv2 = vizmat.Vector([0, 0, 1])
			
			mat = vizmat.Transform()
			mat.setEuler([elbowYaw, 0, 0])
			ev = mat.preMultVec([0, 0, nDot])
			
			mat = vizmat.Transform()
			mat.setEuler([defaultRoll, 0, 0])
			rv = vizmat.Vector(mat.preMultVec([0, 0, (1.0-nDot)]))
			
			rv = rv+ev
			rv.normalize()
			
			r = -(math.atan2(rv.cross(zv2).length(), rv*zv2) *180.0/math.pi) * math.copysign(1, rv.cross(zv2)*vizmat.Vector(zv3))
		
		mat = vizmat.Transform()
		mat.preQuat(self.bone.getQuat(viz.ABS_GLOBAL))
		mat.preAxisAngle([self.yawAxis[0], self.yawAxis[1], self.yawAxis[2], h])
		mat.preAxisAngle([self.pitchAxis[0], self.pitchAxis[1], self.pitchAxis[2], p])
		mat.preAxisAngle([self.rollAxis[0], self.rollAxis[1], self.rollAxis[2], r])
		mat.preMult(rotOffsetMat)
		
		self.bone.setQuat(mat.getQuat(), viz.ABS_GLOBAL)
	
	def appendYawRotation(self, angle):
		"""Rotates the inner joint so that the line between the inner joint
		and the end of the outer joint points toward the specified point
		"""
		# rotate around the current up vector
		#TODO: fix this so it's not needing to mult by a single value of the axis angle
		# need some multiplication by the difference between shoulder rotating angle and elbow rotating angle
		angle = angle*self.yawAxis[2]
		mat = vizmat.Transform()
		mat.setEuler(self.bone.getEuler())
		newVec = mat.preMultVec(self.yawAxis)# get the new up vec
		mat.postAxisAngle([newVec[0], newVec[1], newVec[2], angle])
		self.bone.setEuler(mat.getEuler())
	
	def appendPitchRotation(self, angle):
		"""Rotates the inner joint so that the line between the inner joint
		and the end of the outer joint points toward the specified point
		"""
		# rotate around the current up vector
		#TODO: fix this so it's not needing to mult by a single value of the axis angle
		# need some multiplication by the difference between shoulder rotating angle and elbow rotating angle
		angle = angle*self.pitchAxis[2]
		mat = vizmat.Transform()
		mat.setEuler(self.bone.getEuler())
		newVec = mat.preMultVec(self.pitchAxis)# get the new up vec
		mat.postAxisAngle([newVec[0], newVec[1], newVec[2], angle])
		self.bone.setEuler(mat.getEuler())


class _Arm(object):
	"""Handles IK for an arm"""
	
	def __init__(self,
					avatar,
					handTracker,
					forearmTracker,
					upperArmTracker,
					uparm,
					lowarm,
					hand,
					elbow,
					shoulder,
					wrist,
					rotOffsetMat,
					sign=1,
					debug=False):
		self._avatar = avatar
		self._handTracker = handTracker
		self._forearmTracker = forearmTracker
		self._upperArmTracker = upperArmTracker
		self._upperArm = uparm
		self._forearm = lowarm
		self._hand = hand
		self._shoulder = shoulder
		self._elbow = elbow
		self._wrist = wrist
		self._upperArm.lock(1)
		self._rotOffsetMat = rotOffsetMat
		self._sign = sign
		self._lastElbowRoll = 0
		self._debug = debug
		if self._debug:
			self._debugBall = makeDebugBall(0.05, viz.RED)
			if self._forearmTracker:
				self._debugForearmBall = makeDebugBall(0.05, viz.YELLOW)
			if self._upperArmTracker:
				self._debugUpperArmBall = makeDebugBall(0.05, viz.BLUE)
	
	def update(self, handMat, forearmMat=None, upperArmMat=None):
		"""Updates the IK for the arm, a hand matrix must be provided, but
		a forearm matrix is optional.
		"""
		point = handMat.getPosition()
		if self._debug:
			self._debugBall.setMatrix(handMat)
		
		forearmPoint = None
		if forearmMat:
			forearmPoint = vizmat.Vector(forearmMat.getPosition())
			if self._debug:
				self._debugForearmBall.setPosition(forearmPoint)
		
		upperArmPoint = None
		if upperArmMat:
			upperArmPoint = vizmat.Vector(upperArmMat.getPosition())
			if self._debug:
				self._debugUpperArmBall.setPosition(upperArmPoint)
		
		# extend the arm to the needed distance
		dT = point - vizmat.Vector(self._upperArm.getPosition(viz.ABS_GLOBAL))
		dist = dT.length()
		self._elbow.extendToDist(dist)
		
		# rotate the shoulder back so there's a straight line from shoulder to hand
		oLength = self._elbow.childBoneLength
		iLength = self._elbow.parentBoneLength
		maxDist = iLength + oLength# there's a max distance that the bone can reach
		minDist = abs(iLength - oLength)# there's a min distance that the bone can reach
		dist = max(minDist, min(maxDist, dist))
		A = _angleAfromABC(oLength, iLength, dist)
		
		# rotate the shoulder to point at the point
		if forearmPoint:
			self._shoulder.rotateToPointElbow(dT, rotOffsetMat=self._rotOffsetMat)
			
			shoulderMat = self._upperArm.getMatrix(viz.ABS_GLOBAL)
			smInv = shoulderMat.inverse()
			
			v1 = forearmPoint - handMat.getPosition()
			v1 = v1.normalize()
			v2 = forearmPoint - shoulderMat.getPosition()
			v2 = v2.normalize()
			dot = abs(v1*v2)
			# set a certain range in which we ignore the elbow information since we don't have
			# a tracker on the shoulders, and we don't want the elbow tracker swinging wildly.
			# ease into a standard interpretation using weight. 
			minRollThreshold = 0.8
			maxRollThreshold = 0.99
			
			ep = vizmat.Vector(smInv.preMultVec(forearmPoint))
			# compute roll needed for shoulder vector
			# project onto ZY plane
			ep[0] = 0
			ep = ep.normalize()
			hp = [0, -1, 0]
			
			# now compute the rotation from ep to hp 
			roll = 180-(math.atan2(ep.cross(hp).length(), ep*hp) *180.0/math.pi) * math.copysign(1, ep.cross(hp)*vizmat.Vector([1, 0, 0]))
			if dot > minRollThreshold:# make sure we don't have a locked arm
				hMat = vizmat.Transform()
				hMat.setQuat(smInv.getQuat())
				hMat.setPosition(smInv.preMultVec(forearmPoint))
				ep = vizmat.Vector(hMat.preMultVec([0, 0, -0.5]))# send to back for now
				# compute roll needed for shoulder vector
				# project onto ZY plane
				ep[0] = 0
				ep = ep.normalize()
				newRoll = 180-(math.atan2(ep.cross(hp).length(), ep*hp) *180.0/math.pi) * math.copysign(1, ep.cross(hp)*vizmat.Vector([1, 0, 0]))
				matLast = vizmat.Transform()
				matLast.postAxisAngle([1, 0, 0, roll])
				matNow = vizmat.Transform()
				matNow.postAxisAngle([1, 0, 0, newRoll])
				a = max(0.0, (dot-minRollThreshold)/(maxRollThreshold-minRollThreshold))
				a = 1.0-max(0.0, min(1.0, a))
				
				matMerged = vizmat.Transform()
				matMerged.setQuat(vizmat.slerp(matNow.getQuat(), matLast.getQuat(), a))
				shoulderMat.preQuat(matMerged.getQuat())
			else:
				shoulderMat.preAxisAngle([1, 0, 0, roll])
			self._upperArm.setMatrix(shoulderMat, viz.ABS_GLOBAL)
		elif upperArmPoint:
			self._shoulder.rotateToPointElbow(dT, rotOffsetMat=self._rotOffsetMat)
			
			shoulderMat = self._upperArm.getMatrix(viz.ABS_GLOBAL)
			smInv = shoulderMat.inverse()
			
			# handle rotation globally
			upperArmOri = vizmat.Transform()
			upperArmOri.setQuat(upperArmMat.getQuat())
			ev = vizmat.Vector(upperArmOri.preMultVec([-self._sign, 0, 0]))
			# vec along shoulder mat
			shoulderOri = vizmat.Transform()
			shoulderOri.setQuat(shoulderMat.getQuat())
			sv = vizmat.Vector(shoulderOri.preMultVec([0, 1, 0]))
			
			# get rotation vector
			rv = vizmat.Vector(shoulderOri.preMultVec([1, 0, 0]))
			rv = rv.normalize()
			
			# flatten ev, sv to rotational plane
			ev -= rv*(ev*rv)
			sv -= rv*(sv*rv)
			
			ev = ev.normalize()
			sv = sv.normalize()
			
			# now compute the rotation from ep to hp 
			roll = 180-(math.atan2(ev.cross(sv).length(), ev*sv) *180.0/math.pi) * math.copysign(1, ev.cross(sv)*vizmat.Vector(rv))
			shoulderMat.preAxisAngle([1, 0, 0, roll])
			self._upperArm.setMatrix(shoulderMat, viz.ABS_GLOBAL)
		else:
			self._shoulder.rotateToPoint(dT, rotOffsetMat=self._rotOffsetMat, sign=self._sign)
		# rotate shoulder out since the elbow is bent in
		self._shoulder.appendYawRotation(A)


class _Leg(object):
	"""Handles IK for a leg"""
	
	def __init__(self,
					avatar,
					footTracker,
					calfTracker,
					thigh,
					calf,
					foot,
					hip,
					knee,
					ankle,
					rotOffsetMat,
					sign=1,
					debug=False):
		self._avatar = avatar
		self._footTracker = footTracker
		self._calfTracker = calfTracker
		self._thigh = thigh
		self._calf = calf
		self._foot = foot
		self._knee = knee
		self._hip = hip
		self._ankle = ankle
		self._thigh.lock(1)
		self._rotOffsetMat = rotOffsetMat
		self._sign = sign
		self._lastKneeRoll = 0
		self._debug = debug
		if self._debug:
			self._debugBall = makeDebugBall(0.05, viz.GREEN)
			if self._calfTracker:
				self._debugCalfBall = makeDebugBall(0.05, viz.YELLOW)
	
	def update(self, footMat, calfMat=None):
		"""Updates the IK for the arm, a foot matrix must be provided, but
		a calf matrix is optional.
		"""
		point = footMat.getPosition()
		if self._debug:
			self._debugBall.setPosition(point)
		
		calfPoint = None
		if calfMat:
			calfPoint = calfMat.getPosition()
			if self._debug:
				self._debugCalfBall.setPosition(calfPoint)
		
		# extend the calf to the needed distance
		dT = point - vizmat.Vector(self._thigh.getPosition(viz.ABS_GLOBAL))
		dist = dT.length()
		self._knee.extendToDist(dist)
		
		# rotate the hip back so there's a straight line from hip to hand
		oLength = self._knee.childBoneLength
		iLength = self._knee.parentBoneLength
		maxDist = iLength+oLength# there's a max distance that the bone can reach	
		minDist = abs(iLength-oLength)# there's a min distance that the bone can reach
		dist = max(minDist, min(maxDist, dist))
		A = _angleAfromABC(oLength, iLength, dist)
		
		# rotate the hip to point at the point
		self._hip.rotateHipToPoint(dT, rotOffsetMat=self._rotOffsetMat)
		
		# if the knee point is defined then roll the leg towards the knee.
		if calfPoint:
			# TODO: add support for orientation trackers as well
			# current method works only for position trackers
			# determine the elbow tracker orientation
			calfPoint = vizmat.Vector(calfPoint)
			footPoint = vizmat.Vector(self._foot.getPosition(viz.ABS_GLOBAL))
			
			# determine the knee tracker orientation
			hipMat = self._thigh.getMatrix(viz.ABS_GLOBAL)
			smInv = hipMat.inverse()
			ep = vizmat.Vector(smInv.preMultVec(calfPoint))
			hp = vizmat.Vector(smInv.preMultVec(footPoint))
			# compute roll needed for hip vector
			# project onto YZ plane
			ep[0] = 0
			hp[0] = 0
			ep.normalize()
			hp.normalize()
			
			# now compute the rotation from ep to hp 
			roll = 180-(math.atan2(ep.cross(hp).length(), ep*hp) *180.0/math.pi) * math.copysign(1, ep.cross(hp)*vizmat.Vector([1, 0, 0]))
			
			aSize = 0.75
			if dT.length() < maxDist*aSize:# make sure we don't have a locked knee
				hipMat.preAxisAngle([1, 0, 0, roll])
				self._thigh.setMatrix(hipMat, viz.ABS_GLOBAL)
				self._lastKneeRoll = 0#roll
			else:
				matLast = vizmat.Transform()
				matLast.postAxisAngle([1, 0, 0, self._lastKneeRoll])
				matNow = vizmat.Transform()
				matNow.postAxisAngle([1, 0, 0, roll])
				a = max(0.0, (maxDist - dT.length())/maxDist)
				a = 1.0-max(0.0, min(1.0, a/(1.0-aSize)))
				a *= a
				matMerged = vizmat.Transform()
				matMerged.setQuat(vizmat.slerp(matNow.getQuat(), matLast.getQuat(), a))
				hipMat.preQuat(matMerged.getQuat())
				self._thigh.setMatrix(hipMat, viz.ABS_GLOBAL)
		
		# rotate hip out since the knee is bent in
		self._hip.appendPitchRotation(A)


#######################################################
# Utility Functions
#######################################################

def _getHeadingFromVectorXZ(d):
	"""Returns the heading/yaw rotation needed to transform the vector d's 
	normalized projection onto the XZ plane into the vector [0, 0, 1].
	
	@param d: a vector
	
	@args: []
	@return float
	"""
	dNorm = vizmat.Vector([d[0], 0, d[2]])
	if dNorm.length() > 0:
		dNorm = dNorm / dNorm.length()
	else:
		return 0
	heading = math.asin(dNorm[2])
	if dNorm[0] < 0:
		heading = math.pi-heading
	return heading-math.pi


def _getPitchFromVectorYZ(d):
	"""Returns the pitch rotation needed to transform the vector d's
	normalized projection onto the YZ plane into the vector [0, 0, 1]. The difference between
	this function and _getPitchFromVector is that this function flattens the vector
	onto the YZ plane before getting the pitch. Generally, it's a matter of priority
	for which should be used. Flattened is generally better for first priority of
	the transformations being used.
	
	@param d: a vector
	
	@args: []
	@return float
	"""
	d = [0, d[1], d[2]]
	dNorm = vizmat.Vector(d)
	if dNorm.length() > 0:
		dNorm = dNorm / dNorm.length()
	else:
		return 0
	pitch = math.asin(dNorm[1])
	return pitch


def _getPitchFromVector(d):
	"""Returns the pitch rotation given by the y component of the vector.
	
	@param d: a vector
	
	@args: []
	@return float
	"""
	dNorm = vizmat.Vector(d)
	if dNorm.length() > 0:
		dNorm = dNorm / dNorm.length()
	else:
		return 0
	pitch = math.asin(dNorm[1])
	return pitch


def _getRollFromVector(d):
	"""Returns the roll rotation given by the x component of the vector.
	
	@param d: a vector
	
	@args: []
	@return float
	"""
	dNorm = vizmat.Vector(d)
	if dNorm.length() > 0:
		dNorm = dNorm / dNorm.length()
	else:
		return 0
	roll = math.asin(dNorm[0])
	return roll


def _angleCfromABC(lA, lB, lC):
	"""Calculate angles given 3 vectors in a triangle"""
	if (2 * lA * lB) != 0:
		costheta = max(-1.0, min(1.0, (lA*lA + lB*lB - lC*lC) / (2 * lA * lB)))
		angle = math.degrees(math.acos(costheta))
	else:
		angle = 0
	return angle


def _angleAfromABC(lA, lB, lC):
	"""Calculate angles given 3 vectors in a triangle."""
	return _angleCfromABC(lB, lC, lA)


def _angleBfromABC(lA, lB, lC):
	"""Calculate angles given 3 vectors in a triangle."""
	return _angleCfromABC(lA, lC, lB)


def _signedAngle(a, b, rotAxis):
	"""Returns the signed angle given two vectors and a rotation axis."""
	acb = a.cross(b)
	sign = math.copysign(1, acb*vizmat.Vector(rotAxis))
	dot = a*b
	angle = -math.atan2(acb.length(), dot)
	if dot > 0.99:
		angle *= ((1.0-dot)/0.01)
	return angle * sign * 180.0/math.pi

