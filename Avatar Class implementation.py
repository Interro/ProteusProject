ï»¿import textwrap
import collections

import viz
import vizcam
import vizact
import vizdlg
import vizinfo
import vizinput
import vizmat
import viztask
import vizmic
from util.avatar import virtual_trackers
from util.avatar import animator
from util.avatar import skeleton
from util.avatar import inverse_kinematics

viz.go()

main_view = viz.MainView


# Set up a convenient camera handler, view, and window
cameraHandler = vizcam.PivotNavigate(center=[-2, 1, 0], sensitivity=[0.5, 0.5])
cameraHandler.rotateTo([3.9, 1, 2.8])
viz.window.setSize(1600, 1000)

# Load a model with convenient placement
piazza = viz.add('gallery.osgb')

piazza.setEuler(180, 0, 0)

#predetermine points for avatars to start
#SpawnPoints = {(0, 1.7, -6)}

# to trigger it to reload of Inverse Kinematics Animator

BODY_PART_NAMES = collections.OrderedDict() # Pretty names for use in the menu
BODY_PART_NAMES[skeleton.AVATAR_HEAD] = 'Head'
BODY_PART_NAMES[skeleton.AVATAR_L_HAND] = 'Left Hand'
BODY_PART_NAMES[skeleton.AVATAR_R_HAND] = 'Right Hand'
BODY_PART_NAMES[skeleton.AVATAR_L_FOOT] = 'Left Foot'
BODY_PART_NAMES[skeleton.AVATAR_R_FOOT] = 'Right Foot'
BODY_PART_NAMES[skeleton.AVATAR_PELVIS] = 'Pelvis'
BODY_PART_NAMES[skeleton.AVATAR_L_FOREARM] = 'Left Forearm'
BODY_PART_NAMES[skeleton.AVATAR_R_FOREARM] = 'Right Forearm'
BODY_PART_NAMES[skeleton.AVATAR_L_CALF] = 'Left Calf'
BODY_PART_NAMES[skeleton.AVATAR_R_CALF] = 'Right Calf'

# create a dict with the reversed mappings
NAMES_TO_BODY_PARTS = {pn: bp for bp, pn in BODY_PART_NAMES.iteritems()}

# setup a dictionary with all the body part names
ALL_BODY_PARTS = BODY_PART_NAMES.keys()

# Define a few common presets:
PRESET_ASSIGNMENTS = collections.OrderedDict()
PRESET_ASSIGNMENTS['All Body Parts'] = ALL_BODY_PARTS[:]
PRESET_ASSIGNMENTS['Head, and Hands'] = ALL_BODY_PARTS[:3]
PRESET_ASSIGNMENTS['Head, Hands, and Feet'] = ALL_BODY_PARTS[:5]




def chooseAvatarAndSkeleton(forUser=True):
	"""Returns the user selected avatar, and the associated skeleton

	@return viz.VizNode(), skeleton._Skeleton()
	"""
	requestedAvatar = None
	requestedSkeleton = None
	options = [
		'Male',
		'Female',
		'Male 2',
		'Choose your own Complete Characters Model',
		'Choose your own Complete CharactersHD Model',
		'Choose your own HumanAutoParse character']

	if forUser:
		choice = vizinput.choose("Select a Character to Represent the User", options)
	else:
		choice = vizinput.choose("Select a Character to Represent the User's Mentor", options)

	if choice == 0:
		requestedAvatar = viz.add('vcc_male.cfg')
		requestedSkeleton = skeleton.CompleteCharacters(requestedAvatar)
	elif choice == 1:
		requestedAvatar = viz.add('vcc_female.cfg')
		requestedSkeleton = skeleton.CompleteCharacters(requestedAvatar)
	elif choice == 2:
		requestedAvatar = viz.add('vcc_male2.cfg')
		requestedSkeleton = skeleton.CompleteCharactersHD(requestedAvatar)
	elif choice == 3:
		# Allows loading of custom Complete Characters avatar
		try:
			customCharacterFilePath = vizinput.fileOpen(filter=[('Cal 3D Files', '*.cfg')], directory='./')
			requestedAvatar = viz.add(customCharacterFilePath)
			requestedSkeleton = skeleton.CompleteCharacters(requestedAvatar)
		except ValueError:
			requestedAvatar = viz.add('vcc_male.cfg')
			requestedSkeleton = skeleton.CompleteCharacters(requestedAvatar)
	elif choice == 4:
		# Allows loading of custom Complete Characters HD avatar
		try:
			customCharacterFilePath = vizinput.fileOpen(filter=[('Cal 3D Files', '*.cfg')], directory='./')
			requestedAvatar = viz.add(customCharacterFilePath)
			requestedSkeleton = skeleton.CompleteCharactersHD(requestedAvatar)
		except ValueError:
			requestedAvatar = viz.add('vcc_male2.cfg')
			requestedSkeleton = skeleton.CompleteCharactersHD(requestedAvatar)
	elif choice == 5:
		# Allows loading of custom HumanAutoParse avatar
		try:
			customCharacterFilePath = vizinput.fileOpen(filter=[('Cal 3D Files', '*.cfg')], directory='./')
			requestedAvatar = viz.add(customCharacterFilePath)
			requestedSkeleton = skeleton.HumanAutoParse(requestedAvatar)
		except ValueError:
			requestedAvatar = viz.add('vcc_male.cfg')
			requestedSkeleton = skeleton.CompleteCharacters(requestedAvatar)
	else:
		raise ValueError
	return requestedAvatar, requestedSkeleton



class ProteusAvatar(object):
	"""Represents the User.
	Underlying the Source Character is a VizAvatar.
	"""
	def __init__(self):
		avatarObj, skeletonObj = chooseAvatarAndSkeleton()
		self._avatar = avatarObj
		self._skeleton = skeletonObj

		self._voice = []

		# Add a group with offset and parent the underlying avatar to it
		self._group = viz.addGroup()
		self._group.setPosition(0, 1.7, -6)
		self._group.setEuler([0, 0, 0])
		self._avatar.setParent(self._group)

		# Add a floating overhead in-world label
		inWorldLabel = viz.addText("User", pos=[0, 1.9, -6], euler=[180, 0, 0], scale=[0.2]*3)
		inWorldLabel.alignment(viz.ALIGN_CENTER_BASE)
		inWorldLabel.setBackdrop(viz.BACKDROP_RIGHT_BOTTOM)

		# add a keyboard tracker which can be used to move a paused avatar's group nodes
		self._keyboardTracker = virtual_trackers.MouseAndKeyboardWalking(debug=False)
		#self.OculusTracker = virtual_trackers.MouseAndKeyboardWalking(debug=False)
		#self.OculusHandTracker = virtual_trackers.
		self._keyboardTracker.setParent(self._group)
		self._manualllyAdjustedBodyPart = skeleton.AVATAR_HEAD
		self._keyboardTracker.visible(False)


		# Set the starting body parts to the full list of body parts
		self._bodyPartsInUse = PRESET_ASSIGNMENTS['Head, and Hands']

		# Create a dictionary of empty Groups to serve as Trackers
		# (which are to be provided data from the Underlying Avatar's bones)
		self._trackerDict = {}
		self._fillDatasets()

		self._trackerDofDict = {}
		for bodyPart in self._bodyPartsInUse:
			self._trackerDofDict[bodyPart] = animator.DOF_6DOF

		self._ikAnimator = None
		self._reloadIK()


		# Set up the two update functions, in priority order
		#vizact.onupdate(0, self._onUpdate)


	def getAvatar(self):
		"""Returns the avatar used

		@return viz.VizNode()
		"""
		return self._avatar

	def getSkeleton(self):
		"""Returns the avatar skelly used

		@return viz.VizNode()
		"""
		return self._skeleton

	def face(self,obj,head=None,clamp=None):
		"""Attatch a face object to the head"""
		if isinstance(obj,str):
			newface = addFace(obj)
			if newface.valid():
				self.setFace(newface,head,clamp)
				return newface
			else:
				return None
		self.setFace(obj,head,clamp)

	def getBodyPartsInUse(self):
		"""Returns a list of all the body parts currently in use

		@return []
		"""
		return self._bodyPartsInUse

	def setBodyPartsInUse(self, bodyPartList):
		"""Sets the trackers that are in use by the avatar"""
		self._bodyPartsInUse = bodyPartList[:]

	def getTrackerDict(self):
		"""Returns the "trackers" provided by the Character.

		@return {}
		"""
		return self._trackerDict

	def _fillDatasets(self):
		"""Fills the datasets with the tracker information we're needing"""
		for bodyPart in self._bodyPartsInUse:
			self._trackerDict[bodyPart] = viz.addGroup()
#		self.setManuallyAdjustedBone(skeleton.AVATAR_HEAD)

#	def _getInputTrackerMatrix(self, bodyPart):
#		"""
#		Returns the input tracker matrix from the avatar's bones with shifts
#		and randomization.

#		return vizmat.Transform()
#		"""
#		boneMat = self._skeleton.getBoneDict()[bodyPart].getMatrix(viz.ABS_GLOBAL)
		# shift to put some body parts into more standard locations, such as
		# shifting the head to the eyes
#		boneMat.preMult(self._skeleton.getShift(bodyPart))
#		return boneMat

	#def setTrackerAssignment(self, bodyPart, dofValue = '6DOF'):
	#	"""Maintains Tracker Assignments. Zero-dof-value corresponds to body-part-not-in-use"""
	#	self._trackerDofDict[bodyPart] = dofValue
	#	self._reloadIK()

	def _removeIK(self):
		self._ikAnimator.remove()

	def _reloadIK(self):
		"""Removes the existing IK Animator, and instantiates a new one,
		based on current body parts in use.
		"""
		debugMask = 0
		if self._ikAnimator:
			debugMask = self._ikAnimator.getDebugMask()
			self._ikAnimator.setDebugMask(0)
			#try:
			#	self._ikAnimator.remove()
			#except KeyError:
			#	pass
			#self._ikAnimator = None

		# create a new tracker assignment dict from the values already in use
		self._trackerAssignmentDict = {}
		for bodyPart in self._bodyPartsInUse:
			self._trackerAssignmentDict[bodyPart] = (self._trackerDict[bodyPart], None, animator.DOF_6DOF)

		# Initialize a new IK Animator with the same constituents as before
		self._ikAnimator = animator.InverseKinematics(self._avatar,
																	self._skeleton,
																	self._trackerAssignmentDict,
																	debug=True,
																	debugMask=0)

		self._ikAnimator.setDebugMask(debugMask)



class MentorAvatar(object):
	"""Represents the Avatar the user is mean to sympathize with.
	Underlying the Destination Character is a VizAvatar which is driven
	by an Inverse Kinematics Animator
	"""
	def __init__(self):
		avatarObj, skeletonObj = chooseAvatarAndSkeleton()
		self._avatar = avatarObj
		self._skeleton = skeletonObj

		self._voice = []

		# Add a group with offset and parent the underlying avatar to it
		self._group = viz.addGroup()
		self._group.setPosition(0, 1.7, -2)
		self._group.setEuler([180, 0, 0])
		self._avatar.setParent(self._group)

		# Add a floating overhead in-world label
		inWorldLabel = viz.addText("Mentor", pos=[0, .2, 0], euler=[180, 0, 0], scale=[0.2]*3, parent=self._group)
		inWorldLabel.alignment(viz.ALIGN_CENTER_BASE)
		inWorldLabel.setBackdrop(viz.BACKDROP_RIGHT_BOTTOM)

		#vizact.addCallback(REQUEST_RELOAD_IK_EVENT, self._reloadIK)


		# add a keyboard tracker which can be used to move a paused avatar's group nodes
		self._keyboardTracker = virtual_trackers.MouseAndKeyboardWalking(debug=False)
		#self.OculusTracker = virtual_trackers.MouseAndKeyboardWalking(debug=False)
		#self.OculusHandTracker = virtual_trackers.
		self._keyboardTracker.setParent(self._group)
		self._Head = skeleton.AVATAR_HEAD
		self._keyboardTracker.visible(False)

		# Set the starting body parts to the full list of body parts
		self._bodyPartsInUse = PRESET_ASSIGNMENTS['Head, and Hands']

		# Create a dictionary of empty Groups to serve as Trackers
		# (which are to be provided data from the Underlying Avatar's bones)
		self._trackerDict = {}
		self._fillDatasets()

		# Create a dictionary of empty Groups to serve as Trackers
		# (which are to be provided data from the Underlying Avatar's bones)

		self._trackerDofDict = {}
		for bodyPart in self._bodyPartsInUse:
			self._trackerDofDict[bodyPart] = animator.DOF_6DOF

		self._ikAnimator = None
		self._reloadIK()

		# Set up the two update functions, in priority order
		#vizact.onupdate(0, self._onUpdate)


	def getAvatar(self):
		"""Returns the avatar used

		@return viz.VizNode()
		"""
		return self._avatar

	def getSkeleton(self):
		"""Returns the avatar used

		@return viz.VizNode()
		"""
		return self._skeleton

	def face(self,obj,head=None,clamp=None):
		"""Attatch a face object to the head"""
		if isinstance(obj,str):
			newface = addFace(obj)
			if newface.valid():
				self.setFace(newface,head,clamp)
				return newface
			else:
				return None
		self.setFace(obj,head,clamp)

	def getBodyPartsInUse(self):
		"""Returns a list of all the body parts currently in use

		@return []
		"""
		return self._bodyPartsInUse

	def getTrackerDict(self):
		"""Returns the "trackers" provided by the Character.

		@return {}
		"""
		return self._trackerDict

	def _fillDatasets(self):
		"""Fills the datasets with the tracker information we're needing"""
		for bodyPart in self._bodyPartsInUse:
			self._trackerDict[bodyPart] = viz.addGroup()
	#	self.setManuallyAdjustedBone(skeleton.AVATAR_HEAD)

#	def _getInputTrackerMatrix(self, bodyPart):
#		"""
#		Returns the input tracker matrix from the avatar's bones with shifts
#		and randomization.

#		return vizmat.Transform()
#		"""
#		boneMat = self._skeleton.getBoneDict()[bodyPart].getMatrix(viz.ABS_GLOBAL)
		# shift to put some body parts into more standard locations, such as
		# shifting the head to the eyes
#		boneMat.preMult(self._skeleton.getShift(bodyPart))
#		return boneMat

#	def setTrackerAssignment(self, bodyPart, dofValue):
		"""Maintains Tracker Assignments. Zero-dof-value corresponds to body-part-not-in-use"""
#		self._trackerDofDict[bodyPart] = dofValue
#		self._reloadIK()

	def _removeIK(self):
		self._ikAnimator.remove()

	def _reloadIK(self):
		"""Removes the existing IK Animator, and instantiates a new one,
		based on current body parts in use.
		"""
	#	debugMask = 0
	#	if self._ikAnimator:
	#		debugMask = self._ikAnimator.getDebugMask()
	#		self._ikAnimator.setDebugMask(0)
	#		try:
	#			self._ikAnimator.remove()
	#		except KeyError:
	#			pass
	#		self._ikAnimator = None

		# create a new tracker assignment dict from the values already in use
		self._trackerAssignmentDict = {}
		for bodyPart in self._bodyPartsInUse:
			self._trackerAssignmentDict[bodyPart] = (self._trackerDict[bodyPart], None, animator.DOF_6DOF)

		# Initialize a new IK Animator with the same constituents as before
		self._ikAnimator = animator.InverseKinematics(self._avatar,
																	self._skeleton,
																	self._trackerAssignmentDict,
																	debug=True,
																	debugMask=0)




class AvatarConfig(object):
	"""config that manipulates avatars
	"""
	"""Graphical Interface for controlling a Source Character and a Destination Character"""
	def __init__(self, srcCharacter, dstCharacter):
		self._srcCharacter = srcCharacter
		self._srcskeleton = srcCharacter.getSkeleton()
		self._dstCharacter = dstCharacter
		self._dstskeleton = dstCharacter.getSkeleton()




		self._avatars = {self._srcCharacter:self._srcskeleton, self._dstCharacter:self._dstskeleton}

		#self.avskel_cycle = viz.cycle(self._avatars.values())
		#self.av_cycle = viz.cycle(self._avatars)

		self.av = viz.cycle(self._avatars).next()
		#cycle.next to peek at next variable but not "move to it"
		self.av_peek = viz.cycle(self._avatars).peek()

		self.avs = viz.cycle(self._avatars.values()).next()
		#cycle.next to peek at next variable but not "move to it"
		#peek doesn't actually do that I guess?
		self.avs_peek = viz.cycle(self._avatars.values()).peek()


		# initialize the inverse kinematics animator for avatar
		#self.MyAnimator = animator.InverseKinematics(av, avs, av.getTrackerDict(), debug=True)
		self.SwapView(self.av, self.avs)

		viztask.schedule(self.SwapView(self.av, self.avs))


	def selectAvatar(self, avatar_list, avatar_skeleton_list):
		"""Function which selects the avatar"""
		avatar_list = self.av
		avatar_skeleton_list = self.avs
		peek_list = self.av_peek
		#self._avatar.setParent(self._group)
		#peek_list._removeIK()
		avatar_list._reloadIK

		#resolved in avatar object
		#bodyPart = avatar_list.getBodyPartsInUse()
		#avatar_list.setBodyPartsInUse(ALL_BODY_PARTS[:3])
		#bodyPart = ALL_BODY_PARTS[:3]
		#avatar_list.setManuallyAdjustedBone(bodyPart)
		#for bodyPart in av_bodyparts:
		#	avatar_list.setTrackerAssignment(bodyPart, '6DOF')


	def SwapView(self, avatar_list, avatar_skeleton_list):
		while True:
			avatar_list = self.av
			avatar_skeleton_list = self.avs
			viz.cam.reset()
			#view_link = viz.link(avatar_list._avatar.getBone('Bip01 Head'), main_view)
			#view_link.preTrans([0.0, 0.12, 0.107])
			self.selectAvatar(avatar_list, avatar_skeleton_list)
			#updates or is redundant?
			#view.setPosition(view.getPosition())
			#view.setEuler(view.getEuler(viz.BODY_ORI))
			yield viztask.waitKeyDown('f')



# Set up a Utility Menu to control the Source Character and Destination Character
Scenario = AvatarConfig(srcCharacter=ProteusAvatar(),
						dstCharacter=MentorAvatar())

#viztask.schedule(AvatarConfig.swap)




###########################################################
#														  #
#							TODO						  #
#														  #
###########################################################

'''
1: Create a model for basic office scene
2: Add text tiles
3: Add sound recording and pitch down 2 semitones when coming from other avatars mouth
4: create proteusAvatar, ProteusMentor, and swapping class or func
5: Add hand trackers
6: Add custom face upload(temporary)
7: Mentor Avatar mimics the hand motions and voice of the avatar two semitones lower
8: Add tools for recording hand tracker data interpretation, eye recognition, other psychological data gathering tools
9:
10:

##############################
To implement

#
def switchTo(self, Avatar):
	"""Call back funtion to list of avatars?"""
	#unlinks all trackers
	#blinks
	#other avatar opens eyes
	pass

#{Left:movement, Right:movement}
gesticulationDictionary = {}
def gesticulationPlayback(self):
	pass
def gesticulationRecord(self):
	pass


def repeatSpeech(self,_speech):
	pass
	return _speech

def recordSpeech(self,_speech):
	pass
	return _speech

class ProteusAvatar(object):
	"""
	"""
	def __init__(self):


class ProteusMentor(object):
	"""
	"""
	def __init__(self):



class ProteusScene(object):
	"""config that manipulates avatars
	"""
	def __init__(self):
'''
